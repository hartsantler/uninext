#pragma once

#ifndef VECTORR_H
#define VECTORR_H


class Vec2i {
private:
	int X, Y;
	
public:
	Vec2i(void);
	Vec2i(int X, int Y);
	~Vec2i(void);

	int getX();
	void setX(int X);
	int getY();
	void setY(int Y);
};


#endif