#pragma once

#ifndef HEADER_H
#define HEADER_H

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_joystick.h>

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <vector>

#ifdef EMSCRIPTEN
	#include <emscripten.h>
#else
	#define EMSCRIPTEN_KEEPALIVE 
#endif


class Minion;

namespace uninext {
	class EventProp {
	public:
		std::string str;
		bool state;
		EventProp() {}
		EventProp(std::string a, bool b) {
			str = a;
			state = b;
		};
		friend EventProp operator*(std::string &other, const EventProp& this_) {
			auto res = EventProp();
			res.state = false;
			if (this_.str.find(other) != -1 )
				res.state = true;
			return res;
		};
		friend EventProp operator *(const char *other, const EventProp& this_) {
			auto res = EventProp();
			res.state = false;
			if (this_.str.find(std::string(other)) != -1 )
				res.state = true;
			return res;
		};
		operator bool() {return this->state;}
		operator std::string() {return this->str;}
	};

	class Event {
	public:
		std::string self;
		std::string type;
		std::string value;
		Event(){};
		Event(std::string self, std::string type, std::string value){
			this->self = self;
			this->type = type;
			this->value = value;
		};
		EventProp get(std::string type){
			if (type==this->type) return EventProp{std::string(this->value), false};
			else return EventProp();
		}

	};

	Minion* actors(std::string tag);
	Minion* actors(std::string tag, Minion* ptr);
	double random();
	double uniform(double low, double high);
	bool os_path_isfile( std::string path);
	std::string string_to_upper(std::string txt);
	std::vector<std::string> string_split( std::string s, std::string delimiter );
	std::string read( std::string path );
}

#endif
