#include "header.h"
#include <random>
#include <algorithm>
//#include <locale>

namespace uninext {
	std::mt19937 *__rand_engine = NULL;
	std::map<std::string, Minion*> classes;

	Minion* actors(std::string tag, Minion *ptr) {
		classes[tag] = ptr;
		return ptr;
	}


	Minion* actors(std::string tag) {
		if ( classes.count(tag) > 0 )
			return classes[tag];
		else
			return nullptr;
	}

	double random() {
		if (__rand_engine == NULL) {
			std::random_device rand_dev;
			__rand_engine = new std::mt19937(rand_dev());			
		}
		std::uniform_real_distribution<double> unif(0.0, 1.0);
		return unif(*__rand_engine);
	}

	double uniform(double low, double high) {
		if (__rand_engine == NULL) {
			std::random_device rand_dev;
			__rand_engine = new std::mt19937(rand_dev());			
		}
		std::uniform_real_distribution<double> unif(low, high);
		return unif(*__rand_engine);
	}

	bool os_path_isfile( std::string path) {
		std::ifstream infile( path.c_str() );
		return infile.good();
	}

	std::string string_to_upper(std::string str) {
		std::transform(str.begin(), str.end(),str.begin(), ::toupper);
		return str;
	}


	std::vector<std::string> string_split(std::string text, std::string delims) {
		std::vector<std::string> tokens;
		auto len = delims.size();
		std::size_t start = text.find(delims), end = 0;
		if (start == std::string::npos) {
			tokens.push_back(text);
			return tokens;
		}
		if(start != std::string::npos) tokens.push_back(text.substr(0, start));
		while((end = text.find(delims, start+len)) != std::string::npos) {
			tokens.push_back(text.substr(start+len, (end-len) - start));
			start = text.find(delims, end);
		}
		if(start != std::string::npos) tokens.push_back(text.substr(start+len));
		return tokens;
	}

	std::string read( std::string name ){
		try{
			auto s = std::fstream(name.c_str());
			s.exceptions( std::ios::failbit | std::ios::badbit | std::ios::eofbit );
			std::ostringstream c;
			c << s.rdbuf();
			s.close();
			return c.str();
		} catch (...){
			throw std::string("No such file or directory: ")+name;
		}
	}

} // namespace