#include "header.h"
#include "Core.h"

#ifndef EMSCRIPTEN
	#ifndef TPY_UNINEXT
		int main(int argc, char *argv[])
		{
			CCore oCore;
			
			oCore.mainLoop();

			return 0;
		}
	#endif
#endif