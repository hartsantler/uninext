#include "IMG.h"

/* ******************************************** */

CIMG::CIMG(void) { }

CIMG::CIMG(std::string fileName, SDL_Renderer* rR) {
	setIMG(fileName, rR);
}

CIMG::~CIMG(void) {
	SDL_DestroyTexture(tIMG);
}

/* ******************************************** */

void CIMG::Draw(SDL_Renderer* rR, int iXOffset, int iYOffset) {
	rRect.x = iXOffset;
	rRect.y = iYOffset;

	SDL_RenderCopy(rR, tIMG, NULL, &rRect);
}

void CIMG::Draw(SDL_Renderer* rR, int iXOffset, int iYOffset, bool bRotate) {
	rRect.x = iXOffset;
	rRect.y = iYOffset;

	if(!bRotate) {
		SDL_RenderCopy(rR, tIMG, NULL, &rRect);
	} else {
		SDL_RenderCopyEx(rR, tIMG, NULL, &rRect, 180.0, NULL, SDL_FLIP_VERTICAL);
	}
}

void CIMG::DrawVert(SDL_Renderer* rR, int iXOffset, int iYOffset) {
	rRect.x = iXOffset;
	rRect.y = iYOffset;

	SDL_RenderCopyEx(rR, tIMG, NULL, &rRect, 180.0, NULL, SDL_FLIP_HORIZONTAL);
}

void CIMG::Draw(SDL_Renderer* rR, SDL_Rect rCrop, SDL_Rect rRect) {
	SDL_RenderCopy(rR, tIMG, &rCrop, &rRect);
}

/* ******************************************** */

void CIMG::setIMG(std::string fileName, SDL_Renderer* rR) {
	SDL_Surface* loadedSurface;
	std::cout << fileName << std::endl;
	#ifdef EMSCRIPTEN
		std::string path = "/tinypy/uninext/files/images/";
	#else
		std::string path = "files/images/";
	#endif

	if (uninext::os_path_isfile(path + fileName + ".png") ) {
		#ifdef DEBUG
		std::cout << "LOADING PNG VERSION" << std::endl;
		#endif
		fileName = path + fileName + ".png";
		loadedSurface = IMG_Load(fileName.c_str());

	} else if (uninext::os_path_isfile(path + fileName + ".gif") ) {
		#ifdef DEBUG
		std::cout << "LOADING GIF VERSION" << std::endl;
		#endif
		fileName = path + fileName + ".gif";
		loadedSurface = IMG_Load(fileName.c_str());
		SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 255, 0, 255));

	} else {
		//fileName = "files/images/" + fileName + ".bmp";
		//loadedSurface = SDL_LoadBMP(fileName.c_str());
		//SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 255, 0, 255));
		try {
			std::cout << "TRYING LOADING GIF VERSION" << std::endl;
			fileName = path + fileName + ".gif";
			loadedSurface = IMG_Load(fileName.c_str());
			if (loadedSurface) {
				SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 255, 0, 255));				
			} else {
				std::cout << "LOADING FAILURE: " << fileName << std::endl;
				throw fileName;
			}

		} catch (...) {
			std::cout << "LOADING FAILURE: " << fileName << std::endl;
			throw fileName;

		}
	}

	#ifdef DEBUG
		std::cout << loadedSurface << std::endl;
	#endif

	tIMG = SDL_CreateTextureFromSurface(rR, loadedSurface);
	int iWidth, iHeight;

	SDL_QueryTexture(tIMG, NULL, NULL, &iWidth, &iHeight);
	
	rRect.x  = 0;
	rRect.y = 0;
	rRect.w = iWidth;
	rRect.h = iHeight;
	SDL_FreeSurface(loadedSurface);
}

SDL_Texture* CIMG::getIMG() {
	return tIMG;
}

SDL_Rect CIMG::getRect() {
	return rRect;
}