/************************
 * @author �ukasz Jakowski
 * @since  02.05.2014 16:13
 * 
 ************************/

#include "MusicManager.h"

/* ******************************************** */

MusicManager::MusicManager(void) {
}

MusicManager::~MusicManager(void) {
}

/* ******************************************** */

void MusicManager::PlayMusic() {
	#ifdef EMSCRIPTEN
		std::string path = "/tinypy/uninext/files/sounds/overworld.ogg";
	#else
		std::string path = "files/sounds/overworld.ogg";
	#endif

	vMusic.push_back(Mix_LoadMUS(path.c_str()));
	vMusic.push_back(Mix_LoadMUS(path.c_str()));
	Mix_VolumeMusic(100);
	Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048);
	Mix_PlayMusic(vMusic[0], -1);
}

/* ******************************************** */

Mix_Music* MusicManager::loadMusic(std::string fileName) {
	#ifdef EMSCRIPTEN
		fileName = "/tinypy/uninext/files/sounds/" + fileName + ".ogg";
	#else
		fileName = "files/sounds/" + fileName + ".ogg";
	#endif
	return Mix_LoadMUS( fileName.c_str() );
}

/* ******************************************** */