#include "Vec2i.h"

/* ******************************************** */

Vec2i::Vec2i(void) { }

Vec2i::Vec2i(int X, int Y) {
	this->X = X;
	this->Y = Y;
}

Vec2i::~Vec2i(void) { }

/* ******************************************** */

int Vec2i::getX() {
	return X;
}

void Vec2i::setX(int X) {
	this->X = X;
}

int Vec2i::getY() {
	return Y;
}

void Vec2i::setY(int Y) {
	this->Y = Y;
}