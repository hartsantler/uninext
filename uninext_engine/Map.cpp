#include "Map.h"
#include "CFG.h"
#include "math.h"
#include "stdlib.h"
#include "time.h"

#include "uninext_map.gen.h"

/* ******************************************** */


void Map::run_command( std::string s ){
	std::cout << "parsing..." << s << std::endl;

	if (s[0]=='>') {
		#ifdef TPY_UNINEXT
			std::cout << "setting player bytecode" << std::endl;
			std::string path = s.substr(1, s.size());
			std::cout << path << std::endl;
			oPlayer->bytecode = uninext::read(path);
		#endif
	} else {
		auto tokens = uninext::string_split(s, "|");
		for (auto token: tokens) {
			auto parts = uninext::string_split(token, "/");
			if (parts.size()==3) {
				auto symbol = parts[0];
				auto event  = parts[1];
				auto value  = parts[2];
				std::cout << "symbol=" << symbol << " event=" << event << " value=" << value << std::endl;

				if (event=="📱")
					oPlayer->dialog = uninext::string_to_upper(value);

				if (uninext::actors(symbol)){
					auto evt = uninext::Event(symbol, event, value);
					uninext::actors(symbol)->update_events(evt);  // defined in Minion.cpp				
				}


			} else {
				std::cout << "PARSE ERROR" << token << std::endl;
				std::cout << "PARSE ERROR" << parts.size() << std::endl;
			}
		}


	}


}

Map::Map(void) {

}

Map::Map(SDL_Renderer* rR) {
	oPlayer = new Player(rR, 84, 368);

	this->currentLevelID = 0;

	this->iMapWidth = 0;
	this->iMapHeight = 0;
	this->iLevelType = 0;

	this->drawLines = false;
	this->fXPos = 0;
	this->fYPos = 0;

	this->inEvent = false;

	this->iSpawnPointID = 0;

	this->bMoveMap = true;

	this->iFrameID = 0;

	this->bTP = false;

	CCFG::getText()->setFont(rR, "font");

	oEvent = new Event();
	oFlag = NULL;

	srand((unsigned)time(NULL));

	loadGameData(rR);
	loadLVL();
}

Map::~Map(void) {
	for(std::vector<Block*>::iterator i = vBlock.begin(); i != vBlock.end(); i++) {
		delete (*i);
	}

	for(std::vector<Block*>::iterator i = vMinion.begin(); i != vMinion.end(); i++) {
		delete (*i);
	}

	delete oEvent;
	delete oFlag;
}

/* ******************************************** */

void Map::Update() {
	UpdateBlocks();

	if(!oPlayer->getInLevelAnimation()) {
		UpdateMinionBlokcs();

		UpdateMinions();
		
		if(!inEvent) {
			UpdatePlayer();

			++iFrameID;
			if(iFrameID > 32) {
				iFrameID = 0;
				if(iMapTime > 0) {
					--iMapTime;
					if(iMapTime == 90) {
						CCFG::getMusic()->StopMusic();
						CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cLOWTIME);
					} else if(iMapTime == 86) {
						CCFG::getMusic()->changeMusic(true, true);
					}

					if(iMapTime <= 0) {
						playerDeath(true, true);
					}
				}
			}
		} else {
			oEvent->Animation();
		}

		for(unsigned int i = 0; i < vPlatform.size(); i++) {
			vPlatform[i]->Update();
		}
	} else {
		oPlayer->powerUPAnimation();
	}

	for(unsigned int i = 0; i < lBlockDebris.size(); i++) {
		if(lBlockDebris[i]->getDebrisState() != -1) {
			lBlockDebris[i]->Update();
		} else {
			delete lBlockDebris[i];
			lBlockDebris.erase(lBlockDebris.begin() + i);
		}
	}
	
	for(unsigned int i = 0; i < lPoints.size(); i++) {
		if(!lPoints[i]->getDelete()) {
			lPoints[i]->Update();
		} else {
			delete lPoints[i];
			lPoints.erase(lPoints.begin() + i);
		}
	}

	for(unsigned int i = 0; i < lCoin.size(); i++) {
		if(!lCoin[i]->getDelete()) {
			lCoin[i]->Update();
		} else {
			lPoints.push_back(new Points(lCoin[i]->getXPos(), lCoin[i]->getYPos(), "200"));
			delete lCoin[i];
			lCoin.erase(lCoin.begin() + i);
		}
	}
}

void Map::UpdatePlayer() {
	oPlayer->Update();
	checkSpawnPoint();
}

void Map::UpdateMinions() {
	for(int i = 0; i < iMinionListSize; i++) {
		for(int j = 0, jSize = lMinion[i].size(); j < jSize; j++) {

			// uninext do not update paused actors //
			if (lMinion[i][j]->pause) continue;

			if(lMinion[i][j]->updateMinion()) {
				lMinion[i][j]->Update();
			}
		}
	}
	
	// ----- UPDATE MINION LIST ID
	for(int i = 0; i < iMinionListSize; i++) {
		for(int j = 0, jSize = lMinion[i].size(); j < jSize; j++) {
			if(lMinion[i][j]->minionSpawned) {
				if(lMinion[i][j]->minionState == -1) {
					delete lMinion[i][j];
					lMinion[i].erase(lMinion[i].begin() + j);
					jSize = lMinion[i].size();
					continue;
				}
				
				if(floor(lMinion[i][j]->fXPos / 160) != i) {
					lMinion[(int)floor((int)lMinion[i][j]->fXPos / 160)].push_back(lMinion[i][j]);
					lMinion[i].erase(lMinion[i].begin() + j);
					jSize = lMinion[i].size();
				}
			}
		}
	}

	for(unsigned int i = 0; i < lBubble.size(); i++) {
		lBubble[i]->Update();

		if(lBubble[i]->getDestroy()) {
			delete lBubble[i];
			lBubble.erase(lBubble.begin() + i);
		}
	}
}

void Map::UpdateMinionsCollisions() {
	// ----- COLLISIONS
	for(int i = 0; i < iMinionListSize; i++) {
		for(unsigned int j = 0; j < lMinion[i].size(); j++) {
			if(!lMinion[i][j]->collisionOnlyWithPlayer /*&& lMinion[i][j]->minionSpawned*/ && lMinion[i][j]->deadTime < 0) {
				// ----- WITH MINIONS IN SAME LIST
				for(unsigned int k = j + 1; k < lMinion[i].size(); k++) {
					if(!lMinion[i][k]->collisionOnlyWithPlayer /*&& lMinion[i][k]->minionSpawned*/ && lMinion[i][k]->deadTime < 0) {
						if(lMinion[i][j]->getXPos() < lMinion[i][k]->getXPos()) {
							if(lMinion[i][j]->getXPos() + lMinion[i][j]->iHitBoxX >= lMinion[i][k]->getXPos() && lMinion[i][j]->getXPos() + lMinion[i][j]->iHitBoxX <= lMinion[i][k]->getXPos() + lMinion[i][k]->iHitBoxX && ((lMinion[i][j]->getYPos() <= lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY && lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY >= lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY) || (lMinion[i][k]->getYPos() <= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY && lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY >= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY))) {
								if(lMinion[i][j]->killOtherUnits && lMinion[i][j]->moveSpeed > 0 && lMinion[i][k]->minionSpawned) {
									lMinion[i][k]->setMinionState(-2);
									lMinion[i][j]->collisionWithAnotherUnit();
								}

								if(lMinion[i][k]->killOtherUnits && lMinion[i][k]->moveSpeed > 0 && lMinion[i][j]->minionSpawned) {
									lMinion[i][j]->setMinionState(-2);
									lMinion[i][k]->collisionWithAnotherUnit();
								}
							
								if(lMinion[i][j]->getYPos() - 4 <= lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY && lMinion[i][j]->getYPos() + 4 >= lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY) {
									lMinion[i][k]->onAnotherMinion = true;
								} else if(lMinion[i][k]->getYPos() - 4 <= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY && lMinion[i][k]->getYPos() + 4 >= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY) {
									lMinion[i][j]->onAnotherMinion = true;
								} else {
									lMinion[i][j]->collisionEffect();
									lMinion[i][k]->collisionEffect();
								}
							}
						} else {
							if(lMinion[i][k]->getXPos() + lMinion[i][k]->iHitBoxX >= lMinion[i][j]->getXPos() && lMinion[i][k]->getXPos() + lMinion[i][k]->iHitBoxX <= lMinion[i][j]->getXPos() + lMinion[i][j]->iHitBoxX && ((lMinion[i][j]->getYPos() <= lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY && lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY >= lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY) || (lMinion[i][k]->getYPos() <= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY && lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY >= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY))) {
								if(lMinion[i][j]->killOtherUnits && lMinion[i][j]->moveSpeed > 0 && lMinion[i][k]->minionSpawned) {
									lMinion[i][k]->setMinionState(-2);
									lMinion[i][j]->collisionWithAnotherUnit();
								}

								if(lMinion[i][k]->killOtherUnits && lMinion[i][k]->moveSpeed > 0 && lMinion[i][j]->minionSpawned) {
									lMinion[i][j]->setMinionState(-2);
									lMinion[i][k]->collisionWithAnotherUnit();
								}

								if(lMinion[i][j]->getYPos() - 4 <= lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY && lMinion[i][j]->getYPos() + 4 >= lMinion[i][k]->getYPos() + lMinion[i][k]->iHitBoxY) {
									lMinion[i][k]->onAnotherMinion = true;
								} else if(lMinion[i][k]->getYPos() - 4 <= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY && lMinion[i][k]->getYPos() + 4 >= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY) {
									lMinion[i][j]->onAnotherMinion = true;
								} else {
									lMinion[i][j]->collisionEffect();
									lMinion[i][k]->collisionEffect();
								}
							}
						}
					}
				}

				// ----- WITH MINIONS IN OTHER LIST
				if(i + 1 < iMinionListSize) {
					for(unsigned int k = 0; k < lMinion[i + 1].size(); k++) {
						if(!lMinion[i + 1][k]->collisionOnlyWithPlayer /*&& lMinion[i + 1][k]->minionSpawned*/ && lMinion[i + 1][k]->deadTime < 0) {
							if(lMinion[i][j]->getXPos() < lMinion[i + 1][k]->getXPos()) {
								if(lMinion[i][j]->getXPos() + lMinion[i][j]->iHitBoxX >= lMinion[i + 1][k]->getXPos() && lMinion[i][j]->getXPos() + lMinion[i][j]->iHitBoxX <= lMinion[i + 1][k]->getXPos() + lMinion[i + 1][k]->iHitBoxX && ((lMinion[i][j]->getYPos() <= lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY && lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY >= lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY) || (lMinion[i + 1][k]->getYPos() <= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY && lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY >= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY))) {
									if(lMinion[i][j]->killOtherUnits && lMinion[i][j]->moveSpeed > 0 && lMinion[i + 1][k]->minionSpawned) {
										lMinion[i + 1][k]->setMinionState(-2);
										lMinion[i][j]->collisionWithAnotherUnit();
									}

									if(lMinion[i + 1][k]->killOtherUnits && lMinion[i + 1][k]->moveSpeed > 0 && lMinion[i][j]->minionSpawned) {
										lMinion[i][j]->setMinionState(-2);
										lMinion[i + 1][k]->collisionWithAnotherUnit();
									}
									
									if(lMinion[i][j]->getYPos() - 4 <= lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY && lMinion[i][j]->getYPos() + 4 >= lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY) {
										lMinion[i + 1][k]->onAnotherMinion = true;
									} else if(lMinion[i + 1][k]->getYPos() - 4 <= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY && lMinion[i + 1][k]->getYPos() + 4 >= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY) {
										lMinion[i][j]->onAnotherMinion = true;
									} else {
										lMinion[i][j]->collisionEffect();
										lMinion[i + 1][k]->collisionEffect();
									}
								}
							} else {
								if(lMinion[i + 1][k]->getXPos() + lMinion[i + 1][k]->iHitBoxX >= lMinion[i][j]->getXPos() && lMinion[i + 1][k]->getXPos() + lMinion[i + 1][k]->iHitBoxX < lMinion[i][j]->getXPos() + lMinion[i][j]->iHitBoxX && ((lMinion[i][j]->getYPos() <= lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY && lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY >= lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY) || (lMinion[i + 1][k]->getYPos() <= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY && lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY >= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY))) {
									if(lMinion[i][j]->killOtherUnits && lMinion[i][j]->moveSpeed > 0 && lMinion[i + 1][k]->minionSpawned) {
										lMinion[i + 1][k]->setMinionState(-2);
										lMinion[i][j]->collisionWithAnotherUnit();
									}

									if(lMinion[i + 1][k]->killOtherUnits && lMinion[i + 1][k]->moveSpeed > 0 && lMinion[i][j]->minionSpawned) {
										lMinion[i][j]->setMinionState(-2);
										lMinion[i + 1][k]->collisionWithAnotherUnit();
									}
									/*
									if(lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY < lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY) {
										lMinion[i][j]->onAnotherMinion = true;
										continue;
									} else {
										lMinion[i + 1][k]->onAnotherMinion = true;
										continue;
									}*/

									if(lMinion[i][j]->getYPos() - 4 <= lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY && lMinion[i][j]->getYPos() + 4 >= lMinion[i + 1][k]->getYPos() + lMinion[i + 1][k]->iHitBoxY) {
										lMinion[i + 1][k]->onAnotherMinion = true;
									} else if(lMinion[i + 1][k]->getYPos() - 4 <= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY && lMinion[i + 1][k]->getYPos() + 4 >= lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY) {
										lMinion[i][j]->onAnotherMinion = true;
									} else {
										lMinion[i][j]->collisionEffect();
										lMinion[i + 1][k]->collisionEffect();
									}
								}
							}
						}
					}
				}
			}
		}
	}

	if(!inEvent && !oPlayer->getInLevelAnimation()) {
		// ----- COLLISION WITH PLAYER
		for(int i = getListID(-(int)fXPos + oPlayer->getXPos()) - (getListID(-(int)fXPos + oPlayer->getXPos()) > 0 ? 1 : 0), iSize = i + 2; i < iSize; i++) {
			for(unsigned int j = 0, jSize = lMinion[i].size(); j < jSize; j++) {
				if(lMinion[i][j]->deadTime < 0) {
					if((oPlayer->getXPos() - fXPos >= lMinion[i][j]->getXPos() && oPlayer->getXPos() - fXPos <= lMinion[i][j]->getXPos() + lMinion[i][j]->iHitBoxX) || (oPlayer->getXPos() - fXPos + oPlayer->getHitBoxX() >= lMinion[i][j]->getXPos() && oPlayer->getXPos() - fXPos + oPlayer->getHitBoxX() <= lMinion[i][j]->getXPos() + lMinion[i][j]->iHitBoxX)) {
						if(lMinion[i][j]->getYPos() - 2 <= oPlayer->getYPos() + oPlayer->getHitBoxY() && lMinion[i][j]->getYPos() + 16 >= oPlayer->getYPos() + oPlayer->getHitBoxY()) {
							lMinion[i][j]->collisionWithPlayer(true);
						} else if((lMinion[i][j]->getYPos() <= oPlayer->getYPos() + oPlayer->getHitBoxY() && lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY >= oPlayer->getYPos() + oPlayer->getHitBoxY()) || (lMinion[i][j]->getYPos() <= oPlayer->getYPos() && lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY >= oPlayer->getYPos())) {
							lMinion[i][j]->collisionWithPlayer(false);
						}
					}
				}
			}
		}
	}
}

void Map::UpdateBlocks() {
	vBlock[2]->getSprite()->Update();
	vBlock[8]->getSprite()->Update();
	vBlock[29]->getSprite()->Update();
	vBlock[55]->getSprite()->Update();
	vBlock[57]->getSprite()->Update();
	vBlock[70]->getSprite()->Update();
	vBlock[71]->getSprite()->Update();
	vBlock[72]->getSprite()->Update();
	vBlock[73]->getSprite()->Update();
	vBlock[82]->getSprite()->Update();
}

void Map::UpdateMinionBlokcs() {
	vMinion[0]->getSprite()->Update();
	vMinion[4]->getSprite()->Update();
	vMinion[6]->getSprite()->Update();
	vMinion[7]->getSprite()->Update();
	vMinion[8]->getSprite()->Update();
	vMinion[10]->getSprite()->Update();
	vMinion[12]->getSprite()->Update();
	vMinion[14]->getSprite()->Update();
	vMinion[15]->getSprite()->Update();
	vMinion[17]->getSprite()->Update();
	vMinion[18]->getSprite()->Update();
	vMinion[19]->getSprite()->Update();
	vMinion[20]->getSprite()->Update();
	vMinion[21]->getSprite()->Update();
	vMinion[22]->getSprite()->Update();
	vMinion[23]->getSprite()->Update();
	vMinion[24]->getSprite()->Update();
	vMinion[30]->getSprite()->Update();
	vMinion[31]->getSprite()->Update();
	vMinion[43]->getSprite()->Update();
	vMinion[44]->getSprite()->Update();
	vMinion[45]->getSprite()->Update();
	vMinion[46]->getSprite()->Update();
	vMinion[47]->getSprite()->Update();
	vMinion[48]->getSprite()->Update();
	vMinion[51]->getSprite()->Update();
	vMinion[52]->getSprite()->Update();
	vMinion[53]->getSprite()->Update();
	vMinion[55]->getSprite()->Update();
	vMinion[57]->getSprite()->Update();
	vMinion[62]->getSprite()->Update();
}

/* ******************************************** */

void Map::Draw(SDL_Renderer* rR) {
	DrawMap(rR);

	for(unsigned int i = 0; i < vPlatform.size(); i++) {
		vPlatform[i]->Draw(rR);
	}

	DrawMinions(rR);

	for(unsigned int i = 0; i < lPoints.size(); i++) {
		lPoints[i]->Draw(rR);
	}

	for(unsigned int i = 0; i < lCoin.size(); i++) {
		lCoin[i]->Draw(rR);
	}

	for(unsigned int i = 0; i < lBlockDebris.size(); i++) {
		lBlockDebris[i]->Draw(rR);
	}

	for(unsigned int i = 0; i < vLevelText.size(); i++) {
		CCFG::getText()->Draw(rR, vLevelText[i]->getText(), vLevelText[i]->getXPos() + (int)fXPos, vLevelText[i]->getYPos());
	}
	/*
	if(drawLines) {
		aa->Draw(rR, (int)fXPos, -16);
		DrawLines(rR);
	}*/

	for(unsigned int i = 0; i < lBubble.size(); i++) {
		lBubble[i]->Draw(rR, vBlock[lBubble[i]->getBlockID()]->getSprite()->getTexture());
	}

	oPlayer->Draw(rR);
	if (oPlayer->dialog.size())
		CCFG::getText()->Draw(rR, oPlayer->dialog.c_str(), oPlayer->getXPos()+(int)fXPos, oPlayer->getYPos()-16);


	if(inEvent) {
		oEvent->Draw(rR);
	}

	DrawGameLayout(rR);
}

void Map::DrawMap(SDL_Renderer* rR) {
	if(this->background != NULL) {
		this->background->getTexture()->Draw(rR, this->fXPos,0, false);
	}

	if(oFlag != NULL) {
		oFlag->DrawCastleFlag(rR, vBlock[51]->getSprite()->getTexture());
	}
	int blocks = 0;
	for(int i = getStartBlock(), iEnd = getEndBlock(); i < iEnd && i < iMapWidth; i++) {
		for(int j = iMapHeight - 1; j >= 0; j--) {
			if(lMap[i][j]->getBlockID() != 0) {
				vBlock[lMap[i][j]->getBlockID()]->Draw(rR, 32 * i + (int)fXPos, CCFG::GAME_HEIGHT - 32 * j - 16 - lMap[i][j]->updateYPos());
				blocks += 1;
			}
		}
	}
	//#ifdef EMSCRIPTEN
	//	std::cout << "draw blocks: " << blocks << std::endl;
	//#endif

	if(oFlag != NULL) {
		oFlag->Draw(rR, vBlock[oFlag->iBlockID]->getSprite()->getTexture());
	}
}

void Map::DrawMinions(SDL_Renderer* rR) {
	for(int i = 0; i < iMinionListSize; i++) {
		for(int j = 0, jSize = lMinion[i].size(); j < jSize; j++) {
			//CCFG::getText()->DrawWS(rR, std::to_string(i), lMinion[i][j]->getXPos() + (int)fXPos, lMinion[i][j]->getYPos(), 0, 0, 0, 8);
			auto act = lMinion[i][j];
			act->Draw(rR, vMinion[lMinion[i][j]->getBloockID()]->getSprite()->getTexture());


			// JUST TESTING //
			//auto event = uninext::Event("👸", "📱", "hi");
			//if (event.self==act->tag)
			//	act->update_events(event);  // defined in Minion.cpp


			if (act->dialog.size())
				CCFG::getText()->Draw(rR, act->dialog.c_str(), act->getXPos()+(int)fXPos, act->getYPos()-16);

		}
	}
}

void Map::DrawGameLayout(SDL_Renderer* rR) {
	CCFG::getText()->Draw(rR, "MARIO", 54, 16);

	if(oPlayer->getScore() < 100) {
		CCFG::getText()->Draw(rR, "00000" + std::to_string(oPlayer->getScore()), 54, 32);
	} else if(oPlayer->getScore() < 1000) {
		CCFG::getText()->Draw(rR, "000" + std::to_string(oPlayer->getScore()), 54, 32);
	} else if(oPlayer->getScore() < 10000) {
		CCFG::getText()->Draw(rR, "00" + std::to_string(oPlayer->getScore()), 54, 32);
	} else if(oPlayer->getScore() < 100000) {
		CCFG::getText()->Draw(rR, "0" + std::to_string(oPlayer->getScore()), 54, 32);
	} else {
		CCFG::getText()->Draw(rR, std::to_string(oPlayer->getScore()), 54, 32);
	}

	CCFG::getText()->Draw(rR, "WORLD", 462, 16);
	CCFG::getText()->Draw(rR, getLevelName(), 480, 32);

	if(iLevelType != 1) {
		vBlock[2]->Draw(rR, 268, 32);
	} else {
		vBlock[57]->Draw(rR, 268, 32);
	}
	CCFG::getText()->Draw(rR, "y", 286, 32);
	CCFG::getText()->Draw(rR, (oPlayer->getCoins() < 10 ? "0" : "") + std::to_string(oPlayer->getCoins()), 302, 32);

	CCFG::getText()->Draw(rR, "TIME", 672, 16);
	if(CCFG::getMM()->getViewID() == CCFG::getMM()->eGame) {
		if(iMapTime > 100) {
			CCFG::getText()->Draw(rR, std::to_string(iMapTime), 680, 32);
		} else if(iMapTime > 10) {
			CCFG::getText()->Draw(rR, "0" + std::to_string(iMapTime), 680, 32);
		} else {
			CCFG::getText()->Draw(rR, "00" + std::to_string(iMapTime), 680, 32);
		}
	}
}

void Map::DrawLines(SDL_Renderer* rR) {
	SDL_SetRenderDrawBlendMode(rR, SDL_BLENDMODE_BLEND); // APLHA ON !
	SDL_SetRenderDrawColor(rR, 255, 255, 255, 128);

	for(int i = 0; i < CCFG::GAME_WIDTH / 32 + 1; i++) {
		SDL_RenderDrawLine(rR, 32 * i - (-(int)fXPos) % 32, 0, 32 * i - (-(int)fXPos) % 32, CCFG::GAME_HEIGHT);
	}

	for(int i = 0; i < CCFG::GAME_HEIGHT / 32 + 1; i++) {
		SDL_RenderDrawLine(rR, 0, 32 * i - 16 + (int)fYPos, CCFG::GAME_WIDTH, 32 * i - 16 + (int)fYPos);
	}

	for(int i = 0; i < CCFG::GAME_WIDTH / 32 + 1; i++) {
		for(int j = 0; j < CCFG::GAME_HEIGHT / 32; j++) {
			CCFG::getText()->Draw(rR, std::to_string(i + (-((int)fXPos + (-(int)fXPos) % 32)) / 32), 32 * i + 16 - (-(int)fXPos) % 32 - CCFG::getText()->getTextWidth(std::to_string(i + (-((int)fXPos + (-(int)fXPos) % 32)) / 32), 8) / 2, CCFG::GAME_HEIGHT - 9 - 32 * j, 8);
			CCFG::getText()->Draw(rR, std::to_string(j), 32 * i + 16 - (-(int)fXPos) % 32 - CCFG::getText()->getTextWidth(std::to_string(j), 8) / 2 + 1, CCFG::GAME_HEIGHT - 32 * j, 8);
		}
	}

	SDL_SetRenderDrawBlendMode(rR, SDL_BLENDMODE_NONE); // APLHA OFF !
}

/* ******************************************** */

void Map::moveMap(int nX, int nY) {
	if (fXPos + nX > 0) {
		oPlayer->updateXPos((int)(nX - fXPos));
		fXPos = 0;
	}
	else {
		this->fXPos += nX;
	}
}

int Map::getStartBlock() {
	return (int)(-fXPos - (-(int)fXPos) % 32) / 32;
}

int Map::getEndBlock() {
	return (int)(-fXPos - (-(int)fXPos) % 32 + CCFG::GAME_WIDTH) / 32 + 2;
}

/* ******************************************** */

/* ******************************************** */
/* ---------------- COLLISION ---------------- */

Vec2i* Map::getBlockID(int nX, int nY) {
	return new Vec2i((int)(nX < 0 ? 0 : nX) / 32, (int)(nY > CCFG::GAME_HEIGHT - 16 ? 0 : (CCFG::GAME_HEIGHT - 16 - nY + 32) / 32));
}

int Map::getBlockIDX(int nX) {
	return (int)(nX < 0 ? 0 : nX) / 32;
}

int Map::getBlockIDY(int nY) {
	return (int)(nY > CCFG::GAME_HEIGHT - 16 ? 0 : (CCFG::GAME_HEIGHT - 16 - nY + 32) / 32);
}

bool Map::checkCollisionLB(int nX, int nY, int nHitBoxY, bool checkVisible) {
	return checkCollision(getBlockID(nX, nY + nHitBoxY), checkVisible);
}

bool Map::checkCollisionLT(int nX, int nY, bool checkVisible) {
	return checkCollision(getBlockID(nX, nY), checkVisible);
}

bool Map::checkCollisionLC(int nX, int nY, int nHitBoxY, bool checkVisible) {
	return checkCollision(getBlockID(nX, nY + nHitBoxY), checkVisible);
}

bool Map::checkCollisionRC(int nX, int nY, int nHitBoxX, int nHitBoxY, bool checkVisible) {
	return checkCollision(getBlockID(nX + nHitBoxX, nY + nHitBoxY), checkVisible);
}

bool Map::checkCollisionRB(int nX, int nY, int nHitBoxX, int nHitBoxY, bool checkVisible) {
	return checkCollision(getBlockID(nX + nHitBoxX, nY + nHitBoxY), checkVisible);
}

bool Map::checkCollisionRT(int nX, int nY, int nHitBoxX, bool checkVisible) {
	return checkCollision(getBlockID(nX + nHitBoxX, nY), checkVisible);
}

int Map::checkCollisionWithPlatform(int nX, int nY, int iHitBoxX, int iHitBoxY) {
	for(unsigned int i = 0; i < vPlatform.size(); i++) {
		if(-fXPos + nX + iHitBoxX >= vPlatform[i]->getXPos() && - fXPos + nX <= vPlatform[i]->getXPos() + vPlatform[i]->getSize() * 16) {
			if(nY + iHitBoxY >= vPlatform[i]->getYPos() && nY + iHitBoxY <= vPlatform[i]->getYPos() + 16) {
				return i;
			}
		}
	}

	return -1;
}

bool Map::checkCollision(Vec2i* nV, bool checkVisible) {
	bool output = vBlock[lMap[nV->getX()][nV->getY()]->getBlockID()]->getCollision() && (checkVisible ? vBlock[lMap[nV->getX()][nV->getY()]->getBlockID()]->getVisible() : true);
	delete nV;
	return output;
}

void Map::checkCollisionOnTopOfTheBlock(int nX, int nY) {
	switch(lMap[nX][nY + 1]->getBlockID()) {
		case 29: case 71: case 72: case 73:// COIN
			lMap[nX][nY + 1]->setBlockID(0);
			lCoin.push_back(new Coin(nX * 32 + 7, CCFG::GAME_HEIGHT - nY * 32 - 48));
			CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cCOIN);
			oPlayer->setCoins(oPlayer->getCoins() + 1);
			return;
			break;
	}

	for(int i = (nX - nX%5)/5, iEnd = i + 3; i < iEnd && i < iMinionListSize; i++) {
		for(unsigned int j = 0; j < lMinion[i].size(); j++) {
			if(!lMinion[i][j]->collisionOnlyWithPlayer && lMinion[i][j]->getMinionState() >= 0 && ((lMinion[i][j]->getXPos() >= nX*32 && lMinion[i][j]->getXPos() <= nX*32 + 32) || (lMinion[i][j]->getXPos() + lMinion[i][j]->iHitBoxX >= nX*32 && lMinion[i][j]->getXPos() + lMinion[i][j]->iHitBoxX <= nX*32 + 32))) {
				if(lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY >= CCFG::GAME_HEIGHT - 24 - nY*32 && lMinion[i][j]->getYPos() + lMinion[i][j]->iHitBoxY <= CCFG::GAME_HEIGHT - nY*32 + 16) {
					lMinion[i][j]->moveDirection = !lMinion[i][j]->moveDirection;
					lMinion[i][j]->setMinionState(-2);
				}
			}
		}
	}
}

/* ---------------- COLLISION ---------------- */
/* ******************************************** */
/* ----------------- MINIONS ----------------- */

int Map::getListID(int nX) {
	return (int)(nX / 160);
}

void Map::addPoints(int X, int Y, std::string sText, int iW, int iH) {
	lPoints.push_back(new Points(X, Y, sText, iW, iH));
}

Goombas* Map::addGoombas(int iX, int iY, bool moveDirection) {
	auto ptr = new Goombas(iX, iY, iLevelType == 0 || iLevelType == 4 ? 0 : iLevelType == 1 ? 8 : 10, moveDirection);
	lMinion[getListID(iX)].push_back(ptr);
	return ptr;
}

Koppa* Map::addKoppa(int iX, int iY, int minionState, bool moveDirection) {
	int tempBlock;

	switch(minionState) {
		case 0: case 3:
			tempBlock = iLevelType == 0 || iLevelType == 4 ? 7 : iLevelType == 1 ? 14 : 17;
			break;
		case 1:
			tempBlock = iLevelType == 0 || iLevelType == 4 ? 4 : iLevelType == 1 ? 12 : 15;
			break;
		case 2:
			tempBlock = iLevelType == 0 || iLevelType == 4 ? 5 : iLevelType == 1 ? 13 : 16;
			break;
	}
	auto ptr = new Koppa(iX, iY, minionState, moveDirection, tempBlock);
	lMinion[getListID(iX)].push_back(ptr);
	return ptr;
}

Beetle* Map::addBeetle(int X, int Y, bool moveDirection) {
	auto ptr = new Beetle(X, Y, moveDirection);
	lMinion[getListID(X)].push_back(ptr);
	return ptr;
}

void Map::addPlant(int iX, int iY) {
	lMinion[getListID(iX)].push_back(new Plant(iX, iY, iLevelType == 0 || iLevelType == 4 ? 18 : 19));
}

Toad* Map::addToad(int X, int Y, bool peach) {
	auto ptr = new Toad(X, Y, peach);
	lMinion[getListID(X)].push_back(ptr);
	return ptr;
}

Squid* Map::addSquid(int X, int Y) {
	auto ptr = new Squid(X, Y);
	lMinion[getListID(X)].push_back(ptr);
	return ptr;
}

void Map::addHammer(int X, int Y, bool moveDirection) {
	lMinion[getListID(X)].push_back(new Hammer(X, Y, moveDirection));
}

HammerBro* Map::addHammerBro(int X, int Y) {
	auto ptr = new HammerBro(X, Y);
	lMinion[getListID(X)].push_back(ptr);
	return ptr;
}

void Map::addFireBall(int X, int Y, int iWidth, int iSliceID, bool DIR) {
	for(int i = 0; i < iWidth; i++) {
		lMinion[getListID((int)X)].push_back(new FireBall(X + 8, Y + 8, 14*i, iSliceID, DIR));
	}
}

Spikey* Map::addSpikey(int X, int Y) {
	auto ptr = new Spikey(X, Y);
	lMinion[getListID(X)].push_back(ptr);
	return ptr;
}

void Map::addPlayerFireBall(int X, int Y, bool moveDirection) {
	lMinion[getListID(X)].push_back(new PlayerFireBall(X, Y, moveDirection));
}

Bowser* Map::addBowser(int X, int Y, bool spawnHammer) {
	auto ptr = new Bowser((float)X, (float)Y, spawnHammer);
	lMinion[getListID(X)].push_back(ptr);
	return ptr;
}

void Map::addUpFire(int X, int iYEnd) {
	lMinion[getListID(X)].push_back(new UpFire(X, iYEnd));
}

void Map::addFire(float fX, float fY, int toYPos) {
	lMinion[getListID((int)fX)].push_back(new Fire(fX, fY, toYPos));
}

void Map::addCheep(int X, int Y, int minionType, int moveSpeed, bool moveDirection) {
	lMinion[getListID(X)].push_back(new Cheep(X, Y, minionType, moveSpeed, moveDirection));
}

void Map::addCheepSpawner(int X, int XEnd) {
	lMinion[getListID(X)].push_back(new CheepSpawner(X, XEnd));
}

void Map::addBubble(int X, int Y) {
	lBubble.push_back(new Bubble(X, Y));
}

void Map::addLakito(int X, int Y, int iMaxXPos) {
	lMinion[getListID(X)].push_back(new Lakito(X, Y, iMaxXPos));
}

void Map::addVine(int X, int Y, int minionState, int iBlockID) {
	lMinion[getListID(X)].push_back(new Vine(X, Y, minionState, iBlockID));
	if(minionState == 0) {
		CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cVINE);
	}
}

void Map::addSpring(int X, int Y) {
	lMinion[getListID(X)].push_back(new Spring(X, Y));
	//lMap[X/32][(CCFG::GAME_HEIGHT - 16 - Y)/32 - 1]->setBlockID(83);
}

void Map::addBulletBillSpawner(int X, int Y, int minionState) {
	lMinion[getListID(X*32)].push_back(new BulletBillSpawner(X*32, CCFG::GAME_HEIGHT - Y*32, minionState));
}

void Map::addBulletBill(int X, int Y, bool moveDirection, int minionState) {
	lMinion[getListID(X)].push_back(new BulletBill(X, Y, moveDirection, minionState));
}

void Map::lockMinions() {
	for(unsigned int i = 0; i < lMinion.size(); i++) {
		for(unsigned int j = 0; j < lMinion[i].size(); j++) {
			lMinion[i][j]->lockMinion();
		}
	}
}
void Map::addText(int X, int Y, std::string sText) {
	vLevelText.push_back(new LevelText(X, Y, sText));
}

int Map::getNumOfMinions() {
	int iOutput = 0;

	for(int i = 0, size = lMinion.size(); i < size; i++) {
		iOutput += lMinion[i].size();
	}

	return iOutput;
}

/* ----------------- MINIONS ----------------- */
/* ******************************************** */

/* ---------- LOAD GAME DATA ---------- */

void Map::loadGameData(SDL_Renderer* rR) {
	std::vector<std::string> tSprite;
	std::vector<unsigned int> iDelay;

	// ----- uninext background -----
	tSprite.push_back("background");
	iDelay.push_back(0);
	this->background = new Sprite(rR, tSprite, iDelay, false);
	tSprite.clear();
	iDelay.clear();

	
	// ----- 0 Transparent -----
	tSprite.push_back("transp");
	iDelay.push_back(0);
	vBlock.push_back(new Block(0, new Sprite(rR, tSprite, iDelay, false), false, true, false, false));
	tSprite.clear();
	iDelay.clear();
	// ----- 1 -----
	tSprite.push_back("gnd_red_1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(1, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 2 -----
	tSprite.push_back("coin_0");
	iDelay.push_back(300);
	tSprite.push_back("coin_2");
	iDelay.push_back(30);
	tSprite.push_back("coin_1");
	iDelay.push_back(130);
	tSprite.push_back("coin_2");
	iDelay.push_back(140);
	vBlock.push_back(new Block(2, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 3 -----
	tSprite.push_back("bush_center_0");
	iDelay.push_back(0);
	vBlock.push_back(new Block(3, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 4 -----
	tSprite.push_back("bush_center_1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(4, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 5 -----
	tSprite.push_back("bush_left");
	iDelay.push_back(0);
	vBlock.push_back(new Block(5, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 6 -----
	tSprite.push_back("bush_right");
	iDelay.push_back(0);
	vBlock.push_back(new Block(6, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 7 -----
	tSprite.push_back("bush_top");
	iDelay.push_back(0);
	vBlock.push_back(new Block(7, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 8 -----
	tSprite.push_back("blockq_0");
	iDelay.push_back(300);
	tSprite.push_back("blockq_2");
	iDelay.push_back(30);
	tSprite.push_back("blockq_1");
	iDelay.push_back(130);
	tSprite.push_back("blockq_2");
	iDelay.push_back(140);
	vBlock.push_back(new Block(8, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 9 -----
	tSprite.push_back("blockq_used");
	iDelay.push_back(0);
	vBlock.push_back(new Block(9, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 10 -----
	tSprite.push_back("grass_left");
	iDelay.push_back(0);
	vBlock.push_back(new Block(10, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 11 -----
	tSprite.push_back("grass_center");
	iDelay.push_back(0);
	vBlock.push_back(new Block(11, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 12 -----
	tSprite.push_back("grass_right");
	iDelay.push_back(0);
	vBlock.push_back(new Block(12, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 13 -----
	tSprite.push_back("brickred");
	iDelay.push_back(0);
	vBlock.push_back(new Block(13, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 14 -----
	tSprite.push_back("cloud_left_bot");
	iDelay.push_back(0);
	vBlock.push_back(new Block(14, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 15 -----
	tSprite.push_back("cloud_left_top");
	iDelay.push_back(0);
	vBlock.push_back(new Block(15, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 16 -----
	tSprite.push_back("cloud_center_bot");
	iDelay.push_back(0);
	vBlock.push_back(new Block(16, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 17 -----
	tSprite.push_back("cloud_center_top");
	iDelay.push_back(0);
	vBlock.push_back(new Block(17, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 18 -----
	tSprite.push_back("cloud_right_bot");
	iDelay.push_back(0);
	vBlock.push_back(new Block(18, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 19 -----
	tSprite.push_back("cloud_right_top");
	iDelay.push_back(0);
	vBlock.push_back(new Block(19, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 20 -----
	tSprite.push_back("pipe_left_bot");
	iDelay.push_back(0);
	vBlock.push_back(new Block(20, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 21 -----
	tSprite.push_back("pipe_left_top");
	iDelay.push_back(0);
	vBlock.push_back(new Block(21, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 22 -----
	tSprite.push_back("pipe_right_bot");
	iDelay.push_back(0);
	vBlock.push_back(new Block(22, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 23 -----
	tSprite.push_back("pipe_right_top");
	iDelay.push_back(0);
	vBlock.push_back(new Block(23, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 24 BlockQ2 -----
	tSprite.push_back("transp");
	iDelay.push_back(0);
	vBlock.push_back(new Block(24, new Sprite(rR, tSprite, iDelay, false), true, false, true, false));
	tSprite.clear();
	iDelay.clear();
	// ----- 25 -----
	tSprite.push_back("gnd_red2");
	iDelay.push_back(0);
	vBlock.push_back(new Block(25, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 26 -----
	tSprite.push_back("gnd1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(26, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 27 -----
	tSprite.push_back("gnd1_2");
	iDelay.push_back(0);
	vBlock.push_back(new Block(27, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 28 -----
	tSprite.push_back("brick1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(28, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 29 -----
	tSprite.push_back("coin_use0");
	iDelay.push_back(300);
	tSprite.push_back("coin_use2");
	iDelay.push_back(30);
	tSprite.push_back("coin_use1");
	iDelay.push_back(130);
	tSprite.push_back("coin_use2");
	iDelay.push_back(140);
	vBlock.push_back(new Block(29, new Sprite(rR, tSprite, iDelay, false), false, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 30 -----
	tSprite.push_back("pipe1_left_bot");
	iDelay.push_back(0);
	vBlock.push_back(new Block(30, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 31 -----
	tSprite.push_back("pipe1_left_top");
	iDelay.push_back(0);
	vBlock.push_back(new Block(31, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 32 -----
	tSprite.push_back("pipe1_right_bot");
	iDelay.push_back(0);
	vBlock.push_back(new Block(32, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 33 -----
	tSprite.push_back("pipe1_right_top");
	iDelay.push_back(0);
	vBlock.push_back(new Block(33, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 34 -----
	tSprite.push_back("pipe1_hor_bot_right");
	iDelay.push_back(0);
	vBlock.push_back(new Block(34, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 35 -----
	tSprite.push_back("pipe1_hor_top_center");
	iDelay.push_back(0);
	vBlock.push_back(new Block(35, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 36 -----
	tSprite.push_back("pipe1_hor_top_left");
	iDelay.push_back(0);
	vBlock.push_back(new Block(36, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 37 -----
	tSprite.push_back("pipe1_hor_bot_center");
	iDelay.push_back(0);
	vBlock.push_back(new Block(37, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 38 -----
	tSprite.push_back("pipe1_hor_bot_left");
	iDelay.push_back(0);
	vBlock.push_back(new Block(38, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 39 -----
	tSprite.push_back("pipe1_hor_top_right");
	iDelay.push_back(0);
	vBlock.push_back(new Block(39, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 40 -----
	tSprite.push_back("end0_l");
	iDelay.push_back(0);
	vBlock.push_back(new Block(40, new Sprite(rR, tSprite, iDelay, false), false, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 41 -----
	tSprite.push_back("end0_dot");
	iDelay.push_back(0);
	vBlock.push_back(new Block(41, new Sprite(rR, tSprite, iDelay, false), false, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 42 -----
	tSprite.push_back("end0_flag");
	iDelay.push_back(0);
	vBlock.push_back(new Block(42, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 43 -----
	tSprite.push_back("castle0_brick");
	iDelay.push_back(0);
	vBlock.push_back(new Block(43, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 44 -----
	tSprite.push_back("castle0_top0");
	iDelay.push_back(0);
	vBlock.push_back(new Block(44, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 45 -----
	tSprite.push_back("castle0_top1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(45, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 46 -----
	tSprite.push_back("castle0_center_center_top");
	iDelay.push_back(0);
	vBlock.push_back(new Block(46, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 47 -----
	tSprite.push_back("castle0_center_center");
	iDelay.push_back(0);
	vBlock.push_back(new Block(47, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 48 -----
	tSprite.push_back("castle0_center_left");
	iDelay.push_back(0);
	vBlock.push_back(new Block(48, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 49 -----
	tSprite.push_back("castle0_center_right");
	iDelay.push_back(0);
	vBlock.push_back(new Block(49, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 50 -----
	tSprite.push_back("coin_an0");
	iDelay.push_back(0);
	tSprite.push_back("coin_an1");
	iDelay.push_back(0);
	tSprite.push_back("coin_an2");
	iDelay.push_back(0);
	tSprite.push_back("coin_an3");
	iDelay.push_back(0);
	vBlock.push_back(new Block(50, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 51 -----
	tSprite.push_back("castle_flag");
	iDelay.push_back(0);
	vBlock.push_back(new Block(51, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 52 -----
	tSprite.push_back("firework0");
	iDelay.push_back(0);
	vBlock.push_back(new Block(52, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 53 -----
	tSprite.push_back("firework1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(53, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 54 -----
	tSprite.push_back("firework2");
	iDelay.push_back(0);
	vBlock.push_back(new Block(54, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 55 -----
	tSprite.push_back("blockq1_0");
	iDelay.push_back(300);
	tSprite.push_back("blockq1_2");
	iDelay.push_back(30);
	tSprite.push_back("blockq1_1");
	iDelay.push_back(130);
	tSprite.push_back("blockq1_2");
	iDelay.push_back(140);
	vBlock.push_back(new Block(55, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 56 -----
	tSprite.push_back("blockq1_used");
	iDelay.push_back(0);
	vBlock.push_back(new Block(56, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 57 -----
	tSprite.push_back("coin1_0");
	iDelay.push_back(300);
	tSprite.push_back("coin1_2");
	iDelay.push_back(30);
	tSprite.push_back("coin1_1");
	iDelay.push_back(130);
	tSprite.push_back("coin1_2");
	iDelay.push_back(140);
	vBlock.push_back(new Block(57, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 58 -----
	tSprite.push_back("pipe_hor_bot_right");
	iDelay.push_back(0);
	vBlock.push_back(new Block(58, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 59 -----
	tSprite.push_back("pipe_hor_top_center");
	iDelay.push_back(0);
	vBlock.push_back(new Block(59, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 60 -----
	tSprite.push_back("pipe_hor_top_left");
	iDelay.push_back(0);
	vBlock.push_back(new Block(60, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 61 -----
	tSprite.push_back("pipe_hor_bot_center");
	iDelay.push_back(0);
	vBlock.push_back(new Block(61, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 62 -----
	tSprite.push_back("pipe_hor_bot_left");
	iDelay.push_back(0);
	vBlock.push_back(new Block(62, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 63 -----
	tSprite.push_back("pipe_hor_top_right");
	iDelay.push_back(0);
	vBlock.push_back(new Block(63, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 64 -----
	tSprite.push_back("block_debris0");
	iDelay.push_back(0);
	vBlock.push_back(new Block(64, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 65 -----
	tSprite.push_back("block_debris1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(65, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 66 -----
	tSprite.push_back("block_debris2");
	iDelay.push_back(0);
	vBlock.push_back(new Block(66, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 67 -----
	tSprite.push_back("t_left");
	iDelay.push_back(0);
	vBlock.push_back(new Block(67, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 68 -----
	tSprite.push_back("t_center");
	iDelay.push_back(0);
	vBlock.push_back(new Block(68, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 69 -----
	tSprite.push_back("t_right");
	iDelay.push_back(0);
	vBlock.push_back(new Block(69, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 70 -----
	tSprite.push_back("t_bot");
	iDelay.push_back(0);
	vBlock.push_back(new Block(70, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 71 -----
	tSprite.push_back("coin_use00");
	iDelay.push_back(300);
	tSprite.push_back("coin_use02");
	iDelay.push_back(30);
	tSprite.push_back("coin_use01");
	iDelay.push_back(130);
	tSprite.push_back("coin_use02");
	iDelay.push_back(140);
	vBlock.push_back(new Block(71, new Sprite(rR, tSprite, iDelay, false), false, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 72 -----
	tSprite.push_back("coin_use20");
	iDelay.push_back(300);
	tSprite.push_back("coin_use22");
	iDelay.push_back(30);
	tSprite.push_back("coin_use21");
	iDelay.push_back(130);
	tSprite.push_back("coin_use22");
	iDelay.push_back(140);
	vBlock.push_back(new Block(72, new Sprite(rR, tSprite, iDelay, false), false, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 73 -----
	tSprite.push_back("coin_use30");
	iDelay.push_back(300);
	tSprite.push_back("coin_use32");
	iDelay.push_back(30);
	tSprite.push_back("coin_use31");
	iDelay.push_back(130);
	tSprite.push_back("coin_use32");
	iDelay.push_back(140);
	vBlock.push_back(new Block(73, new Sprite(rR, tSprite, iDelay, false), false, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 74 -----
	tSprite.push_back("platform");
	iDelay.push_back(0);
	vBlock.push_back(new Block(74, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 75 -----
	tSprite.push_back("gnd_4");
	iDelay.push_back(0);
	vBlock.push_back(new Block(75, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 76 -----
	tSprite.push_back("bridge_0");
	iDelay.push_back(0);
	vBlock.push_back(new Block(76, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 77 -----
	tSprite.push_back("lava_0");
	iDelay.push_back(0);
	vBlock.push_back(new Block(77, new Sprite(rR, tSprite, iDelay, false), false, true, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 78 -----
	tSprite.push_back("lava_1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(78, new Sprite(rR, tSprite, iDelay, false), false, true, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 79 -----
	tSprite.push_back("bridge_1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(78, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 80 -----
	tSprite.push_back("blockq2_used");
	iDelay.push_back(0);
	vBlock.push_back(new Block(80, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 81 -----
	tSprite.push_back("brick2");
	iDelay.push_back(0);
	vBlock.push_back(new Block(81, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 82 -----
	tSprite.push_back("axe_0");
	iDelay.push_back(225);
	tSprite.push_back("axe_1");
	iDelay.push_back(225);
	tSprite.push_back("axe_2");
	iDelay.push_back(200);
	vBlock.push_back(new Block(82, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 83 ----- END BRIDGE
	tSprite.push_back("transp");
	iDelay.push_back(0);
	vBlock.push_back(new Block(83, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 84 -----
	tSprite.push_back("tree_1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(84, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 85 -----
	tSprite.push_back("tree_2");
	iDelay.push_back(0);
	vBlock.push_back(new Block(85, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 86 -----
	tSprite.push_back("tree_3");
	iDelay.push_back(0);
	vBlock.push_back(new Block(86, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 87 -----
	tSprite.push_back("tree1_1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(87, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 88 -----
	tSprite.push_back("tree1_2");
	iDelay.push_back(0);
	vBlock.push_back(new Block(88, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 89 -----
	tSprite.push_back("tree1_3");
	iDelay.push_back(0);
	vBlock.push_back(new Block(89, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 90 -----
	tSprite.push_back("fence");
	iDelay.push_back(0);
	vBlock.push_back(new Block(90, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 91 -----
	tSprite.push_back("tree_0");
	iDelay.push_back(0);
	vBlock.push_back(new Block(91, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 92 -----
	tSprite.push_back("uw_0");
	iDelay.push_back(0);
	vBlock.push_back(new Block(92, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 93 -----
	tSprite.push_back("uw_1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(93, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 94 -----
	tSprite.push_back("water_0");
	iDelay.push_back(0);
	vBlock.push_back(new Block(94, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 95 -----
	tSprite.push_back("water_1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(95, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 96 -----
	tSprite.push_back("bubble");
	iDelay.push_back(0);
	vBlock.push_back(new Block(96, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 97 -----
	tSprite.push_back("pipe2_left_bot");
	iDelay.push_back(0);
	vBlock.push_back(new Block(97, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 98 -----
	tSprite.push_back("pipe2_left_top");
	iDelay.push_back(0);
	vBlock.push_back(new Block(98, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 99 -----
	tSprite.push_back("pipe2_right_bot");
	iDelay.push_back(0);
	vBlock.push_back(new Block(99, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 100 -----
	tSprite.push_back("pipe2_right_top");
	iDelay.push_back(0);
	vBlock.push_back(new Block(100, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 101 -----
	tSprite.push_back("pipe2_hor_bot_right");
	iDelay.push_back(0);
	vBlock.push_back(new Block(101, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 102 -----
	tSprite.push_back("pipe2_hor_top_center");
	iDelay.push_back(0);
	vBlock.push_back(new Block(102, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 103 -----
	tSprite.push_back("pipe2_hor_top_left");
	iDelay.push_back(0);
	vBlock.push_back(new Block(103, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 104 -----
	tSprite.push_back("pipe2_hor_bot_center");
	iDelay.push_back(0);
	vBlock.push_back(new Block(104, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 105 -----
	tSprite.push_back("pipe2_hor_bot_left");
	iDelay.push_back(0);
	vBlock.push_back(new Block(105, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 106 -----
	tSprite.push_back("pipe2_hor_top_right");
	iDelay.push_back(0);
	vBlock.push_back(new Block(106, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 107 -----
	tSprite.push_back("bridge2");
	iDelay.push_back(0);
	vBlock.push_back(new Block(107, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 108 -----
	tSprite.push_back("bridge3");
	iDelay.push_back(0);
	vBlock.push_back(new Block(108, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 109 -----
	tSprite.push_back("platform1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(109, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 110 -----
	tSprite.push_back("water_2");
	iDelay.push_back(0);
	vBlock.push_back(new Block(110, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 111 -----
	tSprite.push_back("water_3");
	iDelay.push_back(0);
	vBlock.push_back(new Block(111, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 112 -----
	tSprite.push_back("pipe3_left_bot");
	iDelay.push_back(0);
	vBlock.push_back(new Block(112, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 113 -----
	tSprite.push_back("pipe3_left_top");
	iDelay.push_back(0);
	vBlock.push_back(new Block(113, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 114 -----
	tSprite.push_back("pipe3_right_bot");
	iDelay.push_back(0);
	vBlock.push_back(new Block(114, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 115 -----
	tSprite.push_back("pipe3_right_top");
	iDelay.push_back(0);
	vBlock.push_back(new Block(115, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 116 -----
	tSprite.push_back("pipe3_hor_bot_right");
	iDelay.push_back(0);
	vBlock.push_back(new Block(116, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 117 -----
	tSprite.push_back("pipe3_hor_top_center");
	iDelay.push_back(0);
	vBlock.push_back(new Block(117, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 118 -----
	tSprite.push_back("pipe3_hor_top_left");
	iDelay.push_back(0);
	vBlock.push_back(new Block(118, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 119 -----
	tSprite.push_back("pipe3_hor_bot_center");
	iDelay.push_back(0);
	vBlock.push_back(new Block(119, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 120 -----
	tSprite.push_back("pipe3_hor_bot_left");
	iDelay.push_back(0);
	vBlock.push_back(new Block(120, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 121 -----
	tSprite.push_back("pipe3_hor_top_right");
	iDelay.push_back(0);
	vBlock.push_back(new Block(121, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 122 -----
	tSprite.push_back("bridge4");
	iDelay.push_back(0);
	vBlock.push_back(new Block(122, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 123 -----
	tSprite.push_back("end1_l");
	iDelay.push_back(0);
	vBlock.push_back(new Block(123, new Sprite(rR, tSprite, iDelay, false), false, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 124 -----
	tSprite.push_back("end1_dot");
	iDelay.push_back(0);
	vBlock.push_back(new Block(124, new Sprite(rR, tSprite, iDelay, false), false, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 125 -----
	tSprite.push_back("bonus");
	iDelay.push_back(0);
	vBlock.push_back(new Block(125, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 126 -----
	tSprite.push_back("platformbonus");
	iDelay.push_back(0);
	vBlock.push_back(new Block(126, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 127 ----- // -- BONUS END
	tSprite.push_back("transp");
	iDelay.push_back(0);
	vBlock.push_back(new Block(127, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 128 ----- // -- SPAWN VINE
	tSprite.push_back("brickred");
	iDelay.push_back(0);
	vBlock.push_back(new Block(128, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 129 ----- // -- SPAWN VINE
	tSprite.push_back("brick1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(129, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 130 -----
	tSprite.push_back("vine");
	iDelay.push_back(0);
	vBlock.push_back(new Block(130, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 131 -----
	tSprite.push_back("vine1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(131, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 132 -----
	tSprite.push_back("seesaw_0");
	iDelay.push_back(0);
	vBlock.push_back(new Block(132, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 133 -----
	tSprite.push_back("seesaw_1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(133, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 134 -----
	tSprite.push_back("seesaw_2");
	iDelay.push_back(0);
	vBlock.push_back(new Block(134, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 135 -----
	tSprite.push_back("seesaw_3");
	iDelay.push_back(0);
	vBlock.push_back(new Block(135, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 136 -----
	tSprite.push_back("pipe4_left_bot");
	iDelay.push_back(0);
	vBlock.push_back(new Block(136, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 137 -----
	tSprite.push_back("pipe4_left_top");
	iDelay.push_back(0);
	vBlock.push_back(new Block(137, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 138 -----
	tSprite.push_back("pipe4_right_bot");
	iDelay.push_back(0);
	vBlock.push_back(new Block(138, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 139 -----
	tSprite.push_back("pipe4_right_top");
	iDelay.push_back(0);
	vBlock.push_back(new Block(139, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 140 -----
	tSprite.push_back("t_left1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(140, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 141 -----
	tSprite.push_back("t_center1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(141, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 142 -----
	tSprite.push_back("t_right1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(142, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 143 -----
	tSprite.push_back("t_bot0");
	iDelay.push_back(0);
	vBlock.push_back(new Block(143, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 144 -----
	tSprite.push_back("t_bot1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(144, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 145 -----
	tSprite.push_back("b_top");
	iDelay.push_back(0);
	vBlock.push_back(new Block(145, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 146 -----
	tSprite.push_back("b_top1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(146, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 147 -----
	tSprite.push_back("b_bot");
	iDelay.push_back(0);
	vBlock.push_back(new Block(147, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 148 -----
	tSprite.push_back("cloud_left_bot1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(148, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 149 -----
	tSprite.push_back("cloud_center_bot1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(149, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 150 -----
	tSprite.push_back("cloud_center_top1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(150, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 151 -----
	tSprite.push_back("t_left2");
	iDelay.push_back(0);
	vBlock.push_back(new Block(151, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 152 -----
	tSprite.push_back("t_center2");
	iDelay.push_back(0);
	vBlock.push_back(new Block(152, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 153 -----
	tSprite.push_back("t_right2");
	iDelay.push_back(0);
	vBlock.push_back(new Block(153, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 154 -----
	tSprite.push_back("t_bot2");
	iDelay.push_back(0);
	vBlock.push_back(new Block(154, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 155 -----
	tSprite.push_back("castle1_brick");
	iDelay.push_back(0);
	vBlock.push_back(new Block(155, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 156 -----
	tSprite.push_back("castle1_top0");
	iDelay.push_back(0);
	vBlock.push_back(new Block(156, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 157 -----
	tSprite.push_back("castle1_top1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(157, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 158 -----
	tSprite.push_back("castle1_center_center_top");
	iDelay.push_back(0);
	vBlock.push_back(new Block(158, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 159 -----
	tSprite.push_back("castle1_center_center");
	iDelay.push_back(0);
	vBlock.push_back(new Block(159, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 160 -----
	tSprite.push_back("castle1_center_left");
	iDelay.push_back(0);
	vBlock.push_back(new Block(160, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 161 -----
	tSprite.push_back("castle1_center_right");
	iDelay.push_back(0);
	vBlock.push_back(new Block(161, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 162 -----
	tSprite.push_back("seesaw1_0");
	iDelay.push_back(0);
	vBlock.push_back(new Block(162, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 163 -----
	tSprite.push_back("seesaw1_1");
	iDelay.push_back(0);
	vBlock.push_back(new Block(163, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 164 -----
	tSprite.push_back("seesaw1_2");
	iDelay.push_back(0);
	vBlock.push_back(new Block(164, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 165 -----
	tSprite.push_back("seesaw1_3");
	iDelay.push_back(0);
	vBlock.push_back(new Block(165, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 166 -----
	tSprite.push_back("gnd2");
	iDelay.push_back(0);
	vBlock.push_back(new Block(166, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 167 -----
	tSprite.push_back("gnd2_2");
	iDelay.push_back(0);
	vBlock.push_back(new Block(167, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 168 -----
	tSprite.push_back("end1_flag");
	iDelay.push_back(0);
	vBlock.push_back(new Block(168, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 169 ----- TP
	tSprite.push_back("transp");
	iDelay.push_back(0);
	vBlock.push_back(new Block(169, new Sprite(rR, tSprite, iDelay, false), false, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 170 ----- TP2
	tSprite.push_back("transp");
	iDelay.push_back(0);
	vBlock.push_back(new Block(170, new Sprite(rR, tSprite, iDelay, false), false, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 171 ----- TP3 - bTP
	tSprite.push_back("transp");
	iDelay.push_back(0);
	vBlock.push_back(new Block(171, new Sprite(rR, tSprite, iDelay, false), false, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 172 -----
	tSprite.push_back("pipe5_left_bot");
	iDelay.push_back(0);
	vBlock.push_back(new Block(172, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 173 -----
	tSprite.push_back("pipe5_left_top");
	iDelay.push_back(0);
	vBlock.push_back(new Block(173, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 174 -----
	tSprite.push_back("pipe5_right_bot");
	iDelay.push_back(0);
	vBlock.push_back(new Block(174, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 175 -----
	tSprite.push_back("pipe5_right_top");
	iDelay.push_back(0);
	vBlock.push_back(new Block(175, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 176 -----
	tSprite.push_back("pipe6_left_bot");
	iDelay.push_back(0);
	vBlock.push_back(new Block(176, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 177 -----
	tSprite.push_back("pipe6_left_top");
	iDelay.push_back(0);
	vBlock.push_back(new Block(177, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 178 -----
	tSprite.push_back("pipe6_right_bot");
	iDelay.push_back(0);
	vBlock.push_back(new Block(178, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 179 -----
	tSprite.push_back("pipe6_right_top");
	iDelay.push_back(0);
	vBlock.push_back(new Block(179, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 180 -----
	tSprite.push_back("crown");
	iDelay.push_back(0);
	vBlock.push_back(new Block(180, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 181 -----
	tSprite.push_back("gnd_5");
	iDelay.push_back(0);
	vBlock.push_back(new Block(166, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 182 ----- ENDUSE
	tSprite.push_back("transp");
	iDelay.push_back(0);
	vBlock.push_back(new Block(182, new Sprite(rR, tSprite, iDelay, false), false, false, true, true));
	tSprite.clear();
	iDelay.clear();


	// --------------- MINION ---------------

	// ----- 0 -----
	tSprite.push_back("goombas_0");
	iDelay.push_back(200);
	tSprite.push_back("goombas_1");
	iDelay.push_back(200);
	vMinion.push_back(new Block(0, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 1 -----
	tSprite.push_back("goombas_ded");
	iDelay.push_back(0);
	vMinion.push_back(new Block(1, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 2 -----
	tSprite.push_back("mushroom");
	iDelay.push_back(0);
	vMinion.push_back(new Block(2, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 3 -----
	tSprite.push_back("mushroom_1up");
	iDelay.push_back(0);
	vMinion.push_back(new Block(3, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 4 -----
	tSprite.push_back("koopa_0");
	iDelay.push_back(200);
	tSprite.push_back("koopa_1");
	iDelay.push_back(200);
	vMinion.push_back(new Block(4, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 5 -----
	tSprite.push_back("koopa_ded");
	iDelay.push_back(0);
	vMinion.push_back(new Block(5, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 6 -----
	tSprite.push_back("flower0");
	iDelay.push_back(50);
	tSprite.push_back("flower1");
	iDelay.push_back(50);
	tSprite.push_back("flower2");
	iDelay.push_back(50);
	tSprite.push_back("flower3");
	iDelay.push_back(50);
	vMinion.push_back(new Block(6, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 7 -----
	tSprite.push_back("koopa_2");
	iDelay.push_back(200);
	tSprite.push_back("koopa_3");
	iDelay.push_back(200);
	vMinion.push_back(new Block(7, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 8 -----
	tSprite.push_back("goombas1_0");
	iDelay.push_back(200);
	tSprite.push_back("goombas1_1");
	iDelay.push_back(200);
	vMinion.push_back(new Block(8, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 9 -----
	tSprite.push_back("goombas1_ded");
	iDelay.push_back(0);
	vMinion.push_back(new Block(9, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 10 -----
	tSprite.push_back("goombas2_0");
	iDelay.push_back(200);
	tSprite.push_back("goombas2_1");
	iDelay.push_back(200);
	vMinion.push_back(new Block(10, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 11 -----
	tSprite.push_back("goombas2_ded");
	iDelay.push_back(0);
	vMinion.push_back(new Block(11, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 12 -----
	tSprite.push_back("koopa1_0");
	iDelay.push_back(200);
	tSprite.push_back("koopa1_1");
	iDelay.push_back(200);
	vMinion.push_back(new Block(12, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 13 -----
	tSprite.push_back("koopa1_ded");
	iDelay.push_back(0);
	vMinion.push_back(new Block(13, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 14 -----
	tSprite.push_back("koopa1_2");
	iDelay.push_back(200);
	tSprite.push_back("koopa1_3");
	iDelay.push_back(200);
	vMinion.push_back(new Block(14, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 15 -----
	tSprite.push_back("koopa2_0");
	iDelay.push_back(200);
	tSprite.push_back("koopa2_1");
	iDelay.push_back(200);
	vMinion.push_back(new Block(15, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 16 -----
	tSprite.push_back("koopa2_ded");
	iDelay.push_back(0);
	vMinion.push_back(new Block(16, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 17 -----
	tSprite.push_back("koopa2_2");
	iDelay.push_back(200);
	tSprite.push_back("koopa2_3");
	iDelay.push_back(200);
	vMinion.push_back(new Block(17, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 18 -----
	tSprite.push_back("plant_0");
	iDelay.push_back(125);
	tSprite.push_back("plant_1");
	iDelay.push_back(125);
	vMinion.push_back(new Block(18, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 19 -----
	tSprite.push_back("plant1_0");
	iDelay.push_back(125);
	tSprite.push_back("plant1_1");
	iDelay.push_back(125);
	vMinion.push_back(new Block(19, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 20 -----
	tSprite.push_back("bowser0");
	iDelay.push_back(285);
	tSprite.push_back("bowser1");
	iDelay.push_back(285);
	vMinion.push_back(new Block(20, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 21 -----
	tSprite.push_back("bowser2");
	iDelay.push_back(285);
	tSprite.push_back("bowser3");
	iDelay.push_back(285);
	vMinion.push_back(new Block(21, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 22 -----
	tSprite.push_back("fire_0");
	iDelay.push_back(35);
	tSprite.push_back("fire_1");
	iDelay.push_back(35);
	vMinion.push_back(new Block(22, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 23 -----
	tSprite.push_back("fireball_0");
	iDelay.push_back(75);
	tSprite.push_back("fireball_1");
	iDelay.push_back(75);
	tSprite.push_back("fireball_2");
	iDelay.push_back(75);
	tSprite.push_back("fireball_3");
	iDelay.push_back(75);
	vMinion.push_back(new Block(23, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 24 -----
	tSprite.push_back("star_0");
	iDelay.push_back(75);
	tSprite.push_back("star_1");
	iDelay.push_back(75);
	tSprite.push_back("star_2");
	iDelay.push_back(75);
	tSprite.push_back("star_3");
	iDelay.push_back(75);
	vMinion.push_back(new Block(24, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 25 -----
	tSprite.push_back("mushroom1_1up");
	iDelay.push_back(0);
	vMinion.push_back(new Block(25, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 26 -----
	tSprite.push_back("toad");
	iDelay.push_back(0);
	vMinion.push_back(new Block(26, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 27 -----
	tSprite.push_back("peach");
	iDelay.push_back(0);
	vMinion.push_back(new Block(27, new Sprite(rR, tSprite, iDelay, false), false, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 28 -----
	tSprite.push_back("squid0");
	iDelay.push_back(0);
	vMinion.push_back(new Block(28, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 29 -----
	tSprite.push_back("squid1");
	iDelay.push_back(0);
	vMinion.push_back(new Block(29, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 30 -----
	tSprite.push_back("cheep0");
	iDelay.push_back(120);
	tSprite.push_back("cheep1");
	iDelay.push_back(120);
	vMinion.push_back(new Block(30, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 31 -----
	tSprite.push_back("cheep2");
	iDelay.push_back(110);
	tSprite.push_back("cheep3");
	iDelay.push_back(110);
	vMinion.push_back(new Block(31, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 32 -----
	tSprite.push_back("upfire");
	iDelay.push_back(0);
	vMinion.push_back(new Block(32, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 33 -----
	tSprite.push_back("vine_top");
	iDelay.push_back(0);
	vMinion.push_back(new Block(33, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 34 -----
	tSprite.push_back("vine");
	iDelay.push_back(0);
	vMinion.push_back(new Block(34, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 35 -----
	tSprite.push_back("vine1_top");
	iDelay.push_back(0);
	vMinion.push_back(new Block(35, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 36 -----
	tSprite.push_back("vine1");
	iDelay.push_back(0);
	vMinion.push_back(new Block(36, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 37 -----
	tSprite.push_back("spring_0");
	iDelay.push_back(0);
	vMinion.push_back(new Block(37, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 38 -----
	tSprite.push_back("spring_1");
	iDelay.push_back(0);
	vMinion.push_back(new Block(38, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 39 -----
	tSprite.push_back("spring_2");
	iDelay.push_back(0);
	vMinion.push_back(new Block(39, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 40 -----
	tSprite.push_back("spring1_0");
	iDelay.push_back(0);
	vMinion.push_back(new Block(40, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 41 -----
	tSprite.push_back("spring1_1");
	iDelay.push_back(0);
	vMinion.push_back(new Block(41, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 42 -----
	tSprite.push_back("spring1_2");
	iDelay.push_back(0);
	vMinion.push_back(new Block(42, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 43 -----
	tSprite.push_back("hammerbro_0");
	iDelay.push_back(175);
	tSprite.push_back("hammerbro_1");
	iDelay.push_back(175);
	vMinion.push_back(new Block(43, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 44 -----
	tSprite.push_back("hammerbro_2");
	iDelay.push_back(155);
	tSprite.push_back("hammerbro_3");
	iDelay.push_back(155);
	vMinion.push_back(new Block(44, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 45 -----
	tSprite.push_back("hammerbro1_0");
	iDelay.push_back(175);
	tSprite.push_back("hammerbro1_1");
	iDelay.push_back(175);
	vMinion.push_back(new Block(45, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 46 -----
	tSprite.push_back("hammerbro1_2");
	iDelay.push_back(155);
	tSprite.push_back("hammerbro1_3");
	iDelay.push_back(155);
	vMinion.push_back(new Block(46, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 47 -----
	tSprite.push_back("hammer_0");
	iDelay.push_back(95);
	tSprite.push_back("hammer_1");
	iDelay.push_back(95);
	tSprite.push_back("hammer_2");
	iDelay.push_back(95);
	tSprite.push_back("hammer_3");
	iDelay.push_back(95);
	vMinion.push_back(new Block(47, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 48 -----
	tSprite.push_back("hammer1_0");
	iDelay.push_back(95);
	tSprite.push_back("hammer1_1");
	iDelay.push_back(95);
	tSprite.push_back("hammer1_2");
	iDelay.push_back(95);
	tSprite.push_back("hammer1_3");
	iDelay.push_back(95);
	vMinion.push_back(new Block(48, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 49 -----
	tSprite.push_back("lakito_0");
	iDelay.push_back(0);
	vMinion.push_back(new Block(49, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 50 -----
	tSprite.push_back("lakito_1");
	iDelay.push_back(0);
	vMinion.push_back(new Block(50, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 51 -----
	tSprite.push_back("spikey0_0");
	iDelay.push_back(135);
	tSprite.push_back("spikey0_1");
	iDelay.push_back(135);
	vMinion.push_back(new Block(51, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 52 -----
	tSprite.push_back("spikey1_0");
	iDelay.push_back(75);
	tSprite.push_back("spikey1_1");
	iDelay.push_back(75);
	vMinion.push_back(new Block(52, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 53 -----
	tSprite.push_back("beetle_0");
	iDelay.push_back(155);
	tSprite.push_back("beetle_1");
	iDelay.push_back(155);
	vMinion.push_back(new Block(53, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 54 -----
	tSprite.push_back("beetle_2");
	iDelay.push_back(0);
	vMinion.push_back(new Block(54, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 55 -----
	tSprite.push_back("beetle1_0");
	iDelay.push_back(155);
	tSprite.push_back("beetle1_1");
	iDelay.push_back(155);
	vMinion.push_back(new Block(55, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 56 -----
	tSprite.push_back("beetle1_2");
	iDelay.push_back(0);
	vMinion.push_back(new Block(56, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 57 -----
	tSprite.push_back("beetle2_0");
	iDelay.push_back(155);
	tSprite.push_back("beetle2_1");
	iDelay.push_back(155);
	vMinion.push_back(new Block(57, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 58 -----
	tSprite.push_back("beetle2_2");
	iDelay.push_back(0);
	vMinion.push_back(new Block(58, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 59 -----
	tSprite.push_back("bulletbill");
	iDelay.push_back(0);
	vMinion.push_back(new Block(59, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 60 -----
	tSprite.push_back("bulletbill1");
	iDelay.push_back(0);
	vMinion.push_back(new Block(60, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 61 -----
	tSprite.push_back("hammer1_0");
	iDelay.push_back(0);
	vMinion.push_back(new Block(61, new Sprite(rR, tSprite, iDelay, false), true, false, true, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 62 -----
	tSprite.push_back("fireball_0");
	iDelay.push_back(155);
	tSprite.push_back("fireball_1");
	iDelay.push_back(155);
	tSprite.push_back("fireball_2");
	iDelay.push_back(155);
	tSprite.push_back("fireball_3");
	iDelay.push_back(155);
	vMinion.push_back(new Block(62, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 63 -----
	tSprite.push_back("firework0");
	iDelay.push_back(0);
	vMinion.push_back(new Block(63, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 64 -----
	tSprite.push_back("firework1");
	iDelay.push_back(0);
	vMinion.push_back(new Block(64, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();
	// ----- 65 -----
	tSprite.push_back("firework1");
	iDelay.push_back(0);
	vMinion.push_back(new Block(65, new Sprite(rR, tSprite, iDelay, false), true, false, false, true));
	tSprite.clear();
	iDelay.clear();

	iBlockSize = vBlock.size();
	iMinionSize = vMinion.size();
}

/* ******************************************** */

void Map::clearMap() {
	for(int i = 0; i < iMapWidth; i++) {
		for(int j = 0; j < iMapHeight; j++) {
			delete lMap[i][j];
		}
		lMap[i].clear();
	}
	lMap.clear();

	this->iMapWidth = this->iMapHeight = 0;

	if(oFlag != NULL) {
		delete oFlag;
		oFlag = NULL;
	}

	oEvent->eventTypeID = oEvent->eNormal;

	clearLevelText();
}

void Map::clearMinions() {
	for(int i = 0; i < iMinionListSize; i++) {
		for(int j = 0, jSize = lMinion[i].size(); j < jSize; j++) {
			delete lMinion[i][j];
			jSize = lMinion[i].size();
		}
		lMinion[i].clear();
	}

	clearPlatforms();
}

void Map::clearPlatforms() {
	for(unsigned int i = 0; i < vPlatform.size(); i++) {
		delete vPlatform[i];
	}

	vPlatform.clear();
}

void Map::clearBubbles() {
	for(unsigned int i = 0; i < lBubble.size(); i++) {
		delete lBubble[i];
	}

	lBubble.clear();
}

/* ******************************************** */

void Map::setBackgroundColor(SDL_Renderer* rR) {
	switch(iLevelType) {
		case 0: case 2:
			SDL_SetRenderDrawColor(rR, 93, 148, 252, 255);
			break;
		case 1: case 3: case 4:
			SDL_SetRenderDrawColor(rR, 0, 0, 0, 255);
			break;
		default:
			SDL_SetRenderDrawColor(rR, 93, 148, 252, 255);
			break;
	}
}

std::string Map::getLevelName() {
	return "" + std::to_string(1 + currentLevelID/4) + "-" + std::to_string(currentLevelID%4 + 1);
}

void Map::loadMinionsLVL_1_1() {
	clearMinions();

	addGoombas(704, 368, true);

	addGoombas(1280, 368, true);

	addGoombas(1632, 368, true);
	addGoombas(1680, 368, true);

	addGoombas(2560, 112, true);
	addGoombas(2624, 112, true);

	addGoombas(3104, 368, true);
	addGoombas(3152, 368, true);

	addKoppa(107*32, 368, 1, true);

	addGoombas(3648, 368, true);
	addGoombas(3696, 368, true);

	addGoombas(3968, 368, true);
	addGoombas(4016, 368, true);

	addGoombas(4096, 368, true);
	addGoombas(4144, 368, true);

	addGoombas(5568, 368, true);
	addGoombas(5612, 368, true);
}

void Map::loadMinionsLVL_1_2() {
	clearMinions();

	this->iLevelType = 1;

	addGoombas(16*32, 368, true);
	addGoombas(17*32 + 8, 368 - 32, true);

	addGoombas(29*32, 368, true);

	addKoppa(44*32, 368, 1, true);
	addKoppa(45*32 + 16, 368, 1, true);

	addKoppa(59*32, 368, 1, true);

	addGoombas(62*32, 368, true);
	addGoombas(64*32, 368, true);

	addGoombas(73*32, 368 - 8*32, true);

	addGoombas(76*32, 368 - 4*32, true);
	addGoombas(77*32 + 16, 368 - 4*32, true);

	addGoombas(99*32, 368, true);
	addGoombas(100*32 + 16, 368, true);
	addGoombas(102*32, 368, true);

	addGoombas(113*32, 368, true);

	addGoombas(135*32, 368 - 3*32, true);
	addGoombas(136*32 + 16, 368 - 4*32, true);

	this->iLevelType = 3;

	addKoppa(146*32, 368, 1, false);

	this->iLevelType = 1;
	addPlant(103*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);
	addPlant(109*32 + 16, CCFG::GAME_HEIGHT - 10 - 5*32);
	addPlant(115*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);

	this->iLevelType = 0;
	addPlant(284*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);

	this->iLevelType = 1;
}

void Map::loadMinionsLVL_1_3() {
	clearMinions();

	this->iLevelType = 3;

	addKoppa(30*32 - 8, CCFG::GAME_HEIGHT - 16 - 10*32, 1, true);
	addKoppa(110*32 - 8, CCFG::GAME_HEIGHT - 16 - 8*32, 1, true);

	addKoppa(74*32 - 8, CCFG::GAME_HEIGHT - 16 - 10*32, 3, false);
	addKoppa(114*32 - 8, CCFG::GAME_HEIGHT - 16 - 9*32, 3, false);

	addKoppa(133*32 - 8, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	this->iLevelType = 0;

	addGoombas(44*32, CCFG::GAME_HEIGHT - 16 - 11*32, true);
	addGoombas(46*32, CCFG::GAME_HEIGHT - 16 - 11*32, true);
	addGoombas(80*32, CCFG::GAME_HEIGHT - 16 - 9*32, true);
}

void Map::loadMinionsLVL_1_4() {
	clearMinions();

	addBowser(135*32, CCFG::GAME_HEIGHT - 16 - 6*32);
	
	addToad(153*32, CCFG::GAME_HEIGHT - 3*32, false);
	
	addFireBall(30*32, CCFG::GAME_HEIGHT - 16 - 4*32, 6, rand()%360, true);
	addFireBall(49*32, CCFG::GAME_HEIGHT - 16 - 8*32, 6, rand()%360, true);
	addFireBall(60*32, CCFG::GAME_HEIGHT - 16 - 8*32, 6, rand()%360, true);
	addFireBall(67*32, CCFG::GAME_HEIGHT - 16 - 8*32, 6, rand()%360, true);
	addFireBall(76*32, CCFG::GAME_HEIGHT - 16 - 5*32, 6, rand()%360, true);
	addFireBall(84*32, CCFG::GAME_HEIGHT - 16 - 5*32, 6, rand()%360, true);
	addFireBall(88*32, CCFG::GAME_HEIGHT - 16 - 10*32, 6, rand()%360, false);
}

void Map::loadMinionsLVL_2_1() {
	clearMinions();

	addSpring(188*32, 336);

	addGoombas(24*32, CCFG::GAME_HEIGHT - 16 - 7*32, true);

	addGoombas(42*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(43*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);

	addGoombas(59*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(60*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);

	addGoombas(68*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(69*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(71*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);

	addGoombas(87*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(88*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(90*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);

	addGoombas(102*32 + 16, CCFG::GAME_HEIGHT - 16 - 6*32, true);
	addGoombas(114*32 + 16, CCFG::GAME_HEIGHT - 16 - 4*32, true);

	addGoombas(120*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);

	addGoombas(162*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(163*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	
	addKoppa(32*32 - 2, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(33*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);

	addKoppa(55*32, CCFG::GAME_HEIGHT - 16 - 6*32, 1, true);
	addKoppa(66*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);

	addKoppa(137*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(151*32, CCFG::GAME_HEIGHT - 16 - 4*32, 0, true);
	addKoppa(169*32, CCFG::GAME_HEIGHT - 16 - 2*32, 0, true);
	addKoppa(171*32, CCFG::GAME_HEIGHT - 16 - 2*32, 0, true);

	addKoppa(185*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	
	addPlant(46*32 + 16, CCFG::GAME_HEIGHT - 10 - 5*32);
	addPlant(74*32 + 16, CCFG::GAME_HEIGHT - 10 - 5*32);
	addPlant(103*32 + 16, CCFG::GAME_HEIGHT - 10 - 5*32);
	addPlant(115*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);
	addPlant(122*32 + 16, CCFG::GAME_HEIGHT - 10 - 5*32);
	addPlant(130*32 + 16, CCFG::GAME_HEIGHT - 10 - 6*32);
	addPlant(176*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);
}

void Map::loadMinionsLVL_2_2() {
	clearMinions();

	addSquid(22*32, CCFG::GAME_HEIGHT - 16 - 3*32);
	addSquid(46*32, CCFG::GAME_HEIGHT - 16 - 5*32);
	addSquid(55*32, CCFG::GAME_HEIGHT - 16 - 4*32);
	addSquid(83*32, CCFG::GAME_HEIGHT - 16 - 6*32);
	addSquid(94*32, CCFG::GAME_HEIGHT - 16 - 11*32);
	addSquid(105*32, CCFG::GAME_HEIGHT - 16 - 3*32);

	addCheep(75*32 + 28, CCFG::GAME_HEIGHT - 16 - 4*32, 0, 1);
	addCheep(78*32 + 28, CCFG::GAME_HEIGHT - 16 - 7*32, 0, 1);
	addCheep(81*32 + 28, CCFG::GAME_HEIGHT - 16 - 2*32 - 28, 0, 1);
	addCheep(94*32 + 14, CCFG::GAME_HEIGHT - 16 - 8*32, 0, 1);
	addCheep(101*32 + 28, CCFG::GAME_HEIGHT - 16 - 4*32, 0, 1);
	addCheep(97*32 + 8, CCFG::GAME_HEIGHT - 16 - 11*32, 1, 1);
	addCheep(117*32 + 8, CCFG::GAME_HEIGHT - 16 - 10*32, 0, 1);
	addCheep(127*32 + 24, CCFG::GAME_HEIGHT - 16 - 4*32, 1, 1);
	addCheep(131*32 + 8, CCFG::GAME_HEIGHT - 16 - 3*32 - 4, 0, 1);
	addCheep(136*32 + 16, CCFG::GAME_HEIGHT - 16 - 6*32, 0, 1);
	addCheep(145*32 + 8, CCFG::GAME_HEIGHT - 16 - 4*32, 0, 1);
	addCheep(149*32 + 28, CCFG::GAME_HEIGHT - 16 - 8*32 - 4, 1, 1);
	addCheep(164*32, CCFG::GAME_HEIGHT - 16 - 11*32, 0, 1);
	addCheep(167*32, CCFG::GAME_HEIGHT - 16 - 3*32, 1, 1);
	addCheep(175*32, CCFG::GAME_HEIGHT - 16 - 6*32 - 4, 0, 1);
	addCheep(183*32, CCFG::GAME_HEIGHT - 16 - 10*32, 1, 1);
	addCheep(186*32 + 16, CCFG::GAME_HEIGHT - 16 - 7*32, 1, 1);

	this->iLevelType = 0;
	addPlant(274*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);

	this->iLevelType = 2;
}

void Map::loadMinionsLVL_2_3() {
	clearMinions();

	addCheepSpawner(5*32, 200*32);
}

void Map::loadMinionsLVL_2_4() {
	clearMinions();

	addBowser(135*32, CCFG::GAME_HEIGHT - 16 - 6*32);
	
	addToad(153*32, CCFG::GAME_HEIGHT - 3*32, false);

	addFireBall(49*32, CCFG::GAME_HEIGHT - 16 - 5*32, 6, rand()%360, true);
	addFireBall(55*32, CCFG::GAME_HEIGHT - 16 - 9*32, 6, rand()%360, true);
	addFireBall(61*32, CCFG::GAME_HEIGHT - 16 - 5*32, 6, rand()%360, true);
	addFireBall(73*32, CCFG::GAME_HEIGHT - 16 - 5*32, 6, rand()%360, true);
	addFireBall(82*32, CCFG::GAME_HEIGHT - 16 - 8*32, 6, rand()%360, true);
	addFireBall(92*32, CCFG::GAME_HEIGHT - 16 - 4*32, 6, rand()%360, false);

	addUpFire(16*32 + 4, 9*32);
	addUpFire(30*32, 9*32);
}

void Map::loadMinionsLVL_3_1() {
	clearMinions();

	addSpring(126*32, 336);

	addGoombas(37*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(53*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(54*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(56*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(82*32, CCFG::GAME_HEIGHT - 16 - 6*32, true);
	addGoombas(83*32 + 16, CCFG::GAME_HEIGHT - 16 - 6*32, true);
	addGoombas(85*32, CCFG::GAME_HEIGHT - 16 - 6*32, true);
	addGoombas(94*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(95*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(139*32 - 4, CCFG::GAME_HEIGHT - 16 - 6*32, true);
	addGoombas(140*32, CCFG::GAME_HEIGHT - 16 - 7*32, true);
	addGoombas(154*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(155*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(157*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);

	addPlant(32*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);
	addPlant(38*32 + 16, CCFG::GAME_HEIGHT - 10 - 5*32);
	addPlant(57*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);
	addPlant(67*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);
	addPlant(103*32 + 16, CCFG::GAME_HEIGHT - 10 - 5*32);

	addKoppa(25*32, CCFG::GAME_HEIGHT - 16 - 2*32, 0, true);
	addKoppa(28*32, CCFG::GAME_HEIGHT - 16 - 3*32, 0, true);
	addKoppa(65*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(101*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(149*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(152*32, CCFG::GAME_HEIGHT - 16 - 10*32, 1, true);
	addKoppa(165*32, CCFG::GAME_HEIGHT - 16 - 2*32, 0, true);
	addKoppa(168*32, CCFG::GAME_HEIGHT - 16 - 3*32, 0, true);
	addKoppa(170*32, CCFG::GAME_HEIGHT - 16 - 6*32, 1, true);
	addKoppa(171*32, CCFG::GAME_HEIGHT - 16 - 2*32, 0, true);
	addKoppa(188*32, CCFG::GAME_HEIGHT - 16 - 8*32, 1, true);
	addKoppa(191*32, CCFG::GAME_HEIGHT - 16 - 10*32, 1, true);

	addHammerBro(113*32, CCFG::GAME_HEIGHT - 16 - 7*32);
	addHammerBro(116*32, CCFG::GAME_HEIGHT - 16 - 3*32);
}

void Map::loadMinionsLVL_3_2() {
	clearMinions();

	addKoppa(17*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(33*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(34*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(36*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(43*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(44*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(66*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(78*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(98*32, CCFG::GAME_HEIGHT - 16 - 2*32, 0, true);
	addKoppa(111*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(134*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(140*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(141*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(143*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(150*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(151*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(162*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(163*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(165*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(175*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	
	addGoombas(24*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(25*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(27*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(71*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(72*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(74*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(119*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(120*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(122*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(179*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(180*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(182*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(188*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(189*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(191*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);

	addPlant(169*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);
}

void Map::loadMinionsLVL_3_3() {
	clearMinions();

	addGoombas(26*32, CCFG::GAME_HEIGHT - 16 - 8*32, true);
	
	iLevelType = 3;

	addKoppa(52*32, CCFG::GAME_HEIGHT - 16 - 8*32, 1, true);
	addKoppa(54*32, CCFG::GAME_HEIGHT - 16 - 4*32, 1, true);
	addKoppa(73*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);

	addKoppa(114*32 - 8, CCFG::GAME_HEIGHT - 16 - 9*32, 3, false);

	addKoppa(124*32, CCFG::GAME_HEIGHT - 16 - 5*32, 1, true);
	addKoppa(126*32, CCFG::GAME_HEIGHT - 16 - 5*32, 1, true);

	iLevelType = 4;
}

void Map::loadMinionsLVL_3_4() {
	clearMinions();
	
	addBowser(135*32, CCFG::GAME_HEIGHT - 16 - 6*32);
	
	addToad(153*32, CCFG::GAME_HEIGHT - 3*32, false);

	addFireBall(19*32, CCFG::GAME_HEIGHT - 16 - 3*32, 6, rand()%360, true);
	addFireBall(24*32, CCFG::GAME_HEIGHT - 16 - 3*32, 6, rand()%360, true);
	addFireBall(29*32, CCFG::GAME_HEIGHT - 16 - 3*32, 6, rand()%360, true);
	addFireBall(54*32, CCFG::GAME_HEIGHT - 16 - 3*32, 6, rand()%360, true);
	addFireBall(54*32, CCFG::GAME_HEIGHT - 16 - 9*32, 6, rand()%360, false);
	addFireBall(64*32, CCFG::GAME_HEIGHT - 16 - 3*32, 6, rand()%360, true);
	addFireBall(64*32, CCFG::GAME_HEIGHT - 16 - 9*32, 6, rand()%360, false);
	addFireBall(80*32, CCFG::GAME_HEIGHT - 16 - 3*32, 6, rand()%360, true);
	addFireBall(80*32, CCFG::GAME_HEIGHT - 16 - 9*32, 6, rand()%360, false);

	addUpFire(16*32, 9*32);
	addUpFire(26*32, 9*32);
	addUpFire(88*32, 9*32);
	addUpFire(97*32, 9*32);
	addUpFire(103*32, 9*32);
	addUpFire(109*32, 9*32);
}

void Map::loadMinionsLVL_4_1() {
	clearMinions();

	addPlant(21*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);
	addPlant(116*32 + 16, CCFG::GAME_HEIGHT - 10 - 5*32);
	addPlant(132*32 + 16, CCFG::GAME_HEIGHT - 10 - 5*32);
	addPlant(163*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);

	addLakito(26*32, CCFG::GAME_HEIGHT - 16 - 11*32, 207*32);
}

void Map::loadMinionsLVL_4_2() {
	clearMinions();

	addGoombas(43*32, CCFG::GAME_HEIGHT - 16 - 6*32, true);
	addGoombas(44*32 + 16, CCFG::GAME_HEIGHT - 16 - 6*32, true);
	addGoombas(46*32, CCFG::GAME_HEIGHT - 16 - 6*32, true);

	addKoppa(77*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(100*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(101*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(137*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(168*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(169*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);

	addPlant(72*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);
	addPlant(78*32 + 16, CCFG::GAME_HEIGHT - 10 - 8*32);
	addPlant(84*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);
	addPlant(107*32 + 16, CCFG::GAME_HEIGHT - 10 - 5*32);
	addPlant(138*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);
	addPlant(142*32 + 16, CCFG::GAME_HEIGHT - 10 - 5*32);
	addPlant(180*32 + 16, CCFG::GAME_HEIGHT - 10 - 8*32);

	addBeetle(83*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addBeetle(88*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addBeetle(154*32, CCFG::GAME_HEIGHT - 16 - 5*32, true);
	addBeetle(179*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);

	this->iLevelType = 0;

	addPlant(394*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);

	this->iLevelType = 1;
}

void Map::loadMinionsLVL_4_3() {
	clearMinions();

	this->iLevelType = 3;

	addKoppa(28*32 - 2, CCFG::GAME_HEIGHT - 16 - 6*32, 1, true);
	addKoppa(29*32, CCFG::GAME_HEIGHT - 16 - 6*32, 1, true);
	addKoppa(39*32, CCFG::GAME_HEIGHT - 16 - 3*32, 1, true);
	addKoppa(68*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(78*32, CCFG::GAME_HEIGHT - 16 - 10*32, 1, true);

	addKoppa(35*32, CCFG::GAME_HEIGHT - 16 - 11*32, 3, false);
	
	this->iLevelType = 0;
}


void Map::loadMinionsLVL_4_4() {
	clearMinions();

	addBowser(167*32, CCFG::GAME_HEIGHT - 16 - 6*32);
	
	addToad(186*32, CCFG::GAME_HEIGHT - 3*32, false);

	addFireBall(53*32, CCFG::GAME_HEIGHT - 16 - 8*32, 6, rand()%360, true);
	addFireBall(60*32, CCFG::GAME_HEIGHT - 16 - 5*32, 6, rand()%360, false);
	addFireBall(115*32, CCFG::GAME_HEIGHT - 16 - 8*32, 6, rand()%360, true);
	addFireBall(122*32, CCFG::GAME_HEIGHT - 16 - 4*32, 6, rand()%360, true);
	addFireBall(162*32, CCFG::GAME_HEIGHT - 16 - 4*32, 6, rand()%360, true);

	addUpFire(165*32, 9*32);

	this->iLevelType = 1;

	addPlant(40*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);

	this->iLevelType = 3;
}

void Map::loadMinionsLVL_5_1() {
	clearMinions();

	addGoombas(19*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(20*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(22*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(30*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(31*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(33*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(65*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(66*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(68*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(76*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(77*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(103*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(104*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(106*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(121*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(122*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(124*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(135*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(136*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(138*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	
	addKoppa(16*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(41*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(42*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(61*32, CCFG::GAME_HEIGHT - 16 - 3*32, 0, true);
	addKoppa(87*32, CCFG::GAME_HEIGHT - 16 - 3*32, 0, true);
	addKoppa(127*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(144*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(145*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(178*32, CCFG::GAME_HEIGHT - 16 - 2*32, 0, true);
	addKoppa(182*32, CCFG::GAME_HEIGHT - 16 - 6*32, 1, true);

	addPlant(44*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);
	addPlant(51*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);
	addPlant(156*32 + 16, CCFG::GAME_HEIGHT - 10 - 7*32);
	addPlant(163*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);
	
}

void Map::loadMinionsLVL_5_2() {
	clearMinions();

	addGoombas(143*32, CCFG::GAME_HEIGHT - 16 - 4*32, true);
	addGoombas(145*32, CCFG::GAME_HEIGHT - 16 - 6*32, true);
	addGoombas(235*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(236*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);

	addKoppa(103*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(120*32, CCFG::GAME_HEIGHT - 16 - 3*32, 0, true);
	addKoppa(186*32, CCFG::GAME_HEIGHT - 16 - 4*32, 0, true);
	addKoppa(243*32, CCFG::GAME_HEIGHT - 16 - 3*32, 0, true);
	addKoppa(246*32, CCFG::GAME_HEIGHT - 16 - 2*32, 0, true);
	addKoppa(266*32, CCFG::GAME_HEIGHT - 16 - 7*32, 0, true);

	addHammerBro(126*32, CCFG::GAME_HEIGHT - 16 - 6*32);
	addHammerBro(161*32, CCFG::GAME_HEIGHT - 16 - 7*32);
	addHammerBro(200*32, CCFG::GAME_HEIGHT - 16 - 7*32);
	addHammerBro(204*32, CCFG::GAME_HEIGHT - 16 - 11*32);

	addPlant(135*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);
	addPlant(195*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);

	addBeetle(216*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addBeetle(217*32 + 4, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addBeetle(218*32 + 8, CCFG::GAME_HEIGHT - 16 - 2*32, true);

	this->iLevelType = 3;

	addKoppa(237*32, CCFG::GAME_HEIGHT - 16 - 10*32, 1, true);

	this->iLevelType = 0;

	// -- MAP 5-2-2

	addSquid(17*32, CCFG::GAME_HEIGHT - 16 - 4*32);
	addSquid(34*32, CCFG::GAME_HEIGHT - 16 - 4*32);
	addSquid(43*32 + 16, CCFG::GAME_HEIGHT - 16 - 4*32);

	addCheep(27*32 + 16, CCFG::GAME_HEIGHT - 9*32, 0, 1);
	addCheep(38*32 + 28, CCFG::GAME_HEIGHT - 4*32, 0, 1);
	addCheep(48*32 + 16, CCFG::GAME_HEIGHT - 6*32, 1, 1);
	addCheep(53*32 + 16, CCFG::GAME_HEIGHT - 11*32, 0, 1);
}

void Map::loadMinionsLVL_5_3() {
	clearMinions();

	this->iLevelType = 3;

	addKoppa(30*32 - 8, CCFG::GAME_HEIGHT - 16 - 10*32, 1, true);
	addKoppa(110*32 - 8, CCFG::GAME_HEIGHT - 16 - 8*32, 1, true);

	addKoppa(74*32 - 8, CCFG::GAME_HEIGHT - 16 - 10*32, 3, false);
	addKoppa(114*32 - 8, CCFG::GAME_HEIGHT - 16 - 9*32, 3, false);

	addKoppa(133*32 - 8, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	this->iLevelType = 0;

	addGoombas(44*32, CCFG::GAME_HEIGHT - 16 - 11*32, true);
	addGoombas(46*32, CCFG::GAME_HEIGHT - 16 - 11*32, true);
	addGoombas(80*32, CCFG::GAME_HEIGHT - 16 - 9*32, true);

	addBulletBillSpawner(174, 3, 1);
}

void Map::loadMinionsLVL_5_4() {
	clearMinions();

	addBowser(135*32, CCFG::GAME_HEIGHT - 16 - 6*32);
	
	addToad(153*32, CCFG::GAME_HEIGHT - 3*32, false);

	addFireBall(49*32, CCFG::GAME_HEIGHT - 16 - 5*32, 6, rand()%360, true);
	addFireBall(55*32, CCFG::GAME_HEIGHT - 16 - 9*32, 6, rand()%360, true);
	addFireBall(61*32, CCFG::GAME_HEIGHT - 16 - 5*32, 6, rand()%360, true);
	addFireBall(73*32, CCFG::GAME_HEIGHT - 16 - 5*32, 6, rand()%360, true);
	addFireBall(82*32, CCFG::GAME_HEIGHT - 16 - 8*32, 6, rand()%360, true);
	addFireBall(92*32, CCFG::GAME_HEIGHT - 16 - 4*32, 6, rand()%360, false);

	addFireBall(23*32, CCFG::GAME_HEIGHT - 16 - 7*32, 12, rand()%360, true);

	addFireBall(43*32, CCFG::GAME_HEIGHT - 16 - 1*32, 6, rand()%360, false);
	addFireBall(55*32, CCFG::GAME_HEIGHT - 16 - 1*32, 6, rand()%360, false);
	addFireBall(67*32, CCFG::GAME_HEIGHT - 16 - 1*32, 6, rand()%360, true);

	addFireBall(103*32, CCFG::GAME_HEIGHT - 16 - 3*32, 6, rand()%360, true);

	addUpFire(16*32 + 4, 9*32);
	addUpFire(30*32, 9*32);

	addUpFire(109*32, 9*32);
	addUpFire(113*32, 9*32);
	addUpFire(131*32, 9*32);
}

void Map::loadMinionsLVL_6_1() {
	clearMinions();

	this->iLevelType = 0;

	addLakito(23*32, CCFG::GAME_HEIGHT - 16 - 11*32, 176*32);

	addPlant(102*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);

	this->iLevelType = 4;
}

void Map::loadMinionsLVL_6_2() {
	clearMinions();

	addSquid(17*32, CCFG::GAME_HEIGHT - 16 - 4*32);
	addSquid(34*32, CCFG::GAME_HEIGHT - 16 - 4*32);
	addSquid(43*32 + 16, CCFG::GAME_HEIGHT - 16 - 4*32);

	addCheep(27*32 + 16, CCFG::GAME_HEIGHT - 9*32, 0, 1);
	addCheep(38*32 + 28, CCFG::GAME_HEIGHT - 4*32, 0, 1);
	addCheep(48*32 + 16, CCFG::GAME_HEIGHT - 6*32, 1, 1);
	addCheep(53*32 + 16, CCFG::GAME_HEIGHT - 11*32, 0, 1);

	addKoppa(111*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(128*32, CCFG::GAME_HEIGHT - 16 - 3*32, 0, true);
	addKoppa(291*32, CCFG::GAME_HEIGHT - 16 - 11*32, 0, true);

	addPlant(104*32 + 16, CCFG::GAME_HEIGHT - 10 - 5*32);
	addPlant(113*32 + 16, CCFG::GAME_HEIGHT - 10 - 5*32);
	addPlant(117*32 + 16, CCFG::GAME_HEIGHT - 10 - 8*32);
	addPlant(120*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);
	addPlant(122*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);
	addPlant(131*32 + 16, CCFG::GAME_HEIGHT - 10 - 5*32);
	addPlant(141*32 + 16, CCFG::GAME_HEIGHT - 10 - 6*32);
	addPlant(147*32 + 16, CCFG::GAME_HEIGHT - 10 - 7*32);
	addPlant(152*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);
	addPlant(165*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);
	addPlant(169*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);
	addPlant(172*32 + 16, CCFG::GAME_HEIGHT - 10 - 7*32);
	addPlant(179*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);
	addPlant(190*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);
	addPlant(196*32 + 16, CCFG::GAME_HEIGHT - 10 - 8*32);
	addPlant(200*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);
	addPlant(216*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);
	addPlant(220*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);
	addPlant(238*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);
	addPlant(252*32 + 16, CCFG::GAME_HEIGHT - 10 - 8*32);
	addPlant(259*32 + 16, CCFG::GAME_HEIGHT - 10 - 5*32);
	addPlant(264*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);
	addPlant(266*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);
	addPlant(268*32 + 16, CCFG::GAME_HEIGHT - 10 - 5*32);
	addPlant(274*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);
	addPlant(286*32 + 16, CCFG::GAME_HEIGHT - 10 - 5*32);
	

	addBeetle(139*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addBeetle(177*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addBeetle(205*32, CCFG::GAME_HEIGHT - 16 - 10*32, true);
	addBeetle(248*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);

	addGoombas(152*32, CCFG::GAME_HEIGHT - 16 - 10*32, true);
	addGoombas(254*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);

}

void Map::loadMinionsLVL_6_3() {
	clearMinions();

	addBulletBillSpawner(174, 3, 1);
}

void Map::loadMinionsLVL_6_4() {
	clearMinions();

	addBowser(135*32, CCFG::GAME_HEIGHT - 16 - 6*32, true);
	
	addToad(153*32, CCFG::GAME_HEIGHT - 3*32, false);
	
	addFireBall(30*32, CCFG::GAME_HEIGHT - 16 - 4*32, 6, rand()%360, true);
	addFireBall(49*32, CCFG::GAME_HEIGHT - 16 - 8*32, 6, rand()%360, true);
	addFireBall(60*32, CCFG::GAME_HEIGHT - 16 - 8*32, 6, rand()%360, true);
	addFireBall(67*32, CCFG::GAME_HEIGHT - 16 - 8*32, 6, rand()%360, true);
	addFireBall(76*32, CCFG::GAME_HEIGHT - 16 - 5*32, 6, rand()%360, true);
	addFireBall(84*32, CCFG::GAME_HEIGHT - 16 - 5*32, 6, rand()%360, true);
	addFireBall(88*32, CCFG::GAME_HEIGHT - 16 - 10*32, 6, rand()%360, false);

	addFireBall(23*32, CCFG::GAME_HEIGHT - 16 - 8*32, 6, rand()%360, true);
	addFireBall(37*32, CCFG::GAME_HEIGHT - 16 - 8*32, 6, rand()%360, true);
	addFireBall(80*32, CCFG::GAME_HEIGHT - 16 - 10*32, 6, rand()%360, false);
	addFireBall(92*32, CCFG::GAME_HEIGHT - 16 - 5*32, 6, rand()%360, true);
	
	addUpFire(27*32, 9*32);
	addUpFire(33*32, 9*32);
	addUpFire(131*32, 9*32);
}


void Map::loadMinionsLVL_7_1() {
	clearMinions();


	addKoppa(26*32, CCFG::GAME_HEIGHT - 16 - 3*32, 0, true);
	addKoppa(44*32, CCFG::GAME_HEIGHT - 16 - 4*32, 0, true);
	addKoppa(53*32, CCFG::GAME_HEIGHT - 16 - 2*32, 0, true);
	addKoppa(65*32, CCFG::GAME_HEIGHT - 16 - 3*32, 0, true);

	addKoppa(114*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);

	addPlant(76*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);
	addPlant(93*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);
	addPlant(109*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);
	addPlant(115*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);
	addPlant(128*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);

	addHammerBro(85*32, CCFG::GAME_HEIGHT - 16 - 7*32);
	addHammerBro(87*32, CCFG::GAME_HEIGHT - 16 - 11*32);
	addHammerBro(137*32, CCFG::GAME_HEIGHT - 16 - 7*32);
	addHammerBro(135*32, CCFG::GAME_HEIGHT - 16 - 3*32);

	addBeetle(169*32, CCFG::GAME_HEIGHT - 16 - 10*32, true);
}

void Map::loadMinionsLVL_7_2() {
	clearMinions();

	addSquid(22*32, CCFG::GAME_HEIGHT - 16 - 3*32);
	addSquid(24*32, CCFG::GAME_HEIGHT - 16 - 7*32);
	addSquid(46*32, CCFG::GAME_HEIGHT - 16 - 5*32);
	addSquid(52*32, CCFG::GAME_HEIGHT - 16 - 7*32);
	addSquid(55*32, CCFG::GAME_HEIGHT - 16 - 4*32);
	addSquid(77*32, CCFG::GAME_HEIGHT - 16 - 8*32);
	addSquid(83*32, CCFG::GAME_HEIGHT - 16 - 6*32);
	addSquid(90*32, CCFG::GAME_HEIGHT - 16 - 3*32);
	addSquid(94*32, CCFG::GAME_HEIGHT - 16 - 11*32);
	addSquid(105*32, CCFG::GAME_HEIGHT - 16 - 3*32);
	addSquid(150*32, CCFG::GAME_HEIGHT - 16 - 6*32);
	addSquid(173*32, CCFG::GAME_HEIGHT - 16 - 3*32);
	addSquid(179*32, CCFG::GAME_HEIGHT - 16 - 3*32);

	addCheep(75*32 + 28, CCFG::GAME_HEIGHT - 16 - 4*32, 0, 1);
	addCheep(78*32 + 28, CCFG::GAME_HEIGHT - 16 - 7*32, 0, 1);
	addCheep(81*32 + 28, CCFG::GAME_HEIGHT - 16 - 2*32 - 28, 0, 1);
	addCheep(94*32 + 14, CCFG::GAME_HEIGHT - 16 - 8*32, 0, 1);
	addCheep(101*32 + 28, CCFG::GAME_HEIGHT - 16 - 4*32, 0, 1);
	addCheep(97*32 + 8, CCFG::GAME_HEIGHT - 16 - 11*32, 1, 1);
	addCheep(117*32 + 8, CCFG::GAME_HEIGHT - 16 - 10*32, 0, 1);
	addCheep(127*32 + 24, CCFG::GAME_HEIGHT - 16 - 4*32, 1, 1);
	addCheep(131*32 + 8, CCFG::GAME_HEIGHT - 16 - 3*32 - 4, 0, 1);
	addCheep(136*32 + 16, CCFG::GAME_HEIGHT - 16 - 6*32, 0, 1);
	addCheep(145*32 + 8, CCFG::GAME_HEIGHT - 16 - 4*32, 0, 1);
	addCheep(149*32 + 28, CCFG::GAME_HEIGHT - 16 - 8*32 - 4, 1, 1);
	addCheep(164*32, CCFG::GAME_HEIGHT - 16 - 11*32, 0, 1);
	addCheep(167*32, CCFG::GAME_HEIGHT - 16 - 3*32, 1, 1);
	addCheep(175*32, CCFG::GAME_HEIGHT - 16 - 6*32 - 4, 0, 1);
	addCheep(183*32, CCFG::GAME_HEIGHT - 16 - 10*32, 1, 1);
	addCheep(186*32 + 16, CCFG::GAME_HEIGHT - 16 - 7*32, 1, 1);

	this->iLevelType = 0;
	addPlant(274*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);

	this->iLevelType = 2;
}

void Map::loadMinionsLVL_7_3() {
	clearMinions();

	addCheepSpawner(5*32, 200*32);

	addKoppa(52*32, CCFG::GAME_HEIGHT - 16 - 6*32, 0, true);
	addKoppa(140*32, CCFG::GAME_HEIGHT - 16 - 7*32, 0, true);
	addKoppa(156*32, CCFG::GAME_HEIGHT - 16 - 5*32, 0, true);

	this->iLevelType = 3;

	addKoppa(79*32, CCFG::GAME_HEIGHT - 16 - 5*32, 1, true);
	addKoppa(95*32, CCFG::GAME_HEIGHT - 16 - 5*32, 1, true);
	addKoppa(119*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);

	this->iLevelType = 0;
}

void Map::loadMinionsLVL_7_4() {
	clearMinions();

	addUpFire(20*32, 9*32);
	addUpFire(260*32, 9*32);

	addFireBall(167*32, CCFG::GAME_HEIGHT - 16 - 7*32, 6, rand()%360, true);

	addBowser(263*32, CCFG::GAME_HEIGHT - 16 - 6*32, true);

	addToad(281*32, CCFG::GAME_HEIGHT - 3*32, false);
}

void Map::loadMinionsLVL_8_1() {
	clearMinions();

	addGoombas(23*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(24*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(26*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(30*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(31*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(33*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(69*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(70*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(72*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(108*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(109*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(111*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(148*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(149*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(151*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(232*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(233*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(235*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(257*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(258*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(260*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(264*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(265*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(267*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(272*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(273*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);

	addKoppa(43*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(44*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(61*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(119*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(124*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(125*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(127*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(130*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(131*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(133*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(161*32, CCFG::GAME_HEIGHT - 16 - 3*32, 0, true);
	addKoppa(172*32, CCFG::GAME_HEIGHT - 16 - 4*32, 0, true);
	addKoppa(177*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(207*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(208*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(305*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(332*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(339*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(340*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);
	addKoppa(342*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);


	addBeetle(18*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addBeetle(81*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addBeetle(254*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addBeetle(283*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);

	addPlant(35*32 + 16, CCFG::GAME_HEIGHT - 10 - 5*32);
	addPlant(76*32 + 16, CCFG::GAME_HEIGHT - 10 - 5*32);
	addPlant(82*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);
	addPlant(94*32 + 16, CCFG::GAME_HEIGHT - 10 - 5*32);
	addPlant(104*32 + 16, CCFG::GAME_HEIGHT - 10 - 5*32);
	addPlant(140*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);
	addPlant(238*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);
	addPlant(242*32 + 16, CCFG::GAME_HEIGHT - 10 - 5*32);
	addPlant(344*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);
	addPlant(355*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);
}

void Map::loadMinionsLVL_8_2() {
	clearMinions();

	addGoombas(184*32, CCFG::GAME_HEIGHT - 16 - 5*32, true);
	addGoombas(186*32, CCFG::GAME_HEIGHT - 16 - 7*32, true);

	addKoppa(18*32 - 8, CCFG::GAME_HEIGHT - 16 - 5*32, 0, true);
	addKoppa(24*32, CCFG::GAME_HEIGHT - 16 - 10*32, 0, true);
	addKoppa(57*32, CCFG::GAME_HEIGHT - 16 - 3*32, 0, true);
	addKoppa(66*32, CCFG::GAME_HEIGHT - 16 - 3*32, 0, true);
	addKoppa(69*32, CCFG::GAME_HEIGHT - 16 - 3*32, 0, true);
	addKoppa(92*32, CCFG::GAME_HEIGHT - 16 - 4*32, 0, true);
	addKoppa(95*32, CCFG::GAME_HEIGHT - 16 - 3*32, 0, true);
	addKoppa(139*32, CCFG::GAME_HEIGHT - 16 - 2*32, 0, true);
	addKoppa(170*32, CCFG::GAME_HEIGHT - 16 - 4*32, 0, true);
	addKoppa(172*32, CCFG::GAME_HEIGHT - 16 - 3*32, 0, true);
	addKoppa(175*32, CCFG::GAME_HEIGHT - 16 - 6*32, 0, true);
	addKoppa(203*32, CCFG::GAME_HEIGHT - 16 - 8*32, 0, true);

	addBeetle(111*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addBeetle(121*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addBeetle(123*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addBeetle(189*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);

	addLakito(16*32, CCFG::GAME_HEIGHT - 16 - 11*32, 216*32);

	addPlant(131*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);
	addPlant(142*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);
	addPlant(156*32 + 16, CCFG::GAME_HEIGHT - 10 - 5*32);
	addPlant(163*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);
	addPlant(131*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);
}

void Map::loadMinionsLVL_8_3() {
	clearMinions();

	addKoppa(30*32, CCFG::GAME_HEIGHT - 16 - 4*32, 0, true);
	addKoppa(93*32, CCFG::GAME_HEIGHT - 16 - 3*32, 0, true);
	addKoppa(137*32, CCFG::GAME_HEIGHT - 16 - 2*32, 1, true);

	addPlant(53*32 + 16, CCFG::GAME_HEIGHT - 10 - 5*32);
	addPlant(126*32 + 16, CCFG::GAME_HEIGHT - 10 - 5*32);
	addPlant(168*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);

	addHammerBro(63*32, CCFG::GAME_HEIGHT - 16 - 3*32);
	addHammerBro(65*32, CCFG::GAME_HEIGHT - 16 - 7*32);
	addHammerBro(117*32, CCFG::GAME_HEIGHT - 16 - 7*32);
	addHammerBro(119*32, CCFG::GAME_HEIGHT - 16 - 3*32);
	addHammerBro(146*32, CCFG::GAME_HEIGHT - 16 - 3*32);
	addHammerBro(159*32, CCFG::GAME_HEIGHT - 16 - 3*32);
	addHammerBro(177*32, CCFG::GAME_HEIGHT - 16 - 3*32);
	addHammerBro(185*32, CCFG::GAME_HEIGHT - 16 - 3*32);
}

void Map::loadMinionsLVL_8_4() {
	clearMinions();

	addPlant(19*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);
	addPlant(51*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);
	addPlant(81*32 + 16, CCFG::GAME_HEIGHT - 10 - 6*32);
	addPlant(126*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);
	addPlant(133*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);
	addPlant(143*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);
	addPlant(153*32 + 16, CCFG::GAME_HEIGHT - 10 - 4*32);
	addPlant(163*32 + 16, CCFG::GAME_HEIGHT - 10 - 8*32);
	addPlant(215*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);
	addPlant(302*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);
	addPlant(224*32 + 16, CCFG::GAME_HEIGHT - 10 - 6*32);
	addPlant(232*32 + 16, CCFG::GAME_HEIGHT - 10 - 7*32);
	addPlant(248*32 + 16, CCFG::GAME_HEIGHT - 10 - 6*32);
	addPlant(309*32 + 16, CCFG::GAME_HEIGHT - 10 - 3*32);

	addBeetle(139*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addBeetle(141*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);

	addGoombas(56*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(57*32 + 16, CCFG::GAME_HEIGHT - 16 - 2*32, true);
	addGoombas(59*32, CCFG::GAME_HEIGHT - 16 - 2*32, true);

	addHammerBro(316*32, CCFG::GAME_HEIGHT - 16 - 3*32);

	this->iLevelType = 1;

	addKoppa(150*32 - 8, CCFG::GAME_HEIGHT - 16 - 4*32, 0, true);
	addKoppa(152*32 - 8, CCFG::GAME_HEIGHT - 16 - 3*32, 0, true);
	addKoppa(165*32 - 8, CCFG::GAME_HEIGHT - 16 - 3*32, 0, true);
	addKoppa(167*32 - 8, CCFG::GAME_HEIGHT - 16 - 4*32, 0, true);

	this->iLevelType = 3;

	addCheepSpawner(224*32, 237*32);

	addBowser(338*32, CCFG::GAME_HEIGHT - 16 - 6*32, true);

	addToad(356*32, CCFG::GAME_HEIGHT - 3*32, true);

	addUpFire(332*32, 9*32);

	addFireBall(410*32, CCFG::GAME_HEIGHT - 16 - 7*32, 6, rand()%360, true);
	addFireBall(421*32, CCFG::GAME_HEIGHT - 16 - 4*32, 6, rand()%360, false);
	addFireBall(430*32, CCFG::GAME_HEIGHT - 16 - 8*32, 6, rand()%360, true);
	addFireBall(446*32, CCFG::GAME_HEIGHT - 16 - 6*32, 6, rand()%360, true);
	addFireBall(454*32, CCFG::GAME_HEIGHT - 16 - 7*32, 6, rand()%360, false);

	addSquid(418*32, CCFG::GAME_HEIGHT - 16 - 3*32);
	addSquid(441*32, CCFG::GAME_HEIGHT - 16 - 4*32);
	addSquid(443*32, CCFG::GAME_HEIGHT - 16 - 8*32);
}

void Map::createMap() {
	// ----- MIONION LIST -----

	for(int i = 0; i < iMapWidth; i += 5) {
		std::vector<Minion*> temp;
		lMinion.push_back(temp);
	}

	iMinionListSize = lMinion.size();

	// ----- MIONION LIST -----
	// ----- CREATE MAP -----

	for(int i = 0; i < iMapWidth; i++) {
		std::vector<MapLevel*> temp;
		for(int i = 0; i < iMapHeight; i++) {
			temp.push_back(new MapLevel(0));
		}

		lMap.push_back(temp);
	}

	// ----- CREATE MAP -----

	this->underWater = false;
	this->bTP = false;
}

void Map::checkSpawnPoint() {
	if(getNumOfSpawnPoints() > 1) {
		for(int i = iSpawnPointID + 1; i < getNumOfSpawnPoints(); i++) {
			if(getSpawnPointXPos(i) > oPlayer->getXPos() - fXPos && getSpawnPointXPos(i) < oPlayer->getXPos() - fXPos + 128) {
				iSpawnPointID = i;
			}
		}
	}
}

int Map::getNumOfSpawnPoints() {
	switch(currentLevelID) {
		case 0: case 1: case 2: case 4: case 5: case 8: case 9: case 10: case 13: case 14: case 16: case 17: case 18: case 20: case 21: case 22: case 24: case 25: case 26:
			return 2;
	}

	return 1;
}

int Map::getSpawnPointXPos(int iID) {
	switch(currentLevelID) {
		case 0:
			switch(iID) {
				case 0:
					return 84;
				case 1:
					return 82*32;
			}
		case 1:
			switch(iID) {
				case 0:
					return 84;
				case 1:
					return 89*32;
			}
		case 2:
			switch(iID) {
				case 0:
					return 84;
				case 1:
					return 66*32;
			}
		case 3:
			return 64;
		case 4:
			switch(iID) {
				case 0:
					return 84;
				case 1:
					return 98*32;
			}
		case 5:
			switch(iID) {
				case 0:
					return 84;
				case 1:
					return 86*32;
			}
		case 6:
			switch(iID) {
				case 0:
					return 84;
				case 1:
					return 114*32;
			}
		case 7:
			return 64;
		case 8:
			switch(iID) {
				case 0:
					return 84;
				case 1:
					return 90*32;
			}
		case 9:
			switch(iID) {
				case 0:
					return 84;
				case 1:
					return 98*32;
			}
		case 10:
			switch(iID) {
				case 0:
					return 84;
				case 1:
					return 66*32;
			}
		case 13:
			switch(iID) {
				case 0:
					return 84;
				case 1:
					return 95*32;
			}
		case 14:
			switch(iID) {
				case 0:
					return 84;
				case 1:
					return 65*32;
			}
		case 16:
			switch(iID) {
				case 0:
					return 84;
				case 1:
					return 97*32;
			}
		case 17:
			switch(iID) {
				case 0:
					return 84 + 80*32;
				case 1:
					return 177*32;
			}
		case 18:
			switch(iID) {
				case 0:
					return 84;
				case 1:
					return 66*32;
			}
		case 20:
			switch(iID) {
				case 0:
					return 84;
				case 1:
					return 98*32;
			}
		case 21:
			switch(iID) {
				case 0:
					return 84 + 85*32;
				case 1:
					return 183*32;
			}
		case 22:
			switch(iID) {
				case 0:
					return 84;
				case 1:
					return 98*32;
			}
		case 24:
			switch(iID) {
				case 0:
					return 84;
				case 1:
					return 98*32;
			}
		case 25:
			switch(iID) {
				case 0:
					return 84;
				case 1:
					return 86*32;
			}
		case 26:
			switch(iID) {
				case 0:
					return 84;
				case 1:
					return 113*32;
			}
	}

	return 84;
}

int Map::getSpawnPointYPos(int iID) {
	switch(currentLevelID) {
		case 1:
			switch(iID) {
				case 0:
					return 64;
			}
		case 5: case 25:
			switch(iID) {
				case 0:
					return 64;
				case 1:
					return CCFG::GAME_HEIGHT - 48 - oPlayer->getHitBoxY();;
			}
		case 3: case 7: case 11: case 15: case 19: case 23: case 27: case 31:
			return 150;
	}

	return CCFG::GAME_HEIGHT - 48 - oPlayer->getHitBoxY();
}

void Map::setSpawnPoint() {
	float X = (float)getSpawnPointXPos(iSpawnPointID);

	if(X > 6*32) {
		fXPos = -(X - 6*32);
		oPlayer->setXPos(6*32);
		oPlayer->setYPos((float)getSpawnPointYPos(iSpawnPointID));
	} else {
		fXPos = 0;
		oPlayer->setXPos(X);
		oPlayer->setYPos((float)getSpawnPointYPos(iSpawnPointID));
	}

	oPlayer->setMoveDirection(true);
}

void Map::resetGameData() {
	this->currentLevelID = 0;
	this->iSpawnPointID = 0;

	oPlayer->setCoins(0);
	oPlayer->setScore(0);
	oPlayer->resetPowerLVL();

	oPlayer->setNumOfLives(10);

	setSpawnPoint();

	loadLVL();
}

void Map::loadLVL() {
	clearPipeEvents();

	switch(currentLevelID) {
		case 0:
			loadLVL_1_1();
			break;
		case 1:
			loadLVL_1_2();
			break;
		case 2:
			loadLVL_1_3();
			break;
		case 3:
			loadLVL_1_4();
			break;
		case 4:
			loadLVL_2_1();
			break;
		case 5:
			loadLVL_2_2();
			break;
		case 6:
			loadLVL_2_3();
			break;
		case 7:
			loadLVL_2_4();
			break;
		case 8:
			loadLVL_3_1();
			break;
		case 9:
			loadLVL_3_2();
			break;
		case 10:
			loadLVL_3_3();
			break;
		case 11:
			loadLVL_3_4();
			break;
		case 12:
			loadLVL_4_1();
			break;
		case 13:
			loadLVL_4_2();
			break;
		case 14:
			loadLVL_4_3();
			break;
		case 15:
			loadLVL_4_4();
			break;
		case 16:
			loadLVL_5_1();
			break;
		case 17:
			loadLVL_5_2();
			break;
		case 18:
			loadLVL_5_3();
			break;
		case 19:
			loadLVL_5_4();
			break;
		case 20:
			loadLVL_6_1();
			break;
		case 21:
			loadLVL_6_2();
			break;
		case 22:
			loadLVL_6_3();
			break;
		case 23:
			loadLVL_6_4();
			break;
		case 24:
			loadLVL_7_1();
			break;
		case 25:
			loadLVL_7_2();
			break;
		case 26:
			loadLVL_7_3();
			break;
		case 27:
			loadLVL_7_4();
			break;
		case 28:
			loadLVL_8_1();
			break;
		case 29:
			loadLVL_8_2();
			break;
		case 30:
			loadLVL_8_3();
			break;
		case 31:
			loadLVL_8_4();
			break;
	}
}

// ---------- LEVELTEXT -----

void Map::clearLevelText() {
	for(unsigned int i = 0; i < vLevelText.size(); i++) {
		delete vLevelText[i];
	}

	vLevelText.clear();
}

// ---------- LOADPIPEEVENTS -----

void Map::clearPipeEvents() {
	for(unsigned int i = 0; i < lPipe.size(); i++) {
		delete lPipe[i];
	}

	lPipe.clear();
}

/* ******************************************** */

// ----- POS 0 = TOP, 1 = BOT
bool Map::blockUse(int nX, int nY, int iBlockID, int POS) {
	if(POS == 0) {
		switch(iBlockID) {
			case 8: case 55: // ----- BlockQ
				if(lMap[nX][nY]->getSpawnMushroom()) {
					if(lMap[nX][nY]->getPowerUP()) {
						if(oPlayer->getPowerLVL() == 0) {
							lMinion[getListID(32 * nX)].push_back(new Mushroom(32 * nX, CCFG::GAME_HEIGHT - 16 - 32 * nY, true, nX, nY));
						} else {
							lMinion[getListID(32 * nX)].push_back(new Flower(32 * nX, CCFG::GAME_HEIGHT - 16 - 32 * nY, nX, nY));
						}
					} else {
						lMinion[getListID(32 * nX)].push_back(new Mushroom(32 * nX, CCFG::GAME_HEIGHT - 16 - 32 * nY, false, nX, nY));
					}
					CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cMUSHROOMAPPER);
				} else {
					lCoin.push_back(new Coin(nX * 32 + 7, CCFG::GAME_HEIGHT - nY * 32 - 48));
					oPlayer->setScore(oPlayer->getScore() + 200);
					CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cCOIN);
					oPlayer->setCoins(oPlayer->getCoins() + 1);
				}

				if(lMap[nX][nY]->getNumOfUse() > 1) {
					lMap[nX][nY]->setNumOfUse(lMap[nX][nY]->getNumOfUse() - 1);
				} else {
					lMap[nX][nY]->setNumOfUse(0);
					lMap[nX][nY]->setBlockID(iLevelType == 0 || iLevelType == 4 ? 9 : iLevelType == 1 ? 56 : 56);
				}
				
				lMap[nX][nY]->startBlockAnimation();
				checkCollisionOnTopOfTheBlock(nX, nY);
				break;
			case 13: case 28: case 81: // ----- Brick
				if(lMap[nX][nY]->getSpawnStar()) {
					lMap[nX][nY]->setBlockID(iLevelType == 0 || iLevelType == 4 ? 9 : iLevelType == 1 ? 56 : 80);
					lMinion[getListID(32 * nX)].push_back(new Star(32 * nX, CCFG::GAME_HEIGHT - 16 - 32 * nY, nX, nY));
					lMap[nX][nY]->startBlockAnimation();
					CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cMUSHROOMAPPER);
				} else if(lMap[nX][nY]->getSpawnMushroom()) {
					lMap[nX][nY]->setBlockID(iLevelType == 0 || iLevelType == 4 ? 9 : iLevelType == 1 ? 56 : 80);
					if(lMap[nX][nY]->getPowerUP()) {
						if(oPlayer->getPowerLVL() == 0) {
							lMinion[getListID(32 * nX)].push_back(new Mushroom(32 * nX, CCFG::GAME_HEIGHT - 16 - 32 * nY, true, nX, nY));
						} else {
							lMinion[getListID(32 * nX)].push_back(new Flower(32 * nX, CCFG::GAME_HEIGHT - 16 - 32 * nY, nX, nY));
						}
					} else {
						lMinion[getListID(32 * nX)].push_back(new Mushroom(32 * nX, CCFG::GAME_HEIGHT - 16 - 32 * nY, false, nX, nY));
					}
					lMap[nX][nY]->startBlockAnimation();
					CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cMUSHROOMAPPER);
				} else if(lMap[nX][nY]->getNumOfUse() > 0) {
					lCoin.push_back(new Coin(nX * 32 + 7, CCFG::GAME_HEIGHT - nY * 32 - 48));
					oPlayer->setScore(oPlayer->getScore() + 200);
					oPlayer->setCoins(oPlayer->getCoins() + 1);

					lMap[nX][nY]->setNumOfUse(lMap[nX][nY]->getNumOfUse() - 1);
					if(lMap[nX][nY]->getNumOfUse() == 0) {
						lMap[nX][nY]->setBlockID(iLevelType == 0 || iLevelType == 4 ? 9 : iLevelType == 1 ? 56 : 80);
					}

					CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cCOIN);

					lMap[nX][nY]->startBlockAnimation();
				} else {
					if(oPlayer->getPowerLVL() > 0) {
						lMap[nX][nY]->setBlockID(0);
						lBlockDebris.push_back(new BlockDebris(nX * 32, CCFG::GAME_HEIGHT - 48 - nY * 32));
						CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cBLOCKBREAK);
					} else {
						lMap[nX][nY]->startBlockAnimation();
						CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cBLOCKHIT);
					}
				}

				checkCollisionOnTopOfTheBlock(nX, nY);
				break;
			case 24: // ----- BlockQ2
				if(lMap[nX][nY]->getSpawnMushroom()) {
					if(lMap[nX][nY]->getPowerUP()) {
						if(oPlayer->getPowerLVL() == 0) {
							lMinion[getListID(32 * nX)].push_back(new Mushroom(32 * nX, CCFG::GAME_HEIGHT - 16 - 32 * nY, true, nX, nY));
						} else {
							lMinion[getListID(32 * nX)].push_back(new Flower(32 * nX, CCFG::GAME_HEIGHT - 16 - 32 * nY, nX, nY));
						}
					} else {
						lMinion[getListID(32 * nX)].push_back(new Mushroom(32 * nX, CCFG::GAME_HEIGHT - 16 - 32 * nY, false, nX, nY));
					}
					CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cMUSHROOMAPPER);
				} else {
					lCoin.push_back(new Coin(nX * 32 + 7, CCFG::GAME_HEIGHT - nY * 32 - 48));
					oPlayer->setCoins(oPlayer->getCoins() + 1);
					oPlayer->setScore(oPlayer->getScore() + 200);
					CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cCOIN);

					lMap[nX][nY]->startBlockAnimation();
				}

				lMap[nX][nY]->setBlockID(iLevelType == 0 || iLevelType == 4 ? 9 : iLevelType == 1 ? 56 : 80);
				checkCollisionOnTopOfTheBlock(nX, nY);
				break;
			case 128: case 129:
				spawnVine(nX, nY, iBlockID);
				lMap[nX][nY]->setBlockID(iBlockID == 128 ? 9 : 56);
				lMap[nX][nY]->startBlockAnimation();
				break;
			default:
				break;
		}
	} else if(POS == 1) {
		switch(iBlockID) {
			case 21: case 23: case 31: case 33: case 98: case 100: case 113: case 115: case 137: case 139: case 177: case 179: // Pipe
				pipeUse();
				break;
			case 40: case 41: case 123: case 124: case 182: // End
				EndUse();
				break;
			case 82:
				EndBoss();
				break;
			default:
				break;
		}
	}
	
	switch(iBlockID) {
		case 29: case 71: case 72: case 73:// COIN
			lMap[nX][nY]->setBlockID(iLevelType == 2 ? 94 : 0);
			oPlayer->addCoin();
			CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cCOIN);
			return false;
			break;
		case 36: case 38: case 60: case 62: case 103: case 105: case 118: case 120: // Pipe
			pipeUse();
			break;
		case 127: // BONUS END
			EndBonus();
			break;
		case 169:
			TPUse();
			break;
		case 170:
			TPUse2();
			break;
		case 171:
			TPUse3();
			break;
		default:
			break;
	}

	return true;
}

void Map::pipeUse() {
	for(unsigned int i = 0; i < lPipe.size(); i++) {
		lPipe[i]->checkUse();
	}
}

void Map::EndUse() {
	inEvent = true;

	oEvent->resetData();
	oPlayer->resetJump();
	oPlayer->stopMove();

	oEvent->newUnderWater = false;

	CCFG::getMusic()->StopMusic();
	CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cLEVELEND);

	oEvent->eventTypeID = oEvent->eEnd;

	if(oPlayer->getYPos() < CCFG::GAME_HEIGHT - 16 - 10*32) {
		oFlag->iPoints = 5000;
	} else if(oPlayer->getYPos() < CCFG::GAME_HEIGHT - 16 - 8*32) {
		oFlag->iPoints = 2000;
	} else if(oPlayer->getYPos() < CCFG::GAME_HEIGHT - 16 - 6*32) {
		oFlag->iPoints = 500;
	} else if(oPlayer->getYPos() < CCFG::GAME_HEIGHT - 16 - 4*32) {
		oFlag->iPoints = 200;
	} else {
		oFlag->iPoints = 100;
	}

	oEvent->vOLDDir.push_back(oEvent->eRIGHTEND);
	oEvent->vOLDLength.push_back(oPlayer->getHitBoxX());

	oEvent->vOLDDir.push_back(oEvent->eENDBOT1);
	oEvent->vOLDLength.push_back((CCFG::GAME_HEIGHT - 16 - 32 * 2) - oPlayer->getYPos() - oPlayer->getHitBoxY() - 2);

	oEvent->vOLDDir.push_back(oEvent->eENDBOT2);
	oEvent->vOLDLength.push_back((CCFG::GAME_HEIGHT - 16 - 32 * 2) - oPlayer->getYPos() - oPlayer->getHitBoxY() - 2);

	oEvent->vOLDDir.push_back(oEvent->eRIGHTEND);
	oEvent->vOLDLength.push_back(oPlayer->getHitBoxX());

	oEvent->vOLDDir.push_back(oEvent->eBOTRIGHTEND);
	oEvent->vOLDLength.push_back(32);

	oEvent->vOLDDir.push_back(oEvent->eRIGHTEND);
	oEvent->vOLDLength.push_back(132);

	oEvent->iSpeed = 3;

	switch(currentLevelID) {
		case 0: {
			oEvent->newLevelType = 100;

			oEvent->iDelay = 1500;
			oEvent->newCurrentLevel = 1;
			oEvent->inEvent = true;

			oEvent->newMapXPos = -210*32;
			oEvent->newPlayerXPos = 64;
			oEvent->newPlayerYPos = CCFG::GAME_HEIGHT - 48 - oPlayer->getHitBoxY();
			oEvent->newMoveMap = false;

			oPlayer->setMoveDirection(true);

			oEvent->reDrawX.push_back(204);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(204);
			oEvent->reDrawY.push_back(3);
			oEvent->reDrawX.push_back(205);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(205);
			oEvent->reDrawY.push_back(3);
			break;
		}
		case 1: {
			oEvent->newLevelType = 0;

			oEvent->iDelay = 1500;
			oEvent->newCurrentLevel = 2;
			oEvent->inEvent = false;

			oEvent->newMapXPos = 0;
			oEvent->newPlayerXPos = 84;
			oEvent->newPlayerYPos = CCFG::GAME_HEIGHT - 48 - oPlayer->getHitBoxY();
			oEvent->newMoveMap = true;

			oPlayer->setMoveDirection(true);

			oEvent->reDrawX.push_back(309);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(309);
			oEvent->reDrawY.push_back(3);
			oEvent->reDrawX.push_back(310);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(310);
			oEvent->reDrawY.push_back(3);
			break;
		}
		case 2: {
			oEvent->newLevelType = 3;

			oEvent->iDelay = 1500;
			oEvent->newCurrentLevel = 3;
			oEvent->inEvent = false;

			oEvent->newMapXPos = 0;
			oEvent->newPlayerXPos = 84;
			oEvent->newPlayerYPos = 150;
			oEvent->newMoveMap = true;

			oPlayer->setMoveDirection(true);

			oEvent->vOLDDir.push_back(oEvent->eRIGHT);
			oEvent->vOLDLength.push_back(32);

			oFlag->castleFlagExtraXPos = 32;

			oEvent->reDrawX.push_back(159);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(159);
			oEvent->reDrawY.push_back(3);
			oEvent->reDrawX.push_back(160);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(160);
			oEvent->reDrawY.push_back(3);
			break;
		}
		case 4: {
			oEvent->newLevelType = 100;

			oEvent->iDelay = 1500;
			oEvent->newCurrentLevel = 5;
			oEvent->inEvent = true;

			oEvent->newMapXPos = -220*32;
			oEvent->newPlayerXPos = 64;
			oEvent->newPlayerYPos = CCFG::GAME_HEIGHT - 48 - oPlayer->getHitBoxY();
			oEvent->newMoveMap = false;

			oPlayer->setMoveDirection(true);

			oEvent->reDrawX.push_back(206);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(206);
			oEvent->reDrawY.push_back(3);
			oEvent->reDrawX.push_back(207);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(207);
			oEvent->reDrawY.push_back(3);
			break;
		}
		case 5: {
			oEvent->newLevelType = 0;

			oEvent->iDelay = 1500;
			oEvent->newCurrentLevel = 6;
			oEvent->inEvent = false;

			oEvent->newMapXPos = 0;
			oEvent->newPlayerXPos = 84;
			oEvent->newPlayerYPos = CCFG::GAME_HEIGHT - 48 - oPlayer->getHitBoxY();
			oEvent->newMoveMap = true;

			oPlayer->setMoveDirection(true);

			oEvent->vOLDDir.push_back(oEvent->eRIGHT);
			oEvent->vOLDLength.push_back(32);

			oFlag->castleFlagExtraXPos = 32;

			oEvent->reDrawX.push_back(299);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(299);
			oEvent->reDrawY.push_back(3);
			oEvent->reDrawX.push_back(300);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(300);
			oEvent->reDrawY.push_back(3);
			break;
		}
		case 6:
			oEvent->newLevelType = 3;

			oEvent->iDelay = 1500;
			oEvent->newCurrentLevel = currentLevelID + 1;
			oEvent->inEvent = false;

			oEvent->newMapXPos = 0;
			oEvent->newPlayerXPos = 84;
			oEvent->newPlayerYPos = 150;
			oEvent->newMoveMap = true;

			oPlayer->setMoveDirection(true);

			oEvent->vOLDDir.push_back(oEvent->eRIGHT);
			oEvent->vOLDLength.push_back(32);

			oFlag->castleFlagExtraXPos = 32;

			oEvent->reDrawX.push_back(232);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(232);
			oEvent->reDrawY.push_back(3);
			oEvent->reDrawX.push_back(234);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(234);
			oEvent->reDrawY.push_back(3);
			break;
		case 8:
			oEvent->newLevelType = 4;

			oEvent->iDelay = 1500;
			oEvent->newCurrentLevel = currentLevelID + 1;
			oEvent->inEvent = false;

			oEvent->newMapXPos = 0;
			oEvent->newPlayerXPos = 84;
			oEvent->newPlayerYPos = CCFG::GAME_HEIGHT - 48 - oPlayer->getHitBoxY();
			oEvent->newMoveMap = true;

			oPlayer->setMoveDirection(true);

			oEvent->reDrawX.push_back(206);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(206);
			oEvent->reDrawY.push_back(3);
			oEvent->reDrawX.push_back(207);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(207);
			oEvent->reDrawY.push_back(3);
			break;
		case 9:
			oEvent->newLevelType = 4;

			oEvent->iDelay = 1500;
			oEvent->newCurrentLevel = currentLevelID + 1;
			oEvent->inEvent = false;

			oEvent->newMapXPos = 0;
			oEvent->newPlayerXPos = 84;
			oEvent->newPlayerYPos = CCFG::GAME_HEIGHT - 48 - oPlayer->getHitBoxY();
			oEvent->newMoveMap = true;

			oPlayer->setMoveDirection(true);

			oEvent->reDrawX.push_back(215);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(215);
			oEvent->reDrawY.push_back(3);
			oEvent->reDrawX.push_back(216);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(216);
			oEvent->reDrawY.push_back(3);
			break;
		case 10:
			oEvent->newLevelType = 3;

			oEvent->iDelay = 1500;
			oEvent->newCurrentLevel = currentLevelID + 1;
			oEvent->inEvent = false;

			oEvent->newMapXPos = 0;
			oEvent->newPlayerXPos = 84;
			oEvent->newPlayerYPos = 150;
			oEvent->newMoveMap = true;

			oEvent->vOLDDir.push_back(oEvent->eRIGHT);
			oEvent->vOLDLength.push_back(32);

			oFlag->castleFlagExtraXPos = 32;

			oPlayer->setMoveDirection(true);

			oEvent->reDrawX.push_back(158);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(158);
			oEvent->reDrawY.push_back(3);
			oEvent->reDrawX.push_back(159);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(159);
			oEvent->reDrawY.push_back(3);
			break;
		case 12: {
			oEvent->newLevelType = 100;

			oEvent->iDelay = 1500;
			oEvent->newCurrentLevel = currentLevelID + 1;
			oEvent->inEvent = true;

			oEvent->newMapXPos = -240*32;
			oEvent->newPlayerXPos = 64;
			oEvent->newPlayerYPos = CCFG::GAME_HEIGHT - 48 - oPlayer->getHitBoxY();
			oEvent->newMoveMap = false;

			oPlayer->setMoveDirection(true);

			oEvent->reDrawX.push_back(231);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(231);
			oEvent->reDrawY.push_back(3);
			oEvent->reDrawX.push_back(232);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(232);
			oEvent->reDrawY.push_back(3);
			break;
		case 13:
			oEvent->newLevelType = 0;

			oEvent->iDelay = 1500;
			oEvent->newCurrentLevel = currentLevelID + 1;
			oEvent->inEvent = false;

			oEvent->newMapXPos = 0;
			oEvent->newPlayerXPos = 84;
			oEvent->newPlayerYPos = CCFG::GAME_HEIGHT - 48 - oPlayer->getHitBoxY();
			oEvent->newMoveMap = true;

			oPlayer->setMoveDirection(true);

			oEvent->reDrawX.push_back(419);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(419);
			oEvent->reDrawY.push_back(3);
			oEvent->reDrawX.push_back(420);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(420);
			oEvent->reDrawY.push_back(3);
			break;
		case 14:
			oEvent->newLevelType = 3;

			oEvent->iDelay = 1500;
			oEvent->newCurrentLevel = currentLevelID + 1;
			oEvent->inEvent = false;

			oEvent->newMapXPos = 0;
			oEvent->newPlayerXPos = 84;
			oEvent->newPlayerYPos = 150;
			oEvent->newMoveMap = true;

			oEvent->vOLDDir.push_back(oEvent->eRIGHT);
			oEvent->vOLDLength.push_back(32);

			oFlag->castleFlagExtraXPos = 32;

			oPlayer->setMoveDirection(true);

			oEvent->reDrawX.push_back(154);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(154);
			oEvent->reDrawY.push_back(3);
			oEvent->reDrawX.push_back(155);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(155);
			oEvent->reDrawY.push_back(3);
			break;
		}
		case 16: {
			oEvent->newLevelType = 0;

			oEvent->iDelay = 1500;
			oEvent->newCurrentLevel = currentLevelID + 1;
			oEvent->inEvent = false;

			oEvent->newMapXPos = -80*32;
			oEvent->newPlayerXPos = 84;
			oEvent->newPlayerYPos = CCFG::GAME_HEIGHT - 48 - oPlayer->getHitBoxY();
			oEvent->newMoveMap = true;

			oPlayer->setMoveDirection(true);

			oEvent->reDrawX.push_back(205);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(205);
			oEvent->reDrawY.push_back(3);
			oEvent->reDrawX.push_back(206);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(206);
			oEvent->reDrawY.push_back(3);
			break;
		}
		case 17: {
			oEvent->newLevelType = 0;

			oEvent->iDelay = 1500;
			oEvent->newCurrentLevel = currentLevelID + 1;
			oEvent->inEvent = false;

			oEvent->newMapXPos = 0;
			oEvent->newPlayerXPos = 84;
			oEvent->newPlayerYPos = CCFG::GAME_HEIGHT - 48 - oPlayer->getHitBoxY();
			oEvent->newMoveMap = true;

			oPlayer->setMoveDirection(true);

			oEvent->reDrawX.push_back(286);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(286);
			oEvent->reDrawY.push_back(3);
			oEvent->reDrawX.push_back(287);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(287);
			oEvent->reDrawY.push_back(3);
			break;
		}
		case 18: {
			oEvent->newLevelType = 3;

			oEvent->iDelay = 1500;
			oEvent->newCurrentLevel = currentLevelID + 1;
			oEvent->inEvent = false;

			oEvent->newMapXPos = 0;
			oEvent->newPlayerXPos = 84;
			oEvent->newPlayerYPos = 150;
			oEvent->newMoveMap = true;

			oEvent->vOLDDir.push_back(oEvent->eRIGHT);
			oEvent->vOLDLength.push_back(32);

			oFlag->castleFlagExtraXPos = 32;

			oPlayer->setMoveDirection(true);

			oEvent->reDrawX.push_back(159);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(159);
			oEvent->reDrawY.push_back(3);
			oEvent->reDrawX.push_back(160);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(160);
			oEvent->reDrawY.push_back(3);
			break;
		}
		case 20: {
			oEvent->newLevelType = 4;

			oEvent->iDelay = 1500;
			oEvent->newCurrentLevel = currentLevelID + 1;
			oEvent->inEvent = false;

			oEvent->newMapXPos = -85*32;
			oEvent->newPlayerXPos = 84;
			oEvent->newPlayerYPos = CCFG::GAME_HEIGHT - 48 - oPlayer->getHitBoxY();
			oEvent->newMoveMap = true;

			oEvent->vOLDDir.push_back(oEvent->eRIGHT);
			oEvent->vOLDLength.push_back(96);

			oFlag->castleFlagExtraXPos = 96;

			oPlayer->setMoveDirection(true);

			oEvent->reDrawX.push_back(194);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(194);
			oEvent->reDrawY.push_back(3);
			oEvent->reDrawX.push_back(195);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(195);
			oEvent->reDrawY.push_back(3);
			break;
		}
		case 21: {
			oEvent->newLevelType = 4;

			oEvent->iDelay = 1500;
			oEvent->newCurrentLevel = currentLevelID + 1;
			oEvent->inEvent = false;

			oEvent->newMapXPos = 0;
			oEvent->newPlayerXPos = 84;
			oEvent->newPlayerYPos = CCFG::GAME_HEIGHT - 48 - oPlayer->getHitBoxY();
			oEvent->newMoveMap = true;

			oEvent->vOLDDir.push_back(oEvent->eRIGHT);
			oEvent->vOLDLength.push_back(32);

			oFlag->castleFlagExtraXPos = 32;

			oPlayer->setMoveDirection(true);

			oEvent->reDrawX.push_back(307);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(307);
			oEvent->reDrawY.push_back(3);
			oEvent->reDrawX.push_back(308);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(308);
			oEvent->reDrawY.push_back(3);
			break;
		}
		case 22: {
			oEvent->newLevelType = 3;

			oEvent->iDelay = 1500;
			oEvent->newCurrentLevel = currentLevelID + 1;
			oEvent->inEvent = false;

			oEvent->newMapXPos = 0;
			oEvent->newPlayerXPos = 84;
			oEvent->newPlayerYPos = 150;
			oEvent->newMoveMap = true;

			oEvent->vOLDDir.push_back(oEvent->eRIGHT);
			oEvent->vOLDLength.push_back(32);

			oFlag->castleFlagExtraXPos = 32;

			oPlayer->setMoveDirection(true);

			oEvent->reDrawX.push_back(174);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(174);
			oEvent->reDrawY.push_back(3);
			oEvent->reDrawX.push_back(175);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(175);
			oEvent->reDrawY.push_back(3);
			break;
		}
		case 24: {
			oEvent->newLevelType = 100;

			oEvent->iDelay = 1500;
			oEvent->newCurrentLevel = currentLevelID + 1;
			oEvent->inEvent = true;

			oEvent->newMapXPos = -220*32;
			oEvent->newPlayerXPos = 64;
			oEvent->newPlayerYPos = CCFG::GAME_HEIGHT - 48 - oPlayer->getHitBoxY();
			oEvent->newMoveMap = false;

			oPlayer->setMoveDirection(true);

			oEvent->reDrawX.push_back(185);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(185);
			oEvent->reDrawY.push_back(3);
			oEvent->reDrawX.push_back(186);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(186);
			oEvent->reDrawY.push_back(3);
			break;
		}
		case 25: {
			oEvent->newLevelType = 0;

			oEvent->iDelay = 1500;
			oEvent->newCurrentLevel = currentLevelID + 1;
			oEvent->inEvent = false;

			oEvent->newMapXPos = 0;
			oEvent->newPlayerXPos = 84;
			oEvent->newPlayerYPos = CCFG::GAME_HEIGHT - 48 - oPlayer->getHitBoxY();
			oEvent->newMoveMap = true;

			oPlayer->setMoveDirection(true);

			oEvent->vOLDDir.push_back(oEvent->eRIGHT);
			oEvent->vOLDLength.push_back(32);

			oFlag->castleFlagExtraXPos = 32;

			oEvent->reDrawX.push_back(299);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(299);
			oEvent->reDrawY.push_back(3);
			oEvent->reDrawX.push_back(300);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(300);
			oEvent->reDrawY.push_back(3);
			break;
		}
		case 26:
			oEvent->newLevelType = 3;

			oEvent->iDelay = 1500;
			oEvent->newCurrentLevel = currentLevelID + 1;
			oEvent->inEvent = false;

			oEvent->newMapXPos = 0;
			oEvent->newPlayerXPos = 84;
			oEvent->newPlayerYPos = 150;
			oEvent->newMoveMap = true;

			oPlayer->setMoveDirection(true);

			oEvent->vOLDDir.push_back(oEvent->eRIGHT);
			oEvent->vOLDLength.push_back(32);

			oFlag->castleFlagExtraXPos = 32;

			oEvent->reDrawX.push_back(232);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(232);
			oEvent->reDrawY.push_back(3);
			oEvent->reDrawX.push_back(233);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(233);
			oEvent->reDrawY.push_back(3);
			break;
		case 28: {
			oEvent->newLevelType = 0;

			oEvent->iDelay = 1500;
			oEvent->newCurrentLevel = currentLevelID + 1;
			oEvent->inEvent = false;

			oEvent->newMapXPos = 0;
			oEvent->newPlayerXPos = 84;
			oEvent->newPlayerYPos = CCFG::GAME_HEIGHT - 48 - oPlayer->getHitBoxY();
			oEvent->newMoveMap = true;

			oPlayer->setMoveDirection(true);

			oEvent->reDrawX.push_back(382);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(382);
			oEvent->reDrawY.push_back(3);
			oEvent->reDrawX.push_back(383);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(383);
			oEvent->reDrawY.push_back(3);
			break;
		}
		case 29: {
			oEvent->newLevelType = 0;

			oEvent->iDelay = 1500;
			oEvent->newCurrentLevel = currentLevelID + 1;
			oEvent->inEvent = false;

			oEvent->newMapXPos = 0;
			oEvent->newPlayerXPos = 84;
			oEvent->newPlayerYPos = CCFG::GAME_HEIGHT - 48 - oPlayer->getHitBoxY();
			oEvent->newMoveMap = true;

			oPlayer->setMoveDirection(true);

			oEvent->reDrawX.push_back(222);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(222);
			oEvent->reDrawY.push_back(3);
			oEvent->reDrawX.push_back(223);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(223);
			oEvent->reDrawY.push_back(3);
			break;
		}
		case 30: {
			oEvent->newLevelType = 3;

			oEvent->iDelay = 1500;
			oEvent->newCurrentLevel = currentLevelID + 1;
			oEvent->inEvent = false;

			oEvent->newMapXPos = 0;
			oEvent->newPlayerXPos = 84;
			oEvent->newPlayerYPos = 150;
			oEvent->newMoveMap = true;

			oPlayer->setMoveDirection(true);

			oEvent->vOLDDir.push_back(oEvent->eRIGHT);
			oEvent->vOLDLength.push_back(128);

			oFlag->castleFlagExtraXPos = 128;

			oEvent->reDrawX.push_back(224);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(224);
			oEvent->reDrawY.push_back(3);
			oEvent->reDrawX.push_back(225);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(225);
			oEvent->reDrawY.push_back(3);
			break;
		}
	}

	oEvent->vOLDDir.push_back(oEvent->eENDPOINTS);
	oEvent->vOLDLength.push_back(iMapTime);

	oEvent->vOLDDir.push_back(oEvent->eNOTHING);
	oEvent->vOLDLength.push_back(128);
}

void Map::EndBoss() {
	inEvent = true;

	oEvent->resetData();
	oPlayer->resetJump();
	oPlayer->stopMove();

	oEvent->endGame = false;

	switch(currentLevelID) {
		case 31:
			oEvent->endGame = true;
			break;
		default:
			CCFG::getMusic()->StopMusic();
			CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cCASTLEEND);
			break;
	}

	oEvent->eventTypeID = oEvent->eBossEnd;

	oEvent->iSpeed = 3;

	oEvent->newLevelType = 0;
	oEvent->newCurrentLevel = currentLevelID + 1;
	oEvent->newMoveMap = true;
	oEvent->iDelay = 500;
	oEvent->inEvent = false;

	oEvent->newMapXPos = 0;
	oEvent->newPlayerXPos = 64;
	oEvent->newPlayerYPos = CCFG::GAME_HEIGHT - 48 - oPlayer->getHitBoxY();
	oEvent->newMoveMap = true;

	switch(currentLevelID) {
		case 7:
			oEvent->newLevelType = 4;
			break;
		case 19:
			oEvent->newLevelType = 4;
			break;
	}

	bool addOne = false;

	if(lMap[getBlockIDX((int)(oPlayer->getXPos() + oPlayer->getHitBoxX()/2 - fXPos))][6]->getBlockID() == 82) {
		oEvent->vOLDDir.push_back(oEvent->eBOT);
		oEvent->vOLDLength.push_back(CCFG::GAME_HEIGHT - 16 - 5*32 - (oPlayer->getYPos() + oPlayer->getHitBoxY()));
	} else {
		oEvent->vOLDDir.push_back(oEvent->eBOTRIGHTEND);
		oEvent->vOLDLength.push_back(CCFG::GAME_HEIGHT - 16 - 5*32 - (oPlayer->getYPos() + oPlayer->getHitBoxY()));

		oEvent->vOLDDir.push_back(oEvent->eRIGHT);
		oEvent->vOLDLength.push_back(32 - CCFG::GAME_HEIGHT - 16 - 5*32 - (oPlayer->getYPos() + oPlayer->getHitBoxY()));
		addOne = true;
	}

	oEvent->vOLDDir.push_back(oEvent->eBOSSEND1);
	oEvent->vOLDLength.push_back(1);
	oEvent->vOLDDir.push_back(oEvent->eNOTHING);
	oEvent->vOLDLength.push_back(10);

	oEvent->vOLDDir.push_back(oEvent->eBOSSEND2);
	oEvent->vOLDLength.push_back(1);
	oEvent->vOLDDir.push_back(oEvent->eNOTHING);
	oEvent->vOLDLength.push_back(3);

	for(int i = 0; i < 6; i++) {
		oEvent->vOLDDir.push_back(oEvent->eBOSSEND3);
		oEvent->vOLDLength.push_back(2 + i);
		oEvent->vOLDDir.push_back(oEvent->eNOTHING);
		oEvent->vOLDLength.push_back(3);
	}
	
	oEvent->vOLDDir.push_back(oEvent->eBOSSEND4);
		oEvent->vOLDLength.push_back(1);

	for(int i = 6; i < 12; i++) {
		oEvent->vOLDDir.push_back(oEvent->eBOSSEND3);
		oEvent->vOLDLength.push_back(2 + i);
		oEvent->vOLDDir.push_back(oEvent->eNOTHING);
		oEvent->vOLDLength.push_back(3);
	}

	oEvent->vOLDDir.push_back(oEvent->eNOTHING);
	oEvent->vOLDLength.push_back(90);

	if(currentLevelID == 31) {
		CCFG::getMusic()->StopMusic();
		CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cPRINCESSMUSIC);
	}

	oEvent->vOLDDir.push_back(oEvent->eBOTRIGHTBOSS);
	oEvent->vOLDLength.push_back(8*32);

	switch(currentLevelID) {
		case 31:
			oEvent->vOLDDir.push_back(oEvent->eENDGAMEBOSSTEXT1);
			oEvent->vOLDLength.push_back(getBlockIDX((int)(oPlayer->getXPos() + oPlayer->getHitBoxX()/2 - fXPos))*32 + 7*32 + (addOne ? 32 : 0));
		break;
		default:
			oEvent->vOLDDir.push_back(oEvent->eBOSSTEXT1);
			oEvent->vOLDLength.push_back(getBlockIDX((int)(oPlayer->getXPos() + oPlayer->getHitBoxX()/2 - fXPos))*32 + 7*32 + (addOne ? 32 : 0));
		break;
	}

	oEvent->vOLDDir.push_back(oEvent->eRIGHT);
	oEvent->vOLDLength.push_back(2*32 + 16);

	oEvent->vOLDDir.push_back(oEvent->eMARIOSPRITE1);
	oEvent->vOLDLength.push_back(1);

	oEvent->vOLDDir.push_back(oEvent->eNOTHING);
	oEvent->vOLDLength.push_back(90);

	switch(currentLevelID) {
		case 31:
			oEvent->vOLDDir.push_back(oEvent->eENDGAMEBOSSTEXT2);
			oEvent->vOLDLength.push_back(getBlockIDX((int)(oPlayer->getXPos() + oPlayer->getHitBoxX()/2 - fXPos))*32 + 5*32 + (addOne ? 32 : 0) + 28);
			break;
		default:
			oEvent->vOLDDir.push_back(oEvent->eBOSSTEXT2);
			oEvent->vOLDLength.push_back(getBlockIDX((int)(oPlayer->getXPos() + oPlayer->getHitBoxX()/2 - fXPos))*32 + 5*32 + (addOne ? 32 : 0));
			break;
	}

	oEvent->vOLDDir.push_back(oEvent->eNOTHING);
	oEvent->vOLDLength.push_back(300 + (currentLevelID == 31 ? 100 : 0));

	switch(currentLevelID) {
		case 31:
			oEvent->vOLDDir.push_back(oEvent->eNOTHING);
			oEvent->vOLDLength.push_back(90);
		break;
	}
}

void Map::EndBonus() {
	inEvent = true;

	oEvent->resetData();
	oPlayer->resetJump();
	oPlayer->stopMove();

	oEvent->eventTypeID = oEvent->eNormal;

	oEvent->iSpeed = 3;

	oEvent->newLevelType = iLevelType;
	oEvent->newCurrentLevel = currentLevelID;
	oEvent->newMoveMap = true;
	oEvent->iDelay = 0;
	oEvent->inEvent = false;

	oEvent->newMoveMap = true;

	switch(currentLevelID) {
		case 4: {
			oEvent->newMapXPos = -158*32 + 16;
			oEvent->newPlayerXPos = 128;
			oEvent->newPlayerYPos = -oPlayer->getHitBoxY();
			
			break;
		}
		case 8: {
			oEvent->newMapXPos = -158*32 + 16;
			oEvent->newPlayerXPos = 128;
			oEvent->newPlayerYPos = -oPlayer->getHitBoxY();
			break;
		}
		case 17: {
			oEvent->newMapXPos = -207*32 + 16;
			oEvent->newPlayerXPos = 128;
			oEvent->newPlayerYPos = -oPlayer->getHitBoxY();
			break;
		}
		case 21: {
			oEvent->newMapXPos = -243*32 + 16;
			oEvent->newPlayerXPos = 128;
			oEvent->newPlayerYPos = -oPlayer->getHitBoxY();
			break;
		}
	}

	oEvent->vOLDDir.push_back(oEvent->eNOTHING);
	oEvent->vOLDLength.push_back(1);
}

void Map::playerDeath(bool animation, bool instantDeath) {
	if((oPlayer->getPowerLVL() == 0 && !oPlayer->getUnkillAble()) || instantDeath) {
		inEvent = true;

		oEvent->resetData();
		oPlayer->resetJump();
		oPlayer->stopMove();

		oEvent->iDelay = 150;
		oEvent->newCurrentLevel = currentLevelID;

		oEvent->newMoveMap = bMoveMap;

		oEvent->eventTypeID = oEvent->eNormal;

		oPlayer->resetPowerLVL();

		if(animation) {
			oEvent->iSpeed = 4;
			oEvent->newLevelType = iLevelType;

			oPlayer->setYPos(oPlayer->getYPos() + 4.0f);

			oEvent->vOLDDir.push_back(oEvent->eDEATHNOTHING);
			oEvent->vOLDLength.push_back(30);

			oEvent->vOLDDir.push_back(oEvent->eDEATHTOP);
			oEvent->vOLDLength.push_back(64);

			oEvent->vOLDDir.push_back(oEvent->eDEATHBOT);
			oEvent->vOLDLength.push_back(CCFG::GAME_HEIGHT - oPlayer->getYPos() + 128);
		} else {
			oEvent->iSpeed = 4;
			oEvent->newLevelType = iLevelType;

			oEvent->vOLDDir.push_back(oEvent->eDEATHTOP);
			oEvent->vOLDLength.push_back(1);
		}

		oEvent->vOLDDir.push_back(oEvent->eNOTHING);
		oEvent->vOLDLength.push_back(64);

		if(oPlayer->getNumOfLives() > 1) {
			oEvent->vOLDDir.push_back(oEvent->eLOADINGMENU);
			oEvent->vOLDLength.push_back(90);

			oPlayer->setNumOfLives(oPlayer->getNumOfLives() - 1);

			CCFG::getMusic()->StopMusic();
			CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cDEATH);
		} else {
			oEvent->vOLDDir.push_back(oEvent->eGAMEOVER);
			oEvent->vOLDLength.push_back(90);

			oPlayer->setNumOfLives(oPlayer->getNumOfLives() - 1);

			CCFG::getMusic()->StopMusic();
			CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cDEATH);
		}
	} else if(!oPlayer->getUnkillAble()) {
		oPlayer->setPowerLVL(oPlayer->getPowerLVL() - 1);
	}
}

void Map::startLevelAnimation() {
	oEvent->newUnderWater = false;

	switch(currentLevelID) {
		case 0:

			break;
		case 1:
			oEvent->resetData();
			oPlayer->resetJump();
			oPlayer->stopMove();

			oEvent->iSpeed = 2;
			oEvent->newLevelType = 1;

			oEvent->iDelay = 150;
			oEvent->newCurrentLevel = 1;

			oEvent->newMapXPos = 0;
			oEvent->newPlayerXPos = 96;
			oEvent->newPlayerYPos = 64;
			oEvent->newMoveMap = true;

			oEvent->vOLDDir.push_back(oEvent->eRIGHT);
			oEvent->vOLDLength.push_back(7 * 32 + 4);

			oEvent->vOLDDir.push_back(oEvent->ePLAYPIPERIGHT);
			oEvent->vOLDLength.push_back(1);

			oEvent->vOLDDir.push_back(oEvent->eRIGHT);
			oEvent->vOLDLength.push_back(1 * 32 - 2);

			oEvent->vOLDDir.push_back(oEvent->eNOTHING);
			oEvent->vOLDLength.push_back(75);

			oEvent->reDrawX.push_back(220);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(220);
			oEvent->reDrawY.push_back(3);
			break;
		case 5:
			oEvent->resetData();
			oPlayer->resetJump();
			oPlayer->stopMove();

			oEvent->iSpeed = 2;
			oEvent->newLevelType = 2;

			oEvent->iDelay = 150;
			oEvent->newCurrentLevel = 5;

			oEvent->newMapXPos = 0;
			oEvent->newPlayerXPos = 96;
			oEvent->newPlayerYPos = 64;
			oEvent->newMoveMap = true;
			oEvent->newUnderWater = true;

			oEvent->vOLDDir.push_back(oEvent->eRIGHT);
			oEvent->vOLDLength.push_back(7 * 32 + 4);

			oEvent->vOLDDir.push_back(oEvent->ePLAYPIPERIGHT);
			oEvent->vOLDLength.push_back(1);

			oEvent->vOLDDir.push_back(oEvent->eRIGHT);
			oEvent->vOLDLength.push_back(1 * 32 - 2);

			oEvent->vOLDDir.push_back(oEvent->eNOTHING);
			oEvent->vOLDLength.push_back(75);

			oEvent->reDrawX.push_back(230);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(230);
			oEvent->reDrawY.push_back(3);
			break;
		case 13:
			oEvent->resetData();
			oPlayer->resetJump();
			oPlayer->stopMove();

			oEvent->iSpeed = 2;
			oEvent->newLevelType = 1;

			oEvent->iDelay = 150;
			oEvent->newCurrentLevel = currentLevelID;

			oEvent->newMapXPos = 0;
			oEvent->newPlayerXPos = 96;
			oEvent->newPlayerYPos = 64;
			oEvent->newMoveMap = true;

			oEvent->vOLDDir.push_back(oEvent->eRIGHT);
			oEvent->vOLDLength.push_back(7 * 32 + 4);

			oEvent->vOLDDir.push_back(oEvent->ePLAYPIPERIGHT);
			oEvent->vOLDLength.push_back(1);

			oEvent->vOLDDir.push_back(oEvent->eRIGHT);
			oEvent->vOLDLength.push_back(1 * 32 - 2);

			oEvent->vOLDDir.push_back(oEvent->eNOTHING);
			oEvent->vOLDLength.push_back(75);

			oEvent->reDrawX.push_back(250);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(250);
			oEvent->reDrawY.push_back(3);
			break;
		case 25:
			oEvent->resetData();
			oPlayer->resetJump();
			oPlayer->stopMove();

			oEvent->iSpeed = 2;
			oEvent->newLevelType = 2;

			oEvent->iDelay = 150;
			oEvent->newCurrentLevel = 25;

			oEvent->newMapXPos = 0;
			oEvent->newPlayerXPos = 96;
			oEvent->newPlayerYPos = 64;
			oEvent->newMoveMap = true;
			oEvent->newUnderWater = true;

			oEvent->vOLDDir.push_back(oEvent->eRIGHT);
			oEvent->vOLDLength.push_back(7 * 32 + 4);

			oEvent->vOLDDir.push_back(oEvent->ePLAYPIPERIGHT);
			oEvent->vOLDLength.push_back(1);

			oEvent->vOLDDir.push_back(oEvent->eRIGHT);
			oEvent->vOLDLength.push_back(1 * 32 - 2);

			oEvent->vOLDDir.push_back(oEvent->eNOTHING);
			oEvent->vOLDLength.push_back(75);

			oEvent->reDrawX.push_back(230);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(230);
			oEvent->reDrawY.push_back(3);
			break;
		case 26:
			oEvent->resetData();
			oPlayer->resetJump();
			oPlayer->stopMove();

			oEvent->iSpeed = 2;
			oEvent->newLevelType = 2;

			oEvent->iDelay = 150;
			oEvent->newCurrentLevel = 26;

			oEvent->newMapXPos = 0;
			oEvent->newPlayerXPos = 96;
			oEvent->newPlayerYPos = 64;
			oEvent->newMoveMap = true;
			oEvent->newUnderWater = true;

			oEvent->vOLDDir.push_back(oEvent->eRIGHT);
			oEvent->vOLDLength.push_back(7 * 32 + 4);

			oEvent->vOLDDir.push_back(oEvent->ePLAYPIPERIGHT);
			oEvent->vOLDLength.push_back(1);

			oEvent->vOLDDir.push_back(oEvent->eRIGHT);
			oEvent->vOLDLength.push_back(1 * 32 - 2);

			oEvent->vOLDDir.push_back(oEvent->eNOTHING);
			oEvent->vOLDLength.push_back(75);

			oEvent->reDrawX.push_back(230);
			oEvent->reDrawY.push_back(2);
			oEvent->reDrawX.push_back(230);
			oEvent->reDrawY.push_back(3);
			break;
	}
}

void Map::spawnVine(int nX, int nY, int iBlockID) {
	if(iLevelType == 0 || iLevelType == 4) {
		addVine(nX, nY, 0, 34);
	} else {
		addVine(nX, nY, 0, 36);
	}
}

void Map::TPUse() {
	switch(currentLevelID) {
		case 27:
			if(bTP) {
				if(!(-fXPos > 90*32)) {
					fXPos -= 64*32;
					bTP = false;
				}
			} else {
				if(!(-fXPos < 90*32)) {
					fXPos += 64*32;
				}
			}
			break;
		case 31:
			fXPos += 75*32;
			break;
	}
}

void Map::TPUse2() {
	switch(currentLevelID) {
		case 27:
			fXPos += 64*32;
			bTP = false;
			break;
		case 31:
			fXPos += 12*32;
			break;
	}
}

void Map::TPUse3() {
	switch(currentLevelID) {
		case 27:
			bTP = true;
			break;
		case 31:
			fXPos += 79*32;
			break;
	}
}


/* ******************************************** */

void Map::structBush(int X, int Y, int iSize) {
	// ----- LEFT & RIGHT
	for(int i = 0; i < iSize; i++) {
		lMap[X + i][Y + i]->setBlockID(5);
		lMap[X + iSize + 1 + i][Y + iSize - 1 - i]->setBlockID(6);
	}
	
	// ----- CENTER LEFT & RIGHT
	for(int i = 0, k = 1; i < iSize - 1; i++) {
		for(int j = 0; j < k; j++) {
			lMap[X + 1 + i][Y + j]->setBlockID((i%2 == 0 ? 3 : 4));
			lMap[X + iSize * 2 - 1 - i][Y + j]->setBlockID((i%2 == 0 ? 3 : 4));
		}
		++k;
	}

	// ----- CENTER
	for(int i = 0; i < iSize; i++) {
		lMap[X + iSize][Y + i]->setBlockID((i%2 == 0 && iSize != 1 ? 4 : 3));
	}

	// ----- TOP
	lMap[X + iSize][Y + iSize]->setBlockID(7);
}

void Map::structGrass(int X, int Y, int iSize) {
	lMap[X][Y]->setBlockID(10);
	for(int i = 0; i < iSize; i++) {
		lMap[X + 1 + i][Y]->setBlockID(11);
	}
	lMap[X + iSize + 1][Y]->setBlockID(12);
}

void Map::structCloud(int X, int Y, int iSize) {
	// ----- LEFT
	lMap[X][Y]->setBlockID(iLevelType == 3 ? 148 : 14);
	lMap[X][Y + 1]->setBlockID(15);
	
	for(int i = 0; i < iSize; i++) {
		lMap[X + 1 + i][Y]->setBlockID(iLevelType == 3 ? 149 : 16);
		lMap[X + 1 + i][Y + 1]->setBlockID(iLevelType == 3 ? 150 : 17);
	}

	lMap[X + iSize + 1][Y]->setBlockID(18);
	lMap[X + iSize + 1][Y + 1]->setBlockID(19);
}

MapLevel* Map::structGND(int X, int Y, int iWidth, int iHeight) {
	#ifdef DEBUG
	std::cout << "Map::structGND = " << X << ", " << Y << std::endl;
	#endif

	MapLevel *ptr = nullptr;
	int hits = 0;
	for(int i = 0; i < iWidth; i++) {
		for(int j = 0; j < iHeight; j++) {
			if (X+i >= lMap.size() || Y+j >= lMap[X+i].size() ) continue;
			lMap[X + i][Y + j]->setBlockID(iLevelType == 0 || iLevelType == 4 ? 1 : iLevelType == 1 ? 26 : iLevelType == 2 ? 92 : iLevelType == 6 ? 166 : iLevelType == 7 ? 181 : 75);
			hits ++;
			if (!ptr) ptr = lMap[X + i][Y + j];
		}
	}
	#ifdef DEBUG
	std::cout << "Map::structGND made tiles:" << hits << std::endl;
	#endif

	return ptr;
}

void Map::structBonus(int X, int Y, int iWidth) {
	for(int i = 0; i < iWidth; i++) {
		lMap[X + i][Y]->setBlockID(125);
	}
}

void Map::structBonusEnd(int X) {
	for(int i = 0; i < 20; i++) {
		lMap[X + i][0]->setBlockID(127);
	}
}

void Map::structUW1(int X, int Y, int iWidth, int iHeight) {
	for(int i = 0; i < iWidth; i++) {
		for(int j = 0; j < iHeight; j++) {
			lMap[X + i][Y + j]->setBlockID(93);
		}
	}
}

// ----- true = LEFT, false = RIGHT -----
void Map::structGND2(int X, int Y, int iSize, bool bDir) {
	if(bDir) {
		for(int i = 0, k = 1; i < iSize; i++) {
			for(int j = 0; j < k; j++) {
				lMap[X + i][Y + j]->setBlockID(iLevelType == 0 || iLevelType == 4 ? 25 : iLevelType == 3 ? 167 : 27);
			}
			++k;
		}
	} else {
		for(int i = 0, k = 1; i < iSize; i++) {
			for(int j = 0; j < k; j++) {
				lMap[X + iSize - 1 - i][Y + j]->setBlockID(iLevelType == 0 || iLevelType == 4 ? 25 : iLevelType == 3 ? 167 : 27);
			}
			++k;
		}
	}
}

void Map::structGND2(int X, int Y, int iWidth, int iHeight) {
	for(int i = 0; i < iWidth; i++) {
		for(int j = 0; j < iHeight; j++) {
			lMap[X + i][Y + j]->setBlockID(iLevelType == 0 || iLevelType == 4 ? 25 : iLevelType == 3 ? 167 : 27);
		}
	}
}

void Map::structPipe(int X, int Y, int iHeight) {
	for(int i = 0; i < iHeight; i++) {
		lMap[X][Y + i]->setBlockID(iLevelType == 0 ? 20 : iLevelType == 2 ? 97 : iLevelType == 4 ? 112 : iLevelType == 5 ? 136 : iLevelType == 3 ? 176 : iLevelType == 7 ? 172 : 30);
		lMap[X + 1][Y + i]->setBlockID(iLevelType == 0 ? 22 : iLevelType == 2 ? 99 : iLevelType == 4 ? 114 : iLevelType == 5 ? 138 : iLevelType == 3 ? 178 : iLevelType == 7 ? 174 : 32);
	}

	lMap[X][Y + iHeight]->setBlockID(iLevelType == 0 ? 21 : iLevelType == 2 ? 98 : iLevelType == 4 ? 113 : iLevelType == 5 ? 137 : iLevelType == 3 ? 177 : iLevelType == 7 ? 173 : 31);
	lMap[X + 1][Y + iHeight]->setBlockID(iLevelType == 0 ? 23 : iLevelType == 2 ? 100 : iLevelType == 4 ? 115 : iLevelType == 5 ? 139 : iLevelType == 3 ? 179 : iLevelType == 7 ? 175 : 33);
}

void Map::structPipeVertical(int X, int Y, int iHeight) {
	for(int i = 0; i < iHeight + 1; i++) {
		lMap[X][Y + i]->setBlockID(iLevelType == 0 ? 20 : iLevelType == 2 ? 97 : iLevelType == 4 ? 112 : 30);
		lMap[X + 1][Y + i]->setBlockID(iLevelType == 0 ? 22 : iLevelType == 2 ? 99 : iLevelType == 4 ? 114 : 32);
	}
}

void Map::structPipeHorizontal(int X, int Y, int iWidth) {
	lMap[X][Y]->setBlockID(iLevelType == 0 ? 62 : iLevelType == 2 ? 105 : iLevelType == 4 ? 120 : 38);
	lMap[X][Y + 1]->setBlockID(iLevelType == 0 ? 60 : iLevelType == 2 ? 103 : iLevelType == 4 ? 118 : 36);

	for(int i = 0 ; i < iWidth; i++) {
		lMap[X + 1 + i][Y]->setBlockID(iLevelType == 0 ? 61 : iLevelType == 2 ? 104 : iLevelType == 4 ? 119 : 37);
		lMap[X + 1 + i][Y + 1]->setBlockID(iLevelType == 0 ? 59 : iLevelType == 2 ? 102 : iLevelType == 4 ? 117 : 35);
	}
	
	lMap[X + 1 + iWidth][Y]->setBlockID(iLevelType == 0 ? 58 : iLevelType == 2 ? 101 : iLevelType == 4 ? 116 : 34);
	lMap[X + 1 + iWidth][Y + 1]->setBlockID(iLevelType == 0 ? 63 : iLevelType == 2 ? 106 : iLevelType == 4 ? 121 : 39);
}

MapLevel* Map::structBrick(int X, int Y, int iWidth, int iHeight) {
	MapLevel *ptr = nullptr;
	for(int i = 0; i < iWidth; i++) {
		for(int j = 0; j < iHeight; j++) {
			if (X+i >= lMap.size() || Y+j >= lMap[X+i].size() ) continue;
			lMap[X + i][Y + j]->setBlockID(iLevelType == 0 || iLevelType == 4 ? 13 : iLevelType == 3 ? 81 : 28);
			if (!ptr) ptr = lMap[X + i][Y + j];
		}
	}
	return ptr;
}

void Map::structBlockQ(int X, int Y, int iWidth) {
	for(int i = 0; i < iWidth; i++) {
		lMap[X + i][Y]->setBlockID(iLevelType == 0 || iLevelType == 4 ? 8 : 55);
	}
}

void Map::structBlockQ2(int X, int Y, int iWidth) {
	for(int i = 0; i < iWidth; i++) {
		lMap[X + i][Y]->setBlockID(24);
	}
}

void Map::structCoins(int X, int Y, int iWidth, int iHeight) {
	for(int i = 0; i < iWidth; i++) {
		for(int j = 0; j < iHeight; j++) {
			lMap[X + i][Y + j]->setBlockID(iLevelType == 0 || iLevelType == 4 ? 71 : iLevelType == 1 ? 29 : iLevelType == 2 ? 73 : 29);
		}
	}
}

void Map::structEnd(int X, int Y, int iHeight) {
	for(int i = 0; i < iHeight; i++) {
		lMap[X][Y + i]->setBlockID(iLevelType == 4 ? 123 : 40);
	}

	oFlag = new Flag(X*32 - 16, Y + iHeight + 72);

	lMap[X][Y + iHeight]->setBlockID(iLevelType == 4 ? 124 : 41);

	for(int i = Y + iHeight + 1; i < Y + iHeight + 4; i++) {
		lMap[X][i]->setBlockID(182);
	}
}

void Map::structCastleSmall(int X, int Y) {
	for(int i = 0; i < 2; i++){
		lMap[X][Y + i]->setBlockID(iLevelType == 3 ? 155 : 43);
		lMap[X + 1][Y + i]->setBlockID(iLevelType == 3 ? 155 : 43);
		lMap[X + 3][Y + i]->setBlockID(iLevelType == 3 ? 155 : 43);
		lMap[X + 4][Y + i]->setBlockID(iLevelType == 3 ? 155 : 43);

		lMap[X + 2][Y + i]->setBlockID(iLevelType == 3 ? 159 : 47);
	}

	lMap[X + 2][Y + 1]->setBlockID(iLevelType == 3 ? 158 : 46);

	for(int i = 0; i < 5; i++) {
		lMap[X + i][Y + 2]->setBlockID(i == 0 || i == 4 ? iLevelType == 3 ? 157 : 45 : iLevelType == 3 ? 156 : 44);
	}

	lMap[X + 1][Y + 3]->setBlockID(iLevelType == 3 ? 160 : 48);
	lMap[X + 2][Y + 3]->setBlockID(iLevelType == 3 ? 155 : 43);
	lMap[X + 3][Y + 3]->setBlockID(iLevelType == 3 ? 161 : 49);

	for(int i = 0; i < 3; i++) {
		lMap[X + i + 1][Y + 4]->setBlockID(iLevelType == 3 ? 157 : 45);
	}
}

void Map::structCastleBig(int X, int Y) {
	for(int i = 0; i < 2; i++) {
		for(int j = 0; j < 5; j++) {
			setBlockID(X + i, Y + j, iLevelType == 3 ? 155 : 43);
			setBlockID(X + i + 7, Y + j, iLevelType == 3 ? 155 : 43);
		}
	}

	for(int i = 0; i < 3; i++) {
		setBlockID(X + 2 + i*2, Y, iLevelType == 3 ? 159 : 47);
		setBlockID(X + 2 + i*2, Y + 1, iLevelType == 3 ? 158 : 46);
	}

	for(int i = 0; i < 9; i++) {
		setBlockID(X + i, Y + 2, iLevelType == 3 ? 155 : 43);
	}

	for(int i = 0; i < 9; i++) {
		if(i < 2 || i > 6) {
			setBlockID(X + i, Y + 5, iLevelType == 3 ? 157 : 45);
		} else {
			setBlockID(X + i, Y + 5, iLevelType == 3 ? 156 : 44);
		}
	}

	
	for(int i = 0; i < 2; i++) {
		setBlockID(X + 3 + i*2, Y, iLevelType == 3 ? 155 : 43);
		setBlockID(X + 3 + i*2, Y + 1, iLevelType == 3 ? 155 : 43);
	}
	
	for(int i = 0; i < 2; i++) {
		setBlockID(X + 3 + i*2, Y + 3, iLevelType == 3 ? 159 : 47);
		setBlockID(X + 3 + i*2, Y + 4, iLevelType == 3 ? 158 : 46);
	}

	for(int i = 0; i < 3; i++) {
		setBlockID(X + 2 + i*2, Y + 3, iLevelType == 3 ? 155 : 43);
		setBlockID(X + 2 + i*2, Y + 4, iLevelType == 3 ? 155 : 43);
	}

	for(int i = 0; i < 2; i++) {
		setBlockID(X + 2, Y + 6 + i, iLevelType == 3 ? 155 : 43);
		setBlockID(X + 3, Y + 6 + i, iLevelType == 3 ? 155 : 43);
		setBlockID(X + 5, Y + 6 + i, iLevelType == 3 ? 155 : 43);
		setBlockID(X + 6, Y + 6 + i, iLevelType == 3 ? 155 : 43);
	}

	setBlockID(X + 4, Y + 6, iLevelType == 3 ? 159 : 47);
	setBlockID(X + 4, Y + 7, iLevelType == 3 ? 158 : 46);

	for(int i = 0; i < 3; i++) {
		setBlockID(X + 3 + i, Y + 8, iLevelType == 3 ? 156 : 44);
	}

	setBlockID(X + 2, Y + 8, iLevelType == 3 ? 157 : 45);
	setBlockID(X + 6, Y + 8, iLevelType == 3 ? 157 : 45);

	setBlockID(X + 2, Y + 8, iLevelType == 3 ? 157 : 45);

	setBlockID(X + 3, Y + 9, iLevelType == 3 ? 160 : 48);
	setBlockID(X + 4, Y + 9, iLevelType == 3 ? 155 : 43);
	setBlockID(X + 5, Y + 9, iLevelType == 3 ? 161 : 49);

	for(int i = 0; i < 3; i++) {
		setBlockID(X + 3 + i, Y + 10, iLevelType == 3 ? 157 : 45);
	}
}

void Map::structCastleWall(int X, int Y, int iWidth, int iHeight) {
	for(int i = 0; i < iWidth; i++) {
		for(int j = 0; j  < iHeight - 1; j++) {
			lMap[X + i][Y + j]->setBlockID(iLevelType == 3 ? 155 : 43);
		}
	}

	for(int i = 0; i < iWidth; i++) {
		lMap[X + i][Y + iHeight - 1]->setBlockID(iLevelType == 3 ? 157 : 45);
	}
}

void Map::structT(int X, int Y, int iWidth, int iHeight) {
	for(int i = 0; i < iHeight - 1; i++) {
		for(int j = 1; j < iWidth - 1; j++) {
			lMap[X + j][Y + i]->setBlockID(iLevelType == 3 ? 154 : 70);
		}
	}

	for(int i = 1; i < iWidth - 1; i++) {
		lMap[X + i][Y + iHeight - 1]->setBlockID(iLevelType == 3 ? 152 : 68);
	}

	lMap[X][Y + iHeight - 1]->setBlockID(iLevelType == 3 ? 151 : 67);
	lMap[X + iWidth - 1][Y + iHeight - 1]->setBlockID(iLevelType == 3 ? 153 : 69);
}

void Map::structTMush(int X, int Y, int iWidth, int iHeight) {
	for(int i = 0; i < iHeight - 2; i++) {
		lMap[X + iWidth/2][Y + i]->setBlockID(144);
	}

	lMap[X + iWidth/2][Y + iHeight - 2]->setBlockID(143);

	for(int i = 1; i < iWidth - 1; i++) {
		lMap[X + i][Y + iHeight - 1]->setBlockID(141);
	}

	lMap[X][Y + iHeight - 1]->setBlockID(140);
	lMap[X + iWidth - 1][Y + iHeight - 1]->setBlockID(142);
}

void Map::structWater(int X, int Y, int iWidth, int iHeight) {
	for(int i = 0; i < iWidth; i++) {
		for(int j = 0; j < iHeight - 1; j++) {
			lMap[X + i][Y + j]->setBlockID(iLevelType == 2 ? 94 : 110);
		}
		lMap[X + i][Y + iHeight - 1]->setBlockID(iLevelType == 2 ? 95 : 111);
	}
}

void Map::structLava(int X, int Y, int iWidth, int iHeight) {
	for(int i = 0; i < iWidth; i++) {
		for(int j = 0; j < iHeight - 1; j++) {
			lMap[X + i][Y + j]->setBlockID(77);
		}
		lMap[X + i][Y + iHeight - 1]->setBlockID(78);
	}
}

void Map::structBridge(int X, int Y, int iWidth) {
	for(int i = 0; i < iWidth; i++) {
		lMap[X + i][Y]->setBlockID(76);
	}

	lMap[X + iWidth - 1][Y + 1]->setBlockID(79);

	lMap[X + iWidth][6]->setBlockID(82);
	lMap[X + iWidth + 1][6]->setBlockID(83);
	lMap[X + iWidth + 1][7]->setBlockID(83);
	lMap[X + iWidth + 1][8]->setBlockID(83);
}

void Map::structBridge2(int X, int Y, int iWidth) {
	for(int i = 0; i < iWidth; i++) {
		lMap[X + i][Y]->setBlockID(107);
		lMap[X + i][Y + 1]->setBlockID(iLevelType == 4 ? 122 : 108);
	}
}

void Map::structTree(int X, int Y, int iHeight, bool BIG) {
	for(int i = 0; i < iHeight; i++) {
		lMap[X][Y + i]->setBlockID(91);
	}

	if(BIG) {
		lMap[X][Y + iHeight]->setBlockID(iLevelType == 4 ? 88 : 85);
		lMap[X][Y + iHeight + 1]->setBlockID(iLevelType == 4 ? 89 : 86);
	} else {
		lMap[X][Y + iHeight]->setBlockID(iLevelType == 4 ? 87 : 84);
	}
}

void Map::structFence(int X, int Y, int iWidth) {
	for(int i = 0; i < iWidth; i++) {
		lMap[X + i][Y]->setBlockID(90);
	}
}

void Map::structPlatformLine(int X) {
	for(int i = 0; i < iMapHeight; i++) {
		lMap[X][i]->setBlockID(109);
	}
}

void Map::structSeeSaw(int X, int Y, int iWidth) {
	lMap[X][Y]->setBlockID(iLevelType == 3 ? 162 : 132);
	lMap[X + iWidth - 1][Y]->setBlockID(iLevelType == 3 ? 163 : 133);

	for(int i = 1; i < iWidth - 1; i++) {
		lMap[X + i][Y]->setBlockID(iLevelType == 3 ? 164 : 134);
	}
}

void Map::structBulletBill(int X, int Y, int iHieght) {
	lMap[X][Y + iHieght + 1]->setBlockID(145);
	lMap[X][Y + iHieght]->setBlockID(146);

	for(int i = 0; i < iHieght; i++) {
		lMap[X][Y + i]->setBlockID(147);
	}

	addBulletBillSpawner(X, Y + iHieght + 1, 0);
}

/* ******************************************** */

void Map::setBlockID(int X, int Y, int iBlockID) {
	if(X >= 0 && X < iMapWidth) {
		lMap[X][Y]->setBlockID(iBlockID);
	}
}

/* ******************************************** */

Player* Map::getPlayer() {
	return oPlayer;
}

Platform* Map::getPlatform(int iID) {
	return vPlatform[iID];
}

float Map::getXPos() {
	return fXPos;
}

void Map::setXPos(float iXPos) {
	this->fXPos = iXPos;
}

float Map::getYPos() {
	return fYPos;
}

void Map::setYPos(float iYPos) {
	this->fYPos = iYPos;
}

int Map::getLevelType() {
	return iLevelType;
}

void Map::setLevelType(int iLevelType) {
	this->iLevelType = iLevelType;
}

int Map::getCurrentLevelID() {
	return currentLevelID;
}

void Map::setCurrentLevelID(int currentLevelID) {
	if(this->currentLevelID != currentLevelID) {
		this->currentLevelID = currentLevelID;
		oEvent->resetRedraw();
		loadLVL();
		iSpawnPointID = 0;
	}

	this->currentLevelID = currentLevelID;
}

bool Map::getUnderWater() {
	return underWater;
}

void Map::setUnderWater(bool underWater) {
	this->underWater = underWater;
}

void Map::setSpawnPointID(int iSpawnPointID) {
	this->iSpawnPointID = iSpawnPointID;
}

int Map::getMapTime() {
	return iMapTime;
}

void Map::setMapTime(int iMapTime) {
	this->iMapTime = iMapTime;
}

int Map::getMapWidth() {
	return iMapWidth;
}

bool Map::getMoveMap() {
	return bMoveMap;
}

void Map::setMoveMap(bool bMoveMap) {
	this->bMoveMap = bMoveMap;
}

bool Map::getDrawLines() {
	return drawLines;
}

void Map::setDrawLines(bool drawLines) {
	this->drawLines = drawLines;
}

/* ******************************************** */

Event* Map::getEvent() {
	return oEvent;
}

bool Map::getInEvent() {
	return inEvent;
}

void Map::setInEvent(bool inEvent) {
	this->inEvent = inEvent;
}

/* ******************************************** */

Block* Map::getBlock(int iID) {
	return vBlock[iID];
}

Block* Map::getMinionBlock(int iID) {
	return vMinion[iID];
}

MapLevel* Map::getMapBlock(int iX, int iY) {
	return lMap[iX][iY];
}

Flag* Map::getFlag() {
	return oFlag;
}