#pragma once

#ifndef CORE_H
#define CORE_H

#include "Map.h"
#include <thread>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>

extern "C" {
	void uninext_enter();
	void uninext_gamepad( float x1, float y1, float x2, float y2);
	void uninext_move_x( float f );
	void uninext_move_y( float f );
	void uninext_jump();
	void uninext_reset_jump();
	void uninext_squat();
	void uninext_fireball();
	void uninext_run_command(char*);
}

class CCore
{
private:
	SDL_Window* window;
	SDL_Renderer* rR;
	SDL_Event* mainEvent;

	// ----- FPS -----

	long frameTime;
	static const int MIN_FRAME_TIME = 16;

	unsigned long lFPSTime;
	int iNumOfFPS, iFPS;

	// ----- FPS -----

	// ----- INPUT
	static bool movePressed, keyMenuPressed, keyS, keyW, keyA, keyD, keyShift;

	static bool keyAPressed, keyDPressed;
	// ----- true = RIGHT, false = LEFT
	bool firstDir;

	// ----- INPUT

	static Map* oMap;

	// ----- Methods

public:
	CCore(void);
	~CCore(void);

	static bool quitGame;

	void Input();
	void MouseInput();
	void InputPlayer();
	void InputMenu();

	void clear();
	void input();
	void show();
	void mainLoop();

	void Update();
	void Draw();

	void resetMove();
	static void resetKeys();

	static bool mouseLeftPressed, mouseRightPressed;
	static int mouseX, mouseY;
	
	/* ----- get & set ----- */
	static Map* getMap();


	// uninext engine //
	SDL_Joystick *joystick1;
	int joystick1_axes;
	void parse_input_json(std::string json);

	std::thread *input_thread;
	void start_input_thread( void );

};

#endif
