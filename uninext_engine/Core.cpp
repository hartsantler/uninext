#include "header.h"
#include "Core.h"
#include "IMG.h"
#include "CFG.h"
#include "Text.h"


static CCore *UninextEngine;

extern "C" {
	void EMSCRIPTEN_KEEPALIVE uninext_enter(){
		CCFG::getMM()->enter();
	}
	void EMSCRIPTEN_KEEPALIVE uninext_gamepad( float x1, float y1, float x2, float y2) {
		UninextEngine->getMap()->getPlayer()->update_joystick( x1, y1, x2, y2 );
	}
	void EMSCRIPTEN_KEEPALIVE uninext_move_x( float f ){
		UninextEngine->getMap()->getPlayer()->move_x(f);
	}
	void EMSCRIPTEN_KEEPALIVE uninext_move_y( float f ){
		UninextEngine->getMap()->getPlayer()->move_y(f);
	}
	void EMSCRIPTEN_KEEPALIVE uninext_jump(){
		UninextEngine->getMap()->getPlayer()->jump();
	}
	void EMSCRIPTEN_KEEPALIVE uninext_reset_jump(){
		UninextEngine->getMap()->getPlayer()->resetJump();
	}
	void EMSCRIPTEN_KEEPALIVE uninext_squat(){
		UninextEngine->getMap()->getPlayer()->setSquat(true);
	}
	void EMSCRIPTEN_KEEPALIVE uninext_fireball(){
		UninextEngine->getMap()->getPlayer()->createFireBall();
	}
	void EMSCRIPTEN_KEEPALIVE uninext_run_command(char *ptr){
		UninextEngine->parse_input_json(std::string(ptr));
	}

}


/* ******************************************** */

Map* CCore::oMap = new Map();
bool CCore::mouseLeftPressed = false;
bool CCore::mouseRightPressed = false;
int CCore::mouseX = 0;
int CCore::mouseY = 0;
bool CCore::quitGame = false;

bool CCore::movePressed = false;
bool CCore::keyMenuPressed = false;
bool CCore::keyS = false;
bool CCore::keyW = false;
bool CCore::keyA = false;
bool CCore::keyD = false;
bool CCore::keyShift = false;
bool CCore::keyAPressed = false;
bool CCore::keyDPressed = false;

void CCore::parse_input_json( std::string json ) {
	if (json.size()==0) return;

	std::cout << "stdin: " << json.size() << std::endl;
	//if (json[0]=='/') {
	//	std::cout << "update player sprite: " << json << std::endl;
	//	oMap->getPlayer()->reload_custom_sprite(1);
	//}

	oMap->run_command( json );

}

void CCore::start_input_thread( void ) {
	// thread to read from stdin
	this->input_thread = new std::thread{
		[&]{
			std::string tmp;
			std::cout << "starting read stdin..." << std::endl;
			while (!this->quitGame) {
				// NO WAY // https://stackoverflow.com/questions/21359942/read-function-in-c-similar-to-c-read
				//int header;
				//std::stdin >> header;
				std::getline(std::cin, tmp);
				std::cout << "stdin=" << tmp << std::endl;
				this->parse_input_json( tmp );
			}
		}
	};
}


CCore::CCore(void) {
	//#ifdef DEBUG
	std::cout << "Uninext make CCore" << std::endl;
	//#endif

	this->quitGame = false;
	this->iFPS = 0;
	this->iNumOfFPS = 0;
	this->lFPSTime = 0;

	#ifdef EMSCRIPTEN
		SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_AUDIO );
	#else
		SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_AUDIO | SDL_INIT_JOYSTICK );
	#endif

	window = SDL_CreateWindow("uninext engine", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, CCFG::GAME_WIDTH, CCFG::GAME_HEIGHT, SDL_WINDOW_SHOWN);

	if(window == NULL) {
		quitGame = true;
	}

	#ifndef EMSCRIPTEN
		this->joystick1 = SDL_JoystickOpen(0);
		if (this->joystick1) {
			std::cout << "Primary Name: " <<  SDL_JoystickNameForIndex(0) << std::endl;  // SDL2
			std::cout << "Number of Axes: " << SDL_JoystickNumAxes(joystick1) << std::endl;
			std::cout << "Number of Buttons: " << SDL_JoystickNumButtons(joystick1) << std::endl;
			std::cout << "Number of Balls: " << SDL_JoystickNumBalls(joystick1) << std::endl;
			std::cout << "Number of Hats: " << SDL_JoystickNumHats(joystick1) << std::endl;
			this->joystick1_axes = SDL_JoystickNumAxes(joystick1);
		} else {
			std::cout << "Uninext CCore no gamepad" << std::endl;		
		}
	#endif

	#ifdef DEBUG
		std::cout << "Uninext make CCore renderer" << std::endl;
	#endif


	rR = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

	#ifdef DEBUG
		std::cout << "Uninext make CCore IMG_Init" << std::endl;
	#endif

	#ifdef EMSCRIPTEN
		//IMG_Init(IMG_INIT_GIF);
		// note: no init is required for GIF format
	#else
		IMG_Init(IMG_INIT_PNG);

		#ifdef DEBUG
			std::cout << "Uninext make CCore ico" << std::endl;
		#endif

		// ----- ICO
		std::string fileName = "files/images/ico.bmp";
		SDL_Surface* loadedSurface = SDL_LoadBMP(fileName.c_str());
		SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 255, 0, 255));

		#ifdef DEBUG
			std::cout << "Uninext make CCore set window icon" << std::endl;
		#endif


		SDL_SetWindowIcon(window, loadedSurface);
		SDL_FreeSurface(loadedSurface);

	#endif


	mainEvent = new SDL_Event();
	// ----- ICO
	
	#ifdef DEBUG
		std::cout << "Uninext open mixer" << std::endl;
	#endif
	Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048);
	
	oMap = new Map(rR);
	CCFG::getMM()->setActiveOption(rR);
	CCFG::getSMBLOGO()->setIMG("super_mario_bros", rR);

	CCFG::getMusic()->PlayMusic();

	this->keyMenuPressed = this->movePressed = this->keyS = this->keyW = this->keyA = this->keyD = this->keyShift = false;

	this->keyAPressed = this->keyDPressed = this->firstDir = false;

	this->mouseX = this->mouseY = 0;

	CCFG::keyIDA = SDLK_a;
	CCFG::keyIDS = SDLK_s;
	CCFG::keyIDD = SDLK_d;
	CCFG::keyIDSpace = SDLK_SPACE;
	CCFG::keyIDShift = SDLK_LSHIFT;

	#ifdef DEBUG
		std::cout << "Uninext make CCore starting std input thread" << std::endl;
	#endif

	// uninext //
	#ifndef EMSCRIPTEN
		this->start_input_thread();
	#endif

	//#ifdef DEBUG
	std::cout << "Uninext make CCore OK" << std::endl;
	//#endif
	UninextEngine = this;
}

CCore::~CCore(void) {
	delete oMap;
	delete mainEvent;
	IMG_Quit();
	SDL_DestroyRenderer(rR);
	SDL_DestroyWindow(window);
}

/* ******************************************** */
void CCore::clear() {
	SDL_RenderClear(rR);
	CCFG::getMM()->setBackgroundColor(rR);
	SDL_RenderFillRect(rR, NULL);
}
void CCore::input() {
	#ifndef EMSCRIPTEN
		Input();
		MouseInput();
	#endif
}
void CCore::show() {
	SDL_RenderPresent(rR);
}


void CCore::mainLoop() {
	#ifdef DEBUG
		std::cout << "Uninext Enter MainLoop" << std::endl;
	#endif
	lFPSTime = SDL_GetTicks();

	while(!quitGame && mainEvent->type != SDL_QUIT) {
		frameTime = SDL_GetTicks();
		//SDL_PollEvent(mainEvent);
		SDL_RenderClear(rR);

		CCFG::getMM()->setBackgroundColor(rR);
		SDL_RenderFillRect(rR, NULL);

		Input();
		MouseInput();
		Update();
		Draw();

		/*CCFG::getText()->Draw(rR, "FPS:" + std::to_string(iNumOfFPS), CCFG::GAME_WIDTH - CCFG::getText()->getTextWidth("FPS:" + std::to_string(iNumOfFPS), 8) - 8, 5, 8);

		if(SDL_GetTicks() - 1000 >= lFPSTime) {
			lFPSTime = SDL_GetTicks();
			iNumOfFPS = iFPS;
			iFPS = 0;
		}

		++iFPS;*/

		SDL_RenderPresent(rR);
		
		if(SDL_GetTicks() - frameTime < MIN_FRAME_TIME) {
			SDL_Delay(MIN_FRAME_TIME - (SDL_GetTicks () - frameTime));
		}
	}
}

void CCore::Input() {
	switch(CCFG::getMM()->getViewID()) {
		case 2: case 7:
			if(!oMap->getInEvent()) {
				InputPlayer();
			} else {
				resetMove();
			}
			break;
		default:
			InputMenu();
			break;
	}
}

void CCore::InputMenu() {
	if(mainEvent->type == SDL_KEYDOWN) {
		CCFG::getMM()->setKey(mainEvent->key.keysym.sym);

		switch(mainEvent->key.keysym.sym) {
			case SDLK_s: case SDLK_DOWN:
				if(!keyMenuPressed) {
					CCFG::getMM()->keyPressed(2);
					keyMenuPressed = true;
				}
				break;
			case SDLK_w: case SDLK_UP:
				if(!keyMenuPressed) {
					CCFG::getMM()->keyPressed(0);
					keyMenuPressed = true;
				}
				break;
			case SDLK_KP_ENTER: case SDLK_RETURN:
				if(!keyMenuPressed) {
					CCFG::getMM()->enter();
					keyMenuPressed = true;
				}
				break;
			case SDLK_ESCAPE:
				if(!keyMenuPressed) {
					CCFG::getMM()->escape();
					keyMenuPressed = true;
				}
				break;
			case SDLK_LEFT: case SDLK_d:
				if(!keyMenuPressed) {
					CCFG::getMM()->keyPressed(3);
					keyMenuPressed = true;
				}
				break;
			case SDLK_RIGHT: case SDLK_a:
				if(!keyMenuPressed) {
					CCFG::getMM()->keyPressed(1);
					keyMenuPressed = true;
				}
				break;
		}
	}

	if(mainEvent->type == SDL_KEYUP) {
		switch(mainEvent->key.keysym.sym) {
			case SDLK_s: case SDLK_DOWN: case SDLK_w: case SDLK_UP: case SDLK_KP_ENTER: case SDLK_RETURN: case SDLK_ESCAPE: case SDLK_a: case SDLK_RIGHT: case SDLK_LEFT: case SDLK_d:
				keyMenuPressed = false;
				break;
			default:
				break;
		}
	}
}

void CCore::InputPlayer() {
	if(mainEvent->type == SDL_WINDOWEVENT) {
		switch(mainEvent->window.event) {
			case SDL_WINDOWEVENT_FOCUS_LOST:
				CCFG::getMM()->resetActiveOptionID(CCFG::getMM()->ePasue);
				CCFG::getMM()->setViewID(CCFG::getMM()->ePasue);
				CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cPASUE);
				CCFG::getMusic()->PauseMusic();
				break;
		}
	}

	if(mainEvent->type == SDL_KEYUP) {
		if(mainEvent->key.keysym.sym == CCFG::keyIDD) {
				if(firstDir) {
					firstDir = false;
				}

				keyDPressed = false;
			}

			if(mainEvent->key.keysym.sym == CCFG::keyIDS) {
				oMap->getPlayer()->setSquat(false);
				keyS = false;
			}
		
			if(mainEvent->key.keysym.sym == CCFG::keyIDA) {
				if(!firstDir) {
					firstDir = true;
				}

				keyAPressed = false;
			}
		
			if(mainEvent->key.keysym.sym == CCFG::keyIDSpace) {
				CCFG::keySpace = false;
			}
		
			if(mainEvent->key.keysym.sym == CCFG::keyIDShift) {
				if(keyShift) {
					oMap->getPlayer()->resetRun();
					keyShift = false;
				}
			}
		switch(mainEvent->key.keysym.sym) {
			case SDLK_KP_ENTER: case SDLK_RETURN: case SDLK_ESCAPE:
				keyMenuPressed = false;
				break;
		}
	}

	if(mainEvent->type == SDL_KEYDOWN) {
		if(mainEvent->key.keysym.sym == CCFG::keyIDD) {
			keyDPressed = true;
			if(!keyAPressed) {
				firstDir = true;
			}
		}

		if(mainEvent->key.keysym.sym == CCFG::keyIDS) {
			if(!keyS) {
				keyS = true;
				if(!oMap->getUnderWater() && !oMap->getPlayer()->getInLevelAnimation()) oMap->getPlayer()->setSquat(true);
			}
		}
		
		if(mainEvent->key.keysym.sym == CCFG::keyIDA) {
			keyAPressed = true;
			if(!keyDPressed) {
				firstDir = false;
			}
		}
		
		if(mainEvent->key.keysym.sym == CCFG::keyIDSpace) {
			if(!CCFG::keySpace) {
				oMap->getPlayer()->jump();
				CCFG::keySpace = true;
			}
		}
		
		if(mainEvent->key.keysym.sym == CCFG::keyIDShift) {
			if(!keyShift) {
				oMap->getPlayer()->startRun();
				keyShift = true;
			}
		}

		switch(mainEvent->key.keysym.sym) {
			case SDLK_KP_ENTER: case SDLK_RETURN:
				if(!keyMenuPressed) {
					CCFG::getMM()->enter();
					keyMenuPressed = true;
				}
			case SDLK_ESCAPE:
				if(!keyMenuPressed && CCFG::getMM()->getViewID() == CCFG::getMM()->eGame) {
					CCFG::getMM()->resetActiveOptionID(CCFG::getMM()->ePasue);
					CCFG::getMM()->setViewID(CCFG::getMM()->ePasue);
					CCFG::getMusic()->PlayChunk(CCFG::getMusic()->cPASUE);
					CCFG::getMusic()->PauseMusic();
					keyMenuPressed = true;
				}
				break;
		}
	}

	if(keyAPressed) {
		if(!oMap->getPlayer()->getMove() && firstDir == false && !oMap->getPlayer()->getChangeMoveDirection() && !oMap->getPlayer()->getSquat()) {
			oMap->getPlayer()->startMove();
			oMap->getPlayer()->setMoveDirection(false);
		} else if(!keyDPressed && oMap->getPlayer()->getMoveSpeed() > 0 && firstDir != oMap->getPlayer()->getMoveDirection()) {
			oMap->getPlayer()->setChangeMoveDirection();
		}
	}

	if(keyDPressed) {
		if(!oMap->getPlayer()->getMove() && firstDir == true && !oMap->getPlayer()->getChangeMoveDirection() && !oMap->getPlayer()->getSquat()) {
			oMap->getPlayer()->startMove();
			oMap->getPlayer()->setMoveDirection(true);
		} else if(!keyAPressed && oMap->getPlayer()->getMoveSpeed() > 0 && firstDir != oMap->getPlayer()->getMoveDirection()) {
			oMap->getPlayer()->setChangeMoveDirection();
		}
	}

	if(oMap->getPlayer()->getMove() && !keyAPressed && !keyDPressed) {
		oMap->getPlayer()->resetMove();
	}
}

void CCore::MouseInput() {
	switch(mainEvent->type) {
		case SDL_MOUSEBUTTONDOWN: {
			switch (mainEvent->button.button) {
				case SDL_BUTTON_LEFT:
					mouseLeftPressed = true;
					break;
				case SDL_BUTTON_RIGHT:
					mouseRightPressed = true;
					break;
			}
			break;
		}
		case SDL_MOUSEMOTION: {
			
			SDL_GetMouseState(&mouseX, &mouseY);
			//CCFG::getMM()->getConsole()->print("x:" + std::to_string(mouseX));
			//CCFG::getMM()->getConsole()->print("y:" + std::to_string(mouseY));
			break;
		}
		case SDL_MOUSEBUTTONUP: {
			switch (mainEvent->button.button) {
				case SDL_BUTTON_LEFT:
					mouseLeftPressed = false;
					break;
				case SDL_BUTTON_RIGHT:
					mouseRightPressed = false;
					break;
			}
			break;
		}
		case SDL_MOUSEWHEEL:
			if(mainEvent->wheel.timestamp > SDL_GetTicks() - 2) {
				//CCFG::getMM()->getLE()->mouseWheel(mainEvent->wheel.y);
			}
			break;
	}
}

void CCore::resetKeys() {
	movePressed = keyMenuPressed = keyS = keyW = keyA = keyD = CCFG::keySpace = keyShift = keyAPressed = keyDPressed = false;
}

void CCore::Update() {
	// note SDL Joystick broken in Emscripten SDL2?
	#ifndef EMSCRIPTEN
	if (!this->quitGame && this->joystick1) {
		SDL_PumpEvents();
		//int hat = SDL_JoystickGetHat(this->joystick1, 0);
		int btns[10]  = {0,0,0,0, 0,0,0,0, 0,0};
		for (int i=0; i<10; i++) {
			btns[i] = SDL_JoystickGetButton(this->joystick1, i);
		}
		if (btns[9]) this->quitGame=true;
		if (btns[0]) CCFG::getMM()->enter();
		if (btns[1]) CCFG::getMM()->enter();


		if (this->joystick1_axes==6) {
			float x1 = ((double)SDL_JoystickGetAxis(this->joystick1, 0)) / 32768.0;
			float y1 = ((double)SDL_JoystickGetAxis(this->joystick1, 1)) / 32768.0;
			float z1 = (((double)SDL_JoystickGetAxis(this->joystick1, 2)) / 32768.0) + 1.0;  // xbox gamepads have 6 axes
			float x2 = ((double)SDL_JoystickGetAxis(this->joystick1, 2)) / 32768.0;
			float y2 = ((double)SDL_JoystickGetAxis(this->joystick1, 3)) / 32768.0;
			float z2 = ((double)SDL_JoystickGetAxis(this->joystick1, 4)) / 32768.0;
			//std::cout << "<gamepad: x1=" << x1 << ", y1=" << y1 << ", x2=" << x2 << ", y2=" << y2 << ", z1=" << z1 << ", z2=" << z2 << ">" << std::endl;
			this->oMap->getPlayer()->update_joystick( x1, y1, y2, z2 );

		} else {
			float x1 = ((double)SDL_JoystickGetAxis(this->joystick1, 0)) / 32768.0;
			float y1 = ((double)SDL_JoystickGetAxis(this->joystick1, 1)) / 32768.0;
			float x2 = ((double)SDL_JoystickGetAxis(this->joystick1, 2)) / 32768.0;
			float y2 = ((double)SDL_JoystickGetAxis(this->joystick1, 3)) / 32768.0;
			//std::cout << "<gamepad: x=" << x1 << ", y=" << y1 << ", z=" << x2 << ", w=" << y2 << ">" << std::endl;
			this->oMap->getPlayer()->update_joystick( x1, y1, x2, y2 );

		}
	}
	#endif

	CCFG::getMM()->Update();

}


void CCore::Draw() {
	CCFG::getMM()->Draw(rR);
}

/* ******************************************** */

void CCore::resetMove() {
	this->keyAPressed = this->keyDPressed = false;
}

Map* CCore::getMap() {
	return oMap;
}