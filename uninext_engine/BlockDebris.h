#pragma once

#ifndef BLOCKDEBRIS_H
#define BLOCKDEBRIS_H

#include "header.h"
#include "Vec2i.h"

class BlockDebris
{
private:
	// ----- 0 = Animation, -1 = Delete
	int debrisState;

	Vec2i* vPositionL;
	Vec2i* vPositionR;
	Vec2i* vPositionL2;
	Vec2i* vPositionR2;

	int iFrameID;

	float fSpeedX, fSpeedY;

	bool bRotate;
public:
	BlockDebris(int iXPos, int iYPos);
	~BlockDebris(void);

	void Update();
	void Draw(SDL_Renderer* rR);

	int getDebrisState();
};

#endif