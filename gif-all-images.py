#!/usr/bin/python
import os, sys, subprocess
for file in os.listdir('./uninext_engine/files/images'):
	if file.endswith('.bmp'):
		bmp = os.path.join('./uninext_engine/files/images', file)
		gif = os.path.join('./uninext_engine/files/images', file.replace('.bmp', '.gif'))
		subprocess.check_call(['convert', bmp, gif])

for file in os.listdir('./uninext_engine/files/images/mario'):
	if file.endswith('.bmp'):
		bmp = os.path.join('./uninext_engine/files/images/mario', file)
		gif = os.path.join('./uninext_engine/files/images/mario', file.replace('.bmp', '.gif'))
		subprocess.check_call(['convert', bmp, gif])
