#!/usr/bin/python
import os, sys, subprocess
for file in os.listdir('./uninext_engine/files/sounds'):
	if file.endswith('.wav'):
		wav = os.path.join('./uninext_engine/files/sounds', file)
		ogg = os.path.join('./uninext_engine/files/sounds', file.replace('.wav', '.ogg'))
		subprocess.check_call(['oggenc', wav, '--quality=-1', '-o', ogg])

