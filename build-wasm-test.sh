#!/bin/bash
# https://gist.github.com/WesThorburn/00c47b267a0e8c8431e06b14997778e4
# https://medium.com/@tdeniffel/pragmatic-compiling-from-c-to-webassembly-a-guide-a496cc5954b8
#cd ~/emsdk
#source ./emsdk_env.sh --build=Release
#cd ~/uninext
#CMAKE_TOOLCHAIN_FILE=~/emsdk/upstream/emscripten/cmake/Modules/Platform/Emscripten.cmake
#mkdir buildwasm
#cd buildwasm
#cmake ../. -DWASM=ON
#make


rm -rf ~/tpythonpp/tinypy/uninext
mkdir ~/tpythonpp/tinypy/uninext
mkdir ~/tpythonpp/tinypy/uninext/files
mkdir ~/tpythonpp/tinypy/uninext/files/images
mkdir ~/tpythonpp/tinypy/uninext/files/images/mario
mkdir ~/tpythonpp/tinypy/uninext/files/sounds

cp -v ~/uninext/uninext_engine/*.cpp ~/tpythonpp/tinypy/uninext/.
cp -v ~/uninext/uninext_engine/*.h ~/tpythonpp/tinypy/uninext/.
cp -v ~/uninext/uninext_engine/files/images/*.gif ~/tpythonpp/tinypy/uninext/files/images/.
cp -v ~/uninext/uninext_engine/files/images/mario/*.gif ~/tpythonpp/tinypy/uninext/files/images/mario/.
#cp -v ~/uninext/uninext_engine/files/sounds/*.ogg ~/tpythonpp/tinypy/uninext/files/sounds/.

cd ~/tpythonpp
./rebuild.py --uninext --wasm --html --sdl --html-template=~/uninext/template_test.html examples/hello_uninext.py