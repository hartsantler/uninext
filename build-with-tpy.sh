#!/bin/bash

mkdir ~/uninext/build
cp -Rv ~/uninext/uninext_engine/files ~/uninext/build/.

#rm -rf ~/tpythonpp/tinypy/uninext
mkdir ~/tpythonpp/tinypy/uninext
cp -v ~/uninext/uninext_engine/*.cpp ~/tpythonpp/tinypy/uninext/.
cp -v ~/uninext/uninext_engine/*.h ~/tpythonpp/tinypy/uninext/.

cd ~/tpythonpp
#./rebuild.py --uninext --wasm --sdl
#./rebuild.py --uninext --sdl --debug examples/hello_uninext.py
./rebuild.py --uninext --sdl examples/hello_uninext.py
cp -v ./tpython++ ~/uninext/build/.
#cd ~/uninext/build/
#./tpython++

